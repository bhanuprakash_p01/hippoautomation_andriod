package com.components.pages;

import com.components.repository.SiteRepository;
import com.iwaf.framework.BasePage;



public class SitePage extends BasePage
{
	protected SiteRepository repository;
	
	SitePage(SiteRepository repository)
	{
		this.repository=repository;
		
	}
	
	public Hippo_IndustrialPage _gotoIndustrialPage()
	{
		return this.repository.industrialPage();
	}
}
	
	