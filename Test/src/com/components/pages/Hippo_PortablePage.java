package com.components.pages;

import org.testng.Assert;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.components.repository.SiteRepository;
import com.components.yaml.PortableData;
import com.iwaf.framework.components.IReporter.LogType;
import com.iwaf.framework.components.Target;

public class Hippo_PortablePage extends SitePage 
{
	
	public Hippo_PortablePage (SiteRepository repository)
	{
		super(repository);
	}
	public static final Target Hamburger_Menu=new Target("Hamburger_Menu","//*[@id='mobile-trigger']",Target.XPATH);
	public static final Target Down_Arrow=new Target("Down_Arrow","//*[@id='main-nav']/ul[2]/li[2]/span",Target.XPATH);
	public static final Target JobSite_Arrow=new Target("JobSite_Arrow","(//*[@class='respond770 nav-menu-arrow'])[1]",Target.XPATH);
	public static final Target KohlerIndustrialWorldWide = new Target("KohlerIndustrialWorldWide","//*[@id='gb-8a9t0w5t2-c--click-to-toggle']",Target.XPATH);
	
	
	public static final Target search_input_portable = new Target("search_input_portable","//*[@id=\"search\"]",Target.XPATH);
	public static final Target search_btn_portable = new Target("search_btn_portable","//*[@id=\"cms-wrapper\"]/header/div[2]/div[2]/div/form/button",Target.XPATH);
	public static final Target search_header = new Target("search_results","//*[@id=\"main-content-info-pages\"]/div[1]/h1",Target.XPATH);
	public static final Target search_results_count = new Target("search_results_count","//*[@id=\"productResultsCount\"]/div", Target.XPATH);
	public static final Target search_results_span = new Target("search_results_span","//*[@id=\"main-content-info-pages\"]/h2",Target.XPATH);
	public static final Target search_results_grid = new Target("search_results_grid","//*[@id=\"productResultsList\"]/div[2]",Target.XPATH);
	public static final Target dealer_find = new Target("dealer_find","//*[@id='main-nav']/ul[2]/li[6]/a",Target.XPATH);
	public static final Target dealer_header = new Target("dealer_header","//*[@id='dealer-search-form']/div[1]/div[1]/div/div[1]/h1",Target.XPATH);
	public static final Target dealer_zipCode = new Target("dealer_zipCode","//*[@id='dealer-search-form']/div[1]/div[3]/div/div[1]/input",Target.XPATH);
	public static final Target dealer_search_btn = new Target("dealer_search_btn","//*[@id=\"dealer-search-form\"]/div[1]/div[3]/div/div[3]/button",Target.XPATH);
	public static final Target dealer_search_results_count = new Target("dealer_search_results_count","//*[@id=\"dealer-search-form\"]/div[3]/div[1]/div/div/div/span[1]",Target.XPATH);
	public static final Target dealer_search_count_change = new Target("dealer_search_count_change","//*[@id=\"dealer-search-form\"]/div[1]/div[3]/div/div[2]/div/div/div/div[2]/div/ul/li[4]/span",Target.XPATH);
	public static final Target dealer_search_dropdown = new Target("dealer_search_dropdown","//*[@id=\"dealer-search-form\"]/div[1]/div[3]/div/div[2]/div/div/a",Target.XPATH);
	public static final Target dealer_search_dropdonw_span = new Target("dealer_search_dropdonw_span","//*[@id=\"dealer-search-form\"]/div[1]/div[3]/div/div[2]/div/div/a/span",Target.XPATH);
	
	public static final Target contact_portable = new Target("contact_portable","//*[@id=\"footer-link-wrap\"]/div[3]/h3",Target.XPATH);
	public static final Target contact_header = new Target("contact_header","//*[@id=\"contact-sales-service\"]/div[1]",Target.XPATH);
	//Contact Us
	public static final Target contact_us_first_name = new Target("contact_us_first_name","//*[@id=\"firstName\"]",Target.XPATH);
	public static final Target contact_us_last_name = new Target("contact_us_last_name","//*[@id=\"lastName\"]",Target.XPATH);
	public static final Target contact_us_email = new Target("contact_us_email","//*[@id=\"emailAddress\"]",Target.XPATH);
	public static final Target contact_us_email_confirm = new Target("contact_us_email_confirm","//*[@id=\"confirmEmailAddress\"]",Target.XPATH);
	public static final Target contact_us_phone = new Target("contact_us_phone","//*[@id=\"phoneNumber\"]",Target.XPATH);
	public static final Target contact_us_address = new Target("contact_us_address","//*[@id=\"addressLine1\"]",Target.XPATH);
	public static final Target contact_us_address2 = new Target("contact_us_address2","//*[@id=\"addressLine2\"]",Target.XPATH);
	public static final Target contact_us_city = new Target("contact_us_city","//*[@id=\"city\"]",Target.XPATH);
	public static final Target contact_us_postalcode = new Target("contact_us_postalcode","//*[@id=\"postalCode\"]",Target.XPATH);
	public static final Target contact_us_comments = new Target("contact_us_comments","//*[@id=\"message\"]",Target.XPATH);
	public static final Target contact_us_state = new Target("contact_us_state","//select[@id=\"state\"]",Target.XPATH);
	public static final Target contact_us_state_select = new Target("contact_us_state_select","//*[@id=\"contact-sales-form\"]/div[5]/div[2]/div/div/div/div[2]/div/ul/li[55]/span",Target.XPATH);
	public static final Target contact_us_country = new Target("contact_us_country","//select[@id=\"country\"]",Target.XPATH);
	public static final Target contact_us_country_select = new Target("contact_us_country_select","//*[@id=\"contact-sales-form\"]/div[6]/div[2]/div/div/div/div[2]/div/ul/li[2]/span",Target.XPATH);
	public static final Target contact_us_submit_btn = new Target("contact_us_submit_btn","//*[@id=\"contact-sales-form\"]/div[8]/button",Target.XPATH);
	
	public static final Target homepage_global_banner = new Target("homepage_global_banner","//*[@id=\"gb-8a9t0w5t2-c--global-banner\"]",Target.XPATH);
	public static final Target homepage_global_nav = new Target("homepage_global_nav","//*[@id=\"cms-wrapper\"]/header/div[2]",Target.XPATH);
	public static final Target homepage_hero_carousel = new Target("homepage_hero_carousel","//*[@id=\"hero\"]",Target.XPATH);
	public static final Target homepage_promo = new Target("homepage_promo","//*[@id=\"homepage\"]/div/div[2]",Target.XPATH);
	public static final Target homepage_footer = new Target("homepage_footer","//*[@id=\"footer-link-wrap\"]",Target.XPATH);
	
	public static final Target btn_home_products = new Target("btn_home_products","//*[@id=\"main-nav\"]/ul[2]/li[4]/a",Target.XPATH);
	public static final Target btn_products_gen = new Target("btn_products_gen","//*[@id=\"products-landing-wrapper\"]/div/ul/li[1]/div/a",Target.XPATH);
	public static final Target product_pdp = new Target("product_pdp","//*[@id=\"enCUBE1.8\"]/div[2]/div[1]",Target.XPATH);
	public static final Target pdp_breadcrumb = new Target("pdp_breadcrumb","//*[@id=\"mobile-wrap\"]/div[2]/div/div/div[1]/div/div[1]",Target.XPATH);
	public static final Target pdp_returntoresults = new Target("pdp_returntoresults","//*[@id=\"mobile-wrap\"]/div[2]/div/div/div[1]/div/div[2]/div/a",Target.XPATH);
	public static final Target pdp_maindisplayimage = new Target("pdp_maindisplayimage","//*[@id=\"product-detail-wrap\"]/div/div[1]/div/div[1]/div[1]",Target.XPATH);
	public static final Target pdp_thumbnails = new Target("pdp_thumbnails","//*[@id=\"product-detail-wrap\"]/div/div[1]/div/div[1]/div[2]",Target.XPATH);
	public static final Target pdp_productinfo = new Target("pdp_productinfo","//*[@id=\"product-detail-wrap\"]/div/div[1]/div/div[2]",Target.XPATH);
	public static final Target pdp_price = new Target("pdp_price","//*[@id=\"product-detail-wrap\"]/div/div[1]/div/div[2]/div[2]",Target.XPATH);
	public static final Target pdp_finddealer = new Target("pdp_finddealer","//*[@id=\"get-quote\"]",Target.XPATH);
	public static final Target pdp_email = new Target("pdp_email","//*[@id=\"product-detail-wrap\"]/div/div[1]/div/div[2]/div[4]/a[1]",Target.XPATH);
	public static final Target pdp_print = new Target("pdp_print","//*[@id=\"product-detail-wrap\"]/div/div[1]/div/div[2]/div[4]/a[2]",Target.XPATH);
	public static final Target pdp_tabs = new Target("pdp_tabs","//*[@id=\"product-detail-wrap\"]/div/div[2]/div",Target.XPATH);
	public static final Target pdp_recommendedaccessories = new Target("pdp_recommendedaccessories","//*[@id=\"product-detail-wrap\"]/div/div[2]/aside",Target.XPATH);
	
	public static final Target Link_Portable_jobSite = new Target("Link_Portable_jobSite","//*[@id='main-nav']//following::a[text()='Job Site']",Target.XPATH);	
	public static final Target Link_Portable_HomePlay = new Target("Link_Portable_HomePlay","//*[@id='main-nav']//following::a[text()='Home & Play']",Target.XPATH);	
	public static final Target Link_Portable_CustomKits = new Target("Link_Portable_CustomKits","//*[@id='main-nav']/ul[2]/li[3]/a",Target.XPATH);	
	public static final Target Link_Portable_generator101 = new Target("Link_Portable_generator101","//*[@id='subnav-jobsite']//following::a[text()='Generators 101']",Target.XPATH);	
	public static final Target Link_Portable_generator = new Target("Link_Portable_generator","(//*[@id=\"subnav-jobsite\"]/ul/li[2]/a)[1]",Target.XPATH);	
	public static final Target Link_OwnerManual = new Target("Link_OwnerManual","//*[@id='subnav-homeplay']//following::li[6]/a",Target.XPATH);	
	
	public static final Target Article_Portable_PerfectGen = new Target("Article_Portable_PerfectGen","//div[@class='column-2 cms-block container ']",Target.XPATH);	
	public static final Target Article_Portable_PerfectGenheader = new Target("Article_Portable_PerfectGenheader","//div[@class='column-2 cms-block container ']/div[1]//following::h1[1]",Target.XPATH);	
	
	public static final Target Article_Portable_PortableGenerator = new Target("Article_Portable_PortableGenerator","//div[@class='icon-tabs cms-block ']/div[@class='container']",Target.XPATH);	
	public static final Target Article_Portable_PortableGeneratorheader = new Target("Article_Portable_PortableGeneratorheader","//div[@class='icon-tabs cms-block ']/div[@class='container']/h1",Target.XPATH);	
	
	public static final Target Article_Portable_UseitProperly = new Target("Article_Portable_UseitProperly","//div[@class='hst-container']",Target.XPATH);	
	public static final Target Article_Portable_UseitProperlyheader = new Target("Article_Portable_UseitProperlyheader","//div[@class='hst-container']//following::div[@class='copy-wrap pull-left']/h1",Target.XPATH);	
	
	public static final Target Article_Portable_GiveUsAShout = new Target("Article_Portable_GiveUsAShout","//div[@class='column-2 banner-action cms-block container']",Target.XPATH);	
	public static final Target Article_Portable_GiveUsAShoutheader = new Target("Article_Portable_GiveUsAShoutheader","//div[@class='column-2 banner-action cms-block container']//following::div[@class='banner box box-large pull-left']/h1",Target.XPATH);
	
	public static final Target Article_Portable_HomePlayPortableGenerator = new Target("Article_Portable_HomePlayPortableGenerator","//div[@id='asset-1365055158359']",Target.XPATH);	
	public static final Target Article_Portable_HomePlayPortableGeneratorheader = new Target("Article_Portable_HomePlayPortableGeneratorheader","//div[@id='asset-1365055158359']//following::h1",Target.XPATH);	
	
	public static final Target Article_Portable_HomePlayPortableGeneratorrated = new Target("Article_Portable_HomePlayPortableGeneratorrated","//div[@id='asset-1365055281271']",Target.XPATH);	
	public static final Target Article_Portable_HomePlayPortableGeneratorratedheader = new Target("Article_Portable_HomePlayPortableGeneratorratedheader","//div[@id='asset-1365055281271']//following::div[@class='container']/h1",Target.XPATH);	
	
	public static final Target Slider_Portables_KW_Range = new Target("Slider_Portables_KW_Range","//*[@id='kwSlider']/a[1]/span",Target.XPATH);	
	public static final Target Slider_Portables_MSRP_Range = new Target("Slider_Portables_MSRP_Range","//*[@id='msrpSlider']/a[1]/span",Target.XPATH);	
	public static final Target link_Portables_SortBy = new Target("link_Portables_SortBy","//*[@id='sort-options-top']",Target.XPATH);	
	public static final Target link_Portables_view = new Target("link_Portables_view","//div[@class='per-page']//following::div[@class='ffSelect']/a",Target.XPATH);	
	public static final Target Text_Portables_PageCount = new Target("Text_Portables_PageCount","//div[@id='paging-top-container']//following::div[@class='paging']/div[1]/span[@class='total-count']",Target.XPATH);	
	public static final Target Text_FindADealer_Title = new Target("Text_FindADealer_Title","//div[@class='page-title ']/h1",Target.XPATH);	
	public static final Target Text_GeneratorsTitle = new Target("Text_GeneratorsTitle","//div[@class='page-title']/h1",Target.XPATH);	
	public static final Target Text_GeneratorViewresults = new Target("Text_GeneratorViewresults","//*[@id='get-quote']",Target.XPATH);	
	
	public static final Target KohlerPortablesWorldWide = new Target("KohlerPortablesWorldWide","//*[@id='gb-8a9t0w5t2-c--click-to-toggle']",Target.XPATH);
	public static final Target Link_Contact = new Target("Link_Contact","//*[@id='footer-link-wrap']/div[3]/ul/li[2]/a",Target.XPATH);
	public static final Target business_Dropdown = new Target("business_Dropdown","//*[@id='selectBusiness']",Target.XPATH);

	
	
	PortableData portableData = PortableData.fetch("PortableData");
	
	public Hippo_PortablePage atPortablePage()
	{
		try 
		{
			//Retrieving page state
			JavascriptExecutor js = (JavascriptExecutor) getCommand().driver;
			String test = js.executeScript("return document.readyState").toString();
			//Verifying page load
			if (test.equalsIgnoreCase("complete")) {
				Assert.assertEquals(getCommand().driver.getTitle(), portableData.page_Title);
				log ("Portable Page loaded",LogType.STEP);
			}
		}
		catch (Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	//Verify Portable search with search term returning no results
	public Hippo_PortablePage verifyNoResults()
	{
		try
		{
			PortableData portableData = PortableData.fetch("PortableData");
			log("Search input: "+portableData.search_NoResult+" entered and clicked on Search button",LogType.STEP);
			getCommand().isTargetVisible(Hamburger_Menu);
			getCommand().click(Hamburger_Menu);
			getCommand().waitFor(5);
			getCommand().sendKeys(search_input_portable, portableData.search_NoResult);
			getCommand().sendKeys(search_btn_portable,Keys.chord(Keys.CONTROL,Keys.RETURN));
			
			getCommand().waitFor(3);
			ArrayList<String> listofTabs = new ArrayList<String> (getCommand().driver.getWindowHandles());
			getCommand().waitFor(1);
			getCommand().driver.switchTo().window(listofTabs.get(1));
			
			
			/*Capabilities caps = ((RemoteWebDriver) getCommand().driver).getCapabilities();
			if(caps.getBrowserName().equals("firefox"))
			{
				WebDriverWait wait = new WebDriverWait(getCommand().driver, 15);
				  wait.until(ExpectedConditions.alertIsPresent());
				Alert alert = getCommand().driver.switchTo().alert();
				alert.accept();
			}*/
			
			getCommand().waitForTargetVisible(search_header);
			Assert.assertEquals(portableData.search_Header, getCommand().getText(search_header));
			log("Search Results page opened",LogType.STEP);
			Assert.assertEquals("We're sorry, we didn't find any results for "+portableData.search_NoResult+".", getCommand().getText(search_results_span));
			log("Verified message: "+getCommand().getText(search_results_span),LogType.STEP);
			
			 getCommand().driver.close();
	         getCommand().driver.switchTo().window(listofTabs.get(0));
		}
		catch (Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	//Verify Portable search with valid search term
	public Hippo_PortablePage verifySearchResults()
	{
		try
		{
			PortableData portableData = PortableData.fetch("PortableData");
			String portable_url = getCommand().driver.getCurrentUrl();
			log("Search input: "+portableData.search_Result+" entered and clicked on Search button",LogType.STEP);

			getCommand().isTargetVisible(Hamburger_Menu);
			getCommand().click(Hamburger_Menu);
			getCommand().waitFor(5);
			getCommand().sendKeys(search_input_portable, portableData.search_Result);
			getCommand().sendKeys(search_btn_portable,Keys.chord(Keys.CONTROL,Keys.RETURN));
			getCommand().waitFor(3);
			ArrayList<String> listofTabs = new ArrayList<String> (getCommand().driver.getWindowHandles());
			getCommand().waitFor(1);
			getCommand().driver.switchTo().window(listofTabs.get(1));
			
			/*Capabilities caps = ((RemoteWebDriver) getCommand().driver).getCapabilities();
			if(caps.getBrowserName().equals("firefox"))
			{
				WebDriverWait wait = new WebDriverWait(getCommand().driver, 15);
				  wait.until(ExpectedConditions.alertIsPresent());
				Alert alert = getCommand().driver.switchTo().alert();
				alert.accept();
			}*/
			
			getCommand().waitForTargetVisible(search_header);
			log("Search Results page opened",LogType.STEP);
			getCommand().waitFor(5);
			boolean status = portable_url.contains(getCommand().driver.getCurrentUrl());
			Assert.assertEquals(false, status);
			Assert.assertEquals("We're sorry, we didn't find any results for "+portableData.search_Result+".", getCommand().getText(search_results_span));
			log("Verified Search Results screen (external url) displayed",LogType.STEP);
		    
			getCommand().driver.close();
	         getCommand().driver.switchTo().window(listofTabs.get(0));
		}
		catch (Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	//Verify Find Dealer on Portable Page
	public Hippo_PortablePage verifyPortableFindDealer() throws InterruptedException
	{
		/*try
		{
			*/getCommand().waitForTargetVisible(Hamburger_Menu).click(Hamburger_Menu);
			getCommand().waitFor(2);
			//getCommand().driver.findElement(By.xpath("(//*[@class='respond770 nav-menu-arrow'])[4]")).click();
			getCommand().waitFor(2);
			getCommand().waitForTargetPresent(dealer_find).click(dealer_find);
			
			System.out.println("enter into find dealer screen");
			getCommand().waitForTargetPresent(dealer_header);
			String header_Text=getCommand().getText(dealer_header);
			System.out.println("Header text  is: "+header_Text);
			if(header_Text.equalsIgnoreCase("Find a Dealer")) {
				log("Find a Dealer page opened",LogType.STEP);
			}
			
			//Assert.assertEquals(portableData.dealer_Header,getCommand().getText(dealer_header));
			
			
			  WebElement element=getCommand().driver.findElement(By.xpath("//*[@id='dealer-search-form']/div[1]/div[3]/div/div[1]/input"));
        	      JavascriptExecutor jse=(JavascriptExecutor)getCommand().driver;
        	       jse.executeScript("arguments[0].scrollIntoView(true);", element);
        	       Thread.sleep(3000);

			getCommand().waitFor(5);
			getCommand().clear(dealer_zipCode).sendKeys(dealer_zipCode,portableData.dealer_zipCode);
			getCommand().click(dealer_search_btn);
			log("Entered Zip Code and clicked on Search button",LogType.STEP);
			
			getCommand().waitFor(5);
			String search_results = getCommand().getText(dealer_search_results_count);
			log(getCommand().getText(dealer_search_results_count)+" locations found for \""+portableData.dealer_zipCode+"\"",LogType.STEP);
			
			getCommand().selectDropDown(dealer_search_dropdown, 2);
			getCommand().click(dealer_search_btn);
			//log("Changed to "+getCommand().getText(dealer_search_dropdonw_span)+ " and clicked on Search button",LogType.STEP);
			
			getCommand().waitFor(5);
			log(getCommand().getText(dealer_search_results_count)+" locations found for \""+portableData.dealer_zipCode+"\"",LogType.STEP);
			Assert.assertNotEquals(getCommand().getText(dealer_search_results_count), search_results);
			log("Results updated and verified",LogType.STEP);
		/*}
		catch (Exception ex) {
			Assert.fail(ex.getMessage());
		}*/
		return this;
	}
	
	
	
	//Verify Find Dealer on Portable Page
		public Hippo_PortablePage verifyPortableFindDealer_mobile() 
		{
			
				getCommand().waitForTargetVisible(Hamburger_Menu).click(Hamburger_Menu);
				getCommand().waitFor(2);
				//getCommand().driver.findElement(By.xpath("(//*[@class='respond770 nav-menu-arrow'])[4]")).click();
				getCommand().waitFor(2);
				getCommand().click(dealer_find);
				//getCommand().waitFor(2);
				
				System.out.println("enter into find dealer screen");
				
				String header_Text=getCommand().waitForTargetVisible(dealer_header).getText(dealer_header);
				System.out.println("Header text  is: "+header_Text);
				
				
				String getetxt=getCommand().driver.findElement(By.xpath("//*[@id='dealer-search-form']/div[1]/div[1]/div/div[2]/p")).getText();
				
				System.out.println("text..."+getetxt);
				System.out.println("enter into find dealer screen");
				
				
				
			
			return this;
		}
	
	//Verify Contact Us on Portable page
	public Hippo_PortablePage verifyContactUs()
	{
		try
		{
			PortableData portableData = PortableData.fetch("PortableData");
			getCommand().scrollTo(contact_portable).click(contact_portable);
			log("Click on Contact under Sales & Service",LogType.STEP);
			getCommand().sendKeys(Link_Contact, Keys.chord(Keys.CONTROL,Keys.RETURN));
			getCommand().waitFor(3);
			ArrayList<String> listofTabs = new ArrayList<String> (getCommand().driver.getWindowHandles());
			getCommand().waitFor(1);
			getCommand().driver.switchTo().window(listofTabs.get(1));
			
			
			
			getCommand().waitFor(5);
			//Filling Send Us a Message Form
			/*getCommand().selectDropDown(business_Dropdown, 6);
			getCommand().waitFor(5);*/
			Assert.assertEquals(getCommand().getText(contact_header), portableData.contact_Header);
			log( getCommand().getText(contact_header)+" page Opened",LogType.STEP);
			
			log("Filling the Contact Us form",LogType.STEP);
			getCommand().sendKeys(contact_us_first_name, portableData.contact_us_firstname);
			getCommand().sendKeys(contact_us_last_name, portableData.contact_us_lastname);
			getCommand().sendKeys(contact_us_email, portableData.contact_us_email);
			getCommand().sendKeys(contact_us_email_confirm, portableData.contact_us_email);
			getCommand().sendKeys(contact_us_phone, portableData.contact_us_phone);
			getCommand().sendKeys(contact_us_address, portableData.contact_us_address);
			getCommand().sendKeys(contact_us_address2, portableData.contact_us_address2);
			getCommand().sendKeys(contact_us_city, portableData.contact_us_city);
			getCommand().selectDropDown(contact_us_state, 54);
			//getCommand().click(contact_us_state_select);
			getCommand().sendKeys(contact_us_postalcode, portableData.contact_us_postalcode);
			getCommand().selectDropDown(contact_us_country, 25);
			//getCommand().click(contact_us_country_select);
			getCommand().sendKeys(contact_us_comments, portableData.contact_us_comments);
			
			getCommand().click(contact_us_submit_btn);
			getCommand().waitFor(5);
			log("Closing Contact Us page and switching to Main page",LogType.STEP);
            getCommand().driver.close();
            getCommand().driver.switchTo().window(listofTabs.get(0));
   		}
	catch (Exception ex) {
		Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	//Verify Footer links on Portable page
	public Hippo_PortablePage VerifyFooter()
	{
        try 
        {
        	log("Verifying footer is composed of 3 columns Links",LogType.STEP);
        	getCommand().scrollTo(homepage_footer);
        	List<WebElement> header_menu = getCommand().driver.findElements(By.xpath("//*[@id='footer-link-wrap']/div/h3"));
        	log("Footer menu has "+header_menu.size()+" columns",LogType.STEP);
        	
        	log("Checking the navigation of each link under Footer section",LogType.STEP);
        	//String PageTitle= getCommand().getPageTitle();
        	for(int i=1;i<=header_menu.size();i++) {
        		getCommand().driver.findElement(By.xpath("//*[@id='footer-link-wrap']/div["+i+"]")).click();
        		List<WebElement> sub_menu = getCommand().driver.findElements(By.xpath("//*[@id='footer-link-wrap']/div["+i+"]/ul/li/a"));
        		for(int j=1;j<=sub_menu.size();j++) {
        			Target subLinks=new Target("subLinks","//*[@id='footer-link-wrap']/div["+i+"]/ul/li["+j+"]/a",Target.XPATH);
               	 	String Linktext=getCommand().getText(subLinks);
               	  log("Clicking on the link "+Linktext,LogType.STEP);
            	  getCommand().waitFor(1);
            	  String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN); 
            
            	  getCommand().sendKeys(subLinks, selectLinkOpeninNewTab);
                  getCommand().waitFor(7);
                  ArrayList<String> listofTabs = new ArrayList<String> (getCommand().driver.getWindowHandles());
                
                  log("Switching to new tab",LogType.STEP);
                  getCommand().driver.switchTo().window(listofTabs.get(1));
                  getCommand().waitFor(3);
                  log("Getting new tab page title",LogType.STEP);
      	    	  String CurrentpageTitle = getCommand().getPageTitle();
      	    	 
      	    	  log("Clicking on "+Linktext+" opens a tab with page title "+CurrentpageTitle+" ",LogType.STEP);
      	    		  
      	    		 
      	    		
      	    		  getCommand().driver.close();
      	    		  getCommand().driver.switchTo().window(listofTabs.get(0));
        		}
        	}
        	
        	
        	
        }
	catch(Exception ex)
        {
			Assert.fail(ex.getMessage());
        }
		return this;
	}
	
	//Verify Portable Home Page
	public Hippo_PortablePage VerifyHomePage()
	{
		try
		{
			getCommand().waitFor(5);
			//Global Banner
			String banner_text = getCommand().driver.findElement(By.xpath("//*[@id=\"gb-8a9t0w5t2-c--click-to-toggle\"]")).getText();
			if (getCommand().isTargetVisible(homepage_global_banner) && banner_text.contains(portableData.banner_text))
			{
				log("Global Banner "+banner_text+" is displayed",LogType.STEP);
			}
			else
			{
				log(banner_text+" is not displayed",LogType.ERROR_MESSAGE);
			}
			
			//Kohler Logo
			if (getCommand().isTargetVisible(homepage_global_banner) && banner_text.contains(portableData.banner_text))
			{
				log("Kohler Logo is displayed",LogType.STEP);
			}
			else
			{
				log("Kohler Logo is not displayed",LogType.ERROR_MESSAGE);
			}
			
			
			
			
			//Carousel
			if (getCommand().isTargetVisible(homepage_hero_carousel))
			{
				log("Hero Carousel is displayed",LogType.STEP);
			}
			else
			{
				log("Hero Carousel is not displayed",LogType.ERROR_MESSAGE);
			}
			
			//Promo		
			if(getCommand().isTargetVisible(homepage_promo))
			{
				log("Promo modules are displayed",LogType.STEP);
			}
			else
			{
				log("Promo modules are not displayed",LogType.ERROR_MESSAGE);
			}
			
			//Footer
			if(getCommand().isTargetVisible(homepage_footer))
			{
				log("Footer is displayed",LogType.STEP);
			}
			else
			{
				log("Footer is not displayed",LogType.ERROR_MESSAGE);
			}
			//Utility Navigation

			getCommand().isTargetVisible(Hamburger_Menu);
			getCommand().click(Hamburger_Menu);
			WebElement utility_nav = getCommand().driver.findElement(By.xpath("//*[@id=\"main-nav\"]/ul[2]"));
			List<WebElement> utility_menu_links = utility_nav.findElements(By.tagName("li"));
			log("Utility Navigation bar consisting of below is displayed:",LogType.STEP);
			for (WebElement ele:utility_menu_links)
			{
				WebElement display_text = ele.findElement(By.tagName("a"));
				if (utility_nav.isDisplayed() && ele.isDisplayed())
				{
					log(display_text.getText(), LogType.SUBSTEP);
				}
			}
			//Search
			WebElement search_box = getCommand().driver.findElement(By.xpath("//*[@id=\"cms-wrapper\"]/header/div[2]/div[2]/div/form"));
			if (search_box.isDisplayed())
			{
				log("Search Box is displayed",LogType.STEP);
			}
			else
			{
				log("Search Box is not displayed",LogType.ERROR_MESSAGE);
			}
			
		}
		catch( Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	//Verify Portable PDP page
	public Hippo_PortablePage verifyPDP()
	{
		try
		{
			
			PortableData portableData = PortableData.fetch("PortableData");
			log("Navigating to Product Display Page from Home Page",LogType.STEP);
			getCommand().isTargetVisible(Hamburger_Menu);
			getCommand().click(Hamburger_Menu);
			getCommand().click(btn_home_products);
			getCommand().waitForTargetPresent(btn_products_gen).click(btn_products_gen);
			getCommand().click(product_pdp);
			
			log("Product page opened",LogType.STEP);
			
			
			//Return to Results
			if(getCommand().isTargetVisible(pdp_returntoresults))
			{
				log("Return to Results is displayed",LogType.STEP);
			}
			else
			{
				log("Return to Results is not displayed",LogType.ERROR_MESSAGE);
			
			}
			
			//Main display image 
			if(getCommand().isTargetVisible(pdp_maindisplayimage))
			{
				log("Main display image is displayed",LogType.STEP);
			}
			else
			{
				log("Main display image is not displayed",LogType.ERROR_MESSAGE);
			
			}
			
			//Thumbnails 
			if(getCommand().isTargetVisible(pdp_thumbnails))
			{
				log("Thumbnails are displayed",LogType.STEP);
			}
			else
			{
				log("Thumbnails are not displayed",LogType.ERROR_MESSAGE);
			
			}
			
			//Product Name & Price 
			if(getCommand().isTargetVisible(pdp_productinfo) && getCommand().getText(pdp_productinfo).contains(portableData.pdp_product) && getCommand().getText(pdp_price).contains(portableData.pdp_currency))
			{
				log("Product name & Price are displayed:",LogType.STEP);
				log(getCommand().getText(pdp_productinfo),LogType.SUBSTEP);
			}
			else
			{
				log("Product name & Price are not displayed",LogType.ERROR_MESSAGE);
			
			}
			
			//Find a Dealer
			if(getCommand().isTargetVisible(pdp_finddealer))
			{
				log("Find a Dealer is displayed",LogType.STEP);
			}
			else
			{
				log("Find a Dealer is not displayed",LogType.ERROR_MESSAGE);
			
			}
			
			//Email & Print
			if(getCommand().isTargetVisible(pdp_email) && getCommand().isTargetVisible(pdp_email))
			{
				log("Email & Print links are displayed",LogType.STEP);
			}
			else
			{
				log("Email & Print links are not displayed",LogType.ERROR_MESSAGE);
			
			}
			
			//Features, Specs & Literature & Downloads
			List<String> productTabs = new ArrayList<String>();
			List<String> actualProductTabs = new ArrayList<String>();
			productTabs.add(portableData.pdp_Tab1);
			productTabs.add(portableData.pdp_Tab2);
			productTabs.add(portableData.pdp_Tab3);
			
			if(getCommand().isTargetVisible(pdp_tabs))
			{
				WebElement pdpTabs = getCommand().driver.findElement(By.xpath("//*[@id=\"product-detail-wrap\"]/div/div[2]/div/dl"));
				List<WebElement> tabs = pdpTabs.findElements(By.tagName("dt"));
				log("Below tabs are displayed:",LogType.STEP);
				for (WebElement ele:tabs)
				{
					actualProductTabs.add(ele.getText());
					log(ele.getText(),LogType.SUBSTEP);
				}
				CompareTabs (productTabs, actualProductTabs);
			}
			else
			{
				log("Specs, Engine Uses & Service tabs are not displayed",LogType.ERROR_MESSAGE);
			
			}
			
			//Recommended Accessories
			if(getCommand().isTargetVisible(pdp_recommendedaccessories))
			{
				log("Recommended Accessories are displayed",LogType.STEP);
			}
			else
			{
				log("Recommended Accessories are not displayed",LogType.ERROR_MESSAGE);
			
			}
		}
		catch (Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	public Hippo_PortablePage VerifyArticlePage()
	{
		try {
			log("Verifying Article Page",LogType.STEP);
			getCommand().waitForTargetVisible(Hamburger_Menu);
			getCommand().click(Hamburger_Menu);
			getCommand().waitForTargetPresent(Link_Portable_jobSite).click(Link_Portable_jobSite);
			
			if(getCommand().getPageUrl().contains("job-site"))
			{
				log("Navigated to Job site Page",LogType.STEP);
			}
			else
			{
				log("Was unable to navigated to Job site Page" + getCommand().getPageUrl() ,LogType.STEP);
				Assert.fail("Was unable to navigated to Job site Page");
			}
			
			//if(getCommand().isSelected(Link_Portable_generator101))
			//	log("In Generator 101 Page",LogType.STEP);
			//	Assert.assertTrue(true, "Generator 101 is Selected by default");
			
			if(getCommand().isTargetVisible(Article_Portable_PerfectGen))
			{
				log("In Generator 101 Page",LogType.STEP);
				String Actualheader = getCommand().getText(Article_Portable_PerfectGenheader);
				Assert.assertEquals(Actualheader, "Find your perfect generator.", "Header miss matches for Article_Portable_PerfectGen");
			}
			
			if(getCommand().isTargetVisible(Article_Portable_PortableGenerator))
			{
				log("In Generator 101 Page",LogType.STEP);
				String Actualheader = getCommand().getText(Article_Portable_PortableGeneratorheader);
				Assert.assertEquals(Actualheader, "How is a\nportable generator rated?", "Header miss matches for Article_Portable_PortableGenerator");
			}
			
			if(getCommand().isTargetVisible(Article_Portable_UseitProperly))
			{
				log("In Generator 101 Page",LogType.STEP);
				String Actualheader = getCommand().getText(Article_Portable_UseitProperlyheader);
				Assert.assertEquals(Actualheader, "How to use it properly", "Header miss matches for Article_Portable_UseitProperly");
			}
			
				
			if(getCommand().isTargetVisible(Article_Portable_GiveUsAShout))
			{
				log("In Generator 101 Page",LogType.STEP);
				String Actualheader = getCommand().getText(Article_Portable_GiveUsAShoutheader);
				Assert.assertEquals(Actualheader, "Give Us a Shout", "Header miss matches for Article_Portable_GiveUsAShout");
			}
			
			log("verify the article in Home & Play Page",LogType.STEP);
			getCommand().waitForTargetVisible(Hamburger_Menu);
			getCommand().click(Hamburger_Menu);
			getCommand().isTargetVisible(Link_Portable_HomePlay);
			getCommand().click(Link_Portable_HomePlay);
			
			if(getCommand().getPageUrl().contains("home-play"))
			{
				log("Navigated to Job site Page",LogType.STEP);
			}
			else
			{
				log("Was unable to navigated to Home & Play Page" + getCommand().getPageUrl() ,LogType.STEP);
				Assert.fail("Was unable to navigated to Home & Play Page");
			}
			
			//if(getCommand().isSelected(Link_Portable_generator101))
				//log("In Generator 101 Page",LogType.STEP);
				//Assert.assertTrue(true, "Generator 101 is Selected by default");
			
			if(getCommand().isTargetVisible(Article_Portable_PerfectGen))
			{
				log("In Generator 101 Page",LogType.STEP);
				String Actualheader = getCommand().getText(Article_Portable_PerfectGenheader);
				Assert.assertEquals(Actualheader, "Find your perfect generator.", "Header miss matches for Article_Portable_PerfectGen");
			}
			
			if(getCommand().isTargetVisible(Article_Portable_HomePlayPortableGeneratorrated))
			{
				log("In Generator 101 Page",LogType.STEP);
				String Actualheader = getCommand().getText(Article_Portable_HomePlayPortableGeneratorratedheader);
				Assert.assertEquals(Actualheader, "How is a\nportable generator rated?", "Header miss matches for Article_Portable_PortableGenerator");
			}
			
			if(getCommand().isTargetVisible(Article_Portable_HomePlayPortableGenerator))
			{
				log("In Generator 101 Page",LogType.STEP);
				String Actualheader = getCommand().getText(Article_Portable_HomePlayPortableGeneratorheader);
				Assert.assertEquals(Actualheader, "What is a\nportable generator?", "Header miss matches for Article_Portable_PortableGenerator");
			}
			
			if(getCommand().isTargetVisible(Article_Portable_UseitProperly))
			{
				log("In Generator 101 Page",LogType.STEP);
				String Actualheader = getCommand().getText(Article_Portable_UseitProperlyheader);
				Assert.assertEquals(Actualheader, "How to use it properly", "Header miss matches for Article_Portable_UseitProperly");
			}
			
				
			if(getCommand().isTargetVisible(Article_Portable_GiveUsAShout))
			{
				log("In Generator 101 Page",LogType.STEP);
				String Actualheader = getCommand().getText(Article_Portable_GiveUsAShoutheader);
				Assert.assertEquals(Actualheader, "Give Us a Shout", "Header miss matches for Article_Portable_GiveUsAShout");
			}
			
			
					
			
			
			
	}catch(Exception ex)
	{
		ex.getMessage();
	}
	

	return this;
	}
	
	
	// Verify the Article Page from generator 101
	public Hippo_PortablePage VerifyOwnerManualsPage()
		{
		try {
			
			log("verify the article in Home & Play Page",LogType.STEP);
			getCommand().waitForTargetVisible(Hamburger_Menu);
			getCommand().click(Hamburger_Menu);
			getCommand().waitFor(3);
			getCommand().click(Down_Arrow);
			log("Navigated to Owners Manual Page",LogType.STEP);
			getCommand().isTargetVisible(Link_OwnerManual);
			getCommand().click(Link_OwnerManual);
		
			/*if(getCommand().getPageUrl().contains("home-play"))
			{
				log("Navigated to Job site Page",LogType.STEP);
			}
			else
			{
				log("Was unable to navigated to Home & Play Page" + getCommand().getPageUrl() ,LogType.STEP);
				Assert.fail("Was unable to navigated to Home & Play Page");
			}
			
		
			getCommand().waitForTargetVisible(Hamburger_Menu);
			getCommand().click(Hamburger_Menu);
			getCommand().waitFor(3);
			getCommand().click(Down_Arrow);
			getCommand().isTargetVisible(Link_OwnerManual);
			getCommand().click(Link_OwnerManual);*/
		
			if(getCommand().getPageUrl().contains("owners-manuals"))
			{
				log("Navigated to owners manuals Page",LogType.STEP);
			}
			else
			{
				log("Was unable to navigated to owners manuals Page" + getCommand().getPageUrl() ,LogType.STEP);
				Assert.fail("Was unable to navigated to owners manuals Page");
			}
			
			
			List<WebElement> Pdfs = getCommand().driver.findElements(By.xpath("//*[@id='product-matrix']/div/ul/li"));
			int count = Pdfs.size();
			
			for (int i = 1; i <= count; i++) 
			{
				
				String PdfText = getCommand().driver.findElement(By.xpath("//*[@id='product-matrix']/div/ul/li["+i+"]//following::h3[1]")).getText();
				log("PDF with PDF text is :"+ PdfText ,LogType.STEP);
				WebElement DwnLink = getCommand().driver.findElement(By.xpath("//*[@id='product-matrix']/div/ul/li["+i+"]//following::p[3]/a"));
				
				
				int Win_size = getCommand().driver.getWindowHandles().size();
				log("Verify the size of open windows currently: "+ Win_size,LogType.STEP);
				
				log("click on each link and verify in new tab",LogType.STEP);
				DwnLink.click();
			
				getCommand().waitFor(10);
				
				ArrayList<String> tabs2 = new ArrayList<String> (getCommand().driver.getWindowHandles());
				
				getCommand().driver.switchTo().window(tabs2.get(1));
				Win_size = getCommand().driver.getWindowHandles().size();
			    Assert.assertEquals(2, Win_size, "On clicking on link, the page is not opned in new window");
			   /* getCommand().switchToWindow(windowTitle);
				
			    if(getCommand().driver.getCurrentUrl().contains("pdf"))
				{
					log("Page is navigated to PDF page with download option" ,LogType.STEP);
				}
				else
				{
					log("Page is not navigated to PDF page " ,LogType.STEP);
					Assert.fail("Page is not navigated to PDF page");
				}
				*/
				
				getCommand().driver.close();
				log("Close the tab and switch back to parent window",LogType.STEP);
				getCommand().driver.switchTo().window(tabs2.get(0));
				
				
			}
			
			
		
	}catch(Exception ex)
	{
		ex.getMessage();
	}
	

	return this;
		}
			
	// Verify breadcrumbs in product category Page
	public Hippo_PortablePage VerifyBreadcrumbs()
	{
		try {
				log("Verifying Job site Page",LogType.STEP);
				getCommand().isTargetVisible(Link_Portable_jobSite);
				getCommand().click(Link_Portable_jobSite);
				
				if(getCommand().getPageTitle().contains("job-site"))
				Assert.assertTrue(true, "Successfully in job site Page");
			
				log("Verifying Generator Page",LogType.STEP);
				getCommand().isTargetVisible(Link_Portable_generator);
				getCommand().click(Link_Portable_generator);
				
				if(getCommand().getPageTitle().contains("generators"))
				Assert.assertTrue(true, "Successfully in generators Page");
			
				log("Verify the breadcrumb navigation and get the size",LogType.STEP);
				List<WebElement> breadcrumbs =  getCommand().driver.findElements(By.xpath("//div[@class='breadcrumbs container clearfix']/ul/li"));
				int count = breadcrumbs.size();
				
				log("Verify breadcrumbs last text",LogType.STEP);
				String breadcrumbs_Page = getCommand().driver.findElement(By.xpath("//div[@class='breadcrumbs container clearfix']/ul/li["+count+"]")).getText();
				
				log("retieve the page url",LogType.STEP);
				String PageURL = getCommand().getPageUrl();
				String LastString = PageURL.substring(PageURL.lastIndexOf("/"), PageURL.length());
				String Page_LstStr = LastString.replace("/", "");
			
				if(Page_LstStr.equalsIgnoreCase(breadcrumbs_Page))
				{
					log("breadcrumbs last text is same as page Url last text",LogType.STEP);
					Assert.assertTrue(true, "Breadcrumbs shows correct page");
				}
				else
				{	log("breadcrumbs last text is not same as page Url last text",LogType.STEP);
					Assert.fail(Page_LstStr+"is not matching with "+breadcrumbs_Page+"description"); 
				}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return this;
	}
	
	// verify Slider filter in product category Page
	public Hippo_PortablePage verifyProductCategorySlider()
	{
		ArrayList<String> KW_values = new ArrayList<String>();
		ArrayList<String> MSRP_values = new ArrayList<String>();
		try {
			
				getCommand().isTargetVisible(Hamburger_Menu);
				getCommand().click(Hamburger_Menu);
				getCommand().waitFor(3);
				log("Verifying Generator Page",LogType.STEP);
				getCommand().click(JobSite_Arrow);
				getCommand().isTargetVisible(Link_Portable_generator);
				getCommand().click(Link_Portable_generator);
			
				if(getCommand().getPageTitle().contains("generators"))
					Assert.assertTrue(true, "Successfully in generators Page");
				
				
				log("Verifying that slider is visble in generators Page ",LogType.STEP);
				
				getCommand().driver.findElement(By.xpath("//*[@id='product-filter-trigger']")).click();
				getCommand().driver.findElement(By.xpath("//*[@id=\"product-filters\"]/div[1]/h5")).click();
				WebElement KW_lefthand = getCommand().driver.findElement(By.xpath("//*[@id='kwSlider']/a[1]"));
				Actions action = new Actions(getCommand().driver);
				
				log("drag the slider to certain range",LogType.STEP);
				action.dragAndDropBy(KW_lefthand, 100, 380).release().build().perform();
				KW_lefthand.click();
	        
				getCommand().waitFor(5);
				getCommand().isTargetVisible(Slider_Portables_KW_Range);
				String sliderRange = getCommand().getText(Slider_Portables_KW_Range);
				log("Retrieve the slider min range by draging the slider and get the range value: " + sliderRange,LogType.STEP);
	        
				log("Verify that Product category grid is loaded based on slider range",LogType.STEP);
				List<WebElement> KW_products = getCommand().driver.findElements(By.xpath("//div[@id='results-container']//following-sibling::div[@class='products-wrap']/ul/li"));
	        
				for (int i = 1; i <= KW_products.size(); i++)
				{
					WebElement product_MaxPower = getCommand().driver.findElement(By.xpath("//div[@id='results-container']//following-sibling::div[@class='products-wrap']/ul/li["+i+"]/div[@class='product-info clearfix']/div[@class='product-specs']/div/ul/li"));
					log("Get the Max power text of product:" + product_MaxPower.getText(),LogType.STEP);
					KW_values.add(product_MaxPower.getText());
				}
	        
				Hippo_PortablePage.verifyStringRange(sliderRange, KW_values);
				
				log("Verifying that slider is visble in generators Page ",LogType.STEP);
				getCommand().driver.findElement(By.xpath("//*[@id=\"product-filters\"]/div[3]/h5")).click();
				WebElement MSRP_lefthand = getCommand().driver.findElement(By.xpath("//*[@id='msrpSlider']/a[1]"));
				action = new Actions(getCommand().driver);
				
				log("drag the slider to certain range",LogType.STEP);
				action.dragAndDropBy(MSRP_lefthand, 100, 1047).release().build().perform();
				MSRP_lefthand.click();
	        
				getCommand().waitFor(5);
				getCommand().isTargetVisible(Slider_Portables_MSRP_Range);
				sliderRange = getCommand().getText(Slider_Portables_MSRP_Range);
				log("Retrieve the slider min range by draging the slider and get the range value: " + sliderRange,LogType.STEP);
	        
				log("Verify that Product category grid is loaded based on slider range",LogType.STEP);
				List<WebElement> MSRP_products = getCommand().driver.findElements(By.xpath("//div[@id='results-container']//following-sibling::div[@class='products-wrap']/ul/li"));
	        
				for (int i = 1; i <= MSRP_products.size(); i++)
				{
					WebElement product_MaxPower = getCommand().driver.findElement(By.xpath("//div[@id='results-container']//following-sibling::div[@class='products-wrap']/ul/li["+i+"]/div[@class='product-info clearfix']/div[@class='product-specs']/div/ul/li"));
					log("Get the Max power text of product:" + product_MaxPower.getText(),LogType.STEP);
					MSRP_values.add(product_MaxPower.getText());
				}
	        
	          	if(MSRP_products.size() < KW_products.size() )	
	          	{
	          		log("Product category page is updated successfully",LogType.STEP);
	          		Assert.assertTrue(true, "Product category page is updated successfully");
	          	}
	          	else
	          		Assert.fail("Product category page is not updated");
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return this;
	}
	
	// Verify Sorting filter in product category Page
	public Hippo_PortablePage verifyProductCategorySortingFilter()
	{
		try {	
				ArrayList<String> values = new ArrayList<String>();
				
				getCommand().isTargetVisible(Hamburger_Menu);
				getCommand().click(Hamburger_Menu);
				getCommand().waitFor(3);
				
				log("Verifying Generator Page",LogType.STEP);
				getCommand().click(JobSite_Arrow);
				getCommand().isTargetVisible(Link_Portable_generator);
				getCommand().click(Link_Portable_generator);
		
				if(getCommand().getPageTitle().contains("generators"))
					Assert.assertTrue(true, "Successfully in generators Page");
			
				log("Verifying the list of element in sort by drop down",LogType.STEP);
				getCommand().selectDropDown(link_Portables_SortBy, 1);
				getCommand().waitFor(5);
				
				/*List<WebElement> drpElement = getCommand().driver.findElements(By.xpath("//*[@id='sort-options-top']/option"));
				
				for(WebElement val : drpElement)
				{
					String valueIs = val.getText();
					if(valueIs.equals("Name"))
					{
						val.click();
						getCommand().waitFor(5);
						break;
					}
				}
				
				getCommand().waitFor(5);*/
				log("Verifying the updated products after sort in Product category",LogType.STEP);
				List<WebElement> products = getCommand().driver.findElements(By.xpath("//div[@id='results-container']//following-sibling::div[@class='products-wrap']/ul/li"));
		        
				log("Verifying the max power after sort in Product category",LogType.STEP);
		        for (int i = 1; i <= products.size(); i++)
		        {
		        	WebElement product_MaxPower = getCommand().driver.findElement(By.xpath("//div[@id='results-container']//following-sibling::div[@class='products-wrap']/ul/li["+i+"]/div[@class='product-header clearfix']/h2/div/a"));
		            String Name = product_MaxPower.getText();
		            values.add(Name);
				}
		        
		        ArrayList<String> actualValueIs = new ArrayList<String>();
		        actualValueIs.addAll(values);
		        Collections.sort(actualValueIs);
		        
		        // compare the actual and expected Products based on sorting used
		        Hippo_PortablePage.VerifySortByNameIs(values,actualValueIs);		
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return this;
	}
	
	// verify Pagination in Generators Page
	public Hippo_PortablePage verifyProductCategoryPagination()
	{
		try {
				
			
				getCommand().isTargetVisible(Hamburger_Menu);
				getCommand().click(Hamburger_Menu);
				getCommand().waitFor(3);
				getCommand().isTargetVisible(JobSite_Arrow);
				getCommand().click(JobSite_Arrow);
				
				log("Verifying Generator Page",LogType.STEP);
				getCommand().isTargetVisible(Link_Portable_generator);
				getCommand().click(Link_Portable_generator);
				if(getCommand().getPageTitle().contains("generators"))
					Assert.assertTrue(true, "Successfully in generators Page");
				

				//log("Verifying the total page count",LogType.STEP);
				//getCommand().isTargetVisible(Text_Portables_PageCount);
				//String InttotalCount= getCommand().getText(Text_Portables_PageCount);
				
				log("Verifying total page count with dropdown count",LogType.STEP);
				
					WebElement previous = getCommand().driver.findElement(By.xpath("//*[@id=\"paging-bottom-container\"]/div[2]/div[1]/a"));
					
					WebElement next = getCommand().driver.findElement(By.xpath("//*[@id='paging-bottom-container']/div[2]/div[2]/a"));
					System.out.println(next.isEnabled());
				if(next.isEnabled()) {
					next.click();
					
					
				}
				else if(previous.isEnabled()) {
					previous.click();
				}
				else {
					log("Previous or next links are disabled due to less number of products",LogType.STEP);
				}
					
				
						
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return this;
	}

	// Verify product modules 
	public Hippo_PortablePage verifyProductCategoryProductModules()
	{
		try {
				
				getCommand().isTargetVisible(Hamburger_Menu);
				getCommand().click(Hamburger_Menu);
				getCommand().waitFor(3);
				
				System.out.println(getCommand().isClickable(JobSite_Arrow));
				
				getCommand().isTargetVisible(JobSite_Arrow);
				getCommand().click(JobSite_Arrow);
				
	
				//if(getCommand().getPageTitle().contains("job-site"))
				//	Assert.assertTrue(true, "Successfully in job site Page");

				log("Verifying Generator Page",LogType.STEP);
				getCommand().isTargetVisible(Link_Portable_generator);
				getCommand().click(Link_Portable_generator);
				

				if(getCommand().getPageTitle().contains("generators"))
					Assert.assertTrue(true, "Successfully in generators Page");
				
				getCommand().waitFor(5);
				List<WebElement> Products =  getCommand().driver.findElements(By.xpath("//div[@class='products-wrap']/ul/li"));
				int count = Products.size();
				
				for (int i = 1; i <= count; i++)
				{
					String ProductTitle = getCommand().driver.findElement(By.xpath("//div[@class='products-wrap']/ul/li["+i+"]/div/h2/div/a")).getText();
					log("Verifying Product Title in Product category Page:" + ProductTitle,LogType.STEP);
					String ProductPrice = getCommand().driver.findElement(By.xpath("//div[@class='products-wrap']/ul/li["+i+"]/div/div/div[@class='price']")).getText();
					log("Verifying Product Price in Product category Page:" + ProductPrice,LogType.STEP);
					boolean image_Product =  getCommand().driver.findElement(By.xpath("//div[@class='products-wrap']/ul/li["+i+"]/div[2]//following::img")).isDisplayed();
					log("Verifying image_Product is available in Product category Page:" + image_Product,LogType.STEP);
					String MaxPower = getCommand().driver.findElement(By.xpath("//div[@class='products-wrap']/ul/li["+i+"]/div[@class='product-info clearfix']//following::div[@class='resources']/ul/li[1]")).getText();
					log("Verifying MaxPower is available in Product category Page:" + MaxPower,LogType.STEP);
					String ContinuousPower = getCommand().driver.findElement(By.xpath("//div[@class='products-wrap']/ul/li["+i+"]/div[@class='product-info clearfix']//following::div[@class='resources']/ul/li[2]")).getText();
					log("Verifying ContinuousPower is available in Product category Page:" + ContinuousPower,LogType.STEP);
					String FuelType = getCommand().driver.findElement(By.xpath("//div[@class='products-wrap']/ul/li["+i+"]/div[@class='product-info clearfix']//following::div[@class='resources']/ul/li[3]")).getText();
					log("Verifying FuelType is available in Product category Page:" + FuelType,LogType.STEP);
				}
				
				/*log("Verifying Find a dealer link and click on it",LogType.STEP);
				getCommand().driver.findElement(By.xpath("//div[@class='products-wrap']/ul/li[1]/div[@class='product-info clearfix']//following::div[@class='resources']/div[@class='view-detail'][1]/a")).click();
				
				log("Verifying that page is navigated to repsective page",LogType.STEP);
				getCommand().waitFor(10);
				getCommand().waitForTargetVisible(Text_FindADealer_Title, 10);
				if(getCommand().isTargetVisible(Text_FindADealer_Title))
					Assert.assertTrue(true, "Successfully in Find a dealer Page");
				
				log("Verifying that user is navigated to generator page again",LogType.STEP);
				getCommand().driver.navigate().to("https://preprod.kohlerpower.kohler.com/en/powerequipment/job-site/products/generators");
				
				log("Verifying Page title in generator page",LogType.STEP);
				getCommand().waitForTargetVisible(Text_GeneratorsTitle, 10);
				if(getCommand().getPageTitle().contains("Job Site - Generators"))
					Assert.assertTrue(true, "Successfully in generators Page");
				 * 
				 */
				
				log("Verifying More details link and click on it",LogType.STEP);
				getCommand().driver.findElement(By.xpath("//div[@class='products-wrap']/ul/li[1]/div[@class='product-info clearfix']//following::div[@class='resources']/div[@class='view-detail'][2]/a")).click();
				
				log("Verifying that page is navigated to repsective page",LogType.STEP);
				getCommand().waitForTargetVisible(Text_GeneratorViewresults, 10);
				if(getCommand().isTargetPresent(Text_GeneratorViewresults))
					Assert.assertTrue(true, "Successfully in results Page");
				
				log("Verifying that user is navigated to generator page again",LogType.STEP);
				getCommand().driver.navigate().to("https://preprod.kohlerpower.kohler.com/en/powerequipment/job-site/products/generators");
				
				log("Verifying that page is navigated to repsective page",LogType.STEP);
				getCommand().waitForTargetVisible(Text_GeneratorsTitle, 10);
				if(getCommand().getPageTitle().contains("Job Site - Generators"))
					Assert.assertTrue(true, "Successfully in generators Page");		
				
				
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return this;
	}
	
	// Verify Custom Kits page
	public Hippo_PortablePage VerifyCustomKits()
	{
		try {
				log("Verifying custom kits Page",LogType.STEP);
				getCommand().isTargetVisible(Hamburger_Menu);
				getCommand().click(Hamburger_Menu);
				getCommand().isTargetVisible(Link_Portable_CustomKits);
				getCommand().click(Link_Portable_CustomKits);

				if(getCommand().getPageTitle().contains("custom-kits"))
					Assert.assertTrue(true, "Successfully in Custom Kits Page");
			
				List<WebElement> thumbnails = getCommand().driver.findElements(By.xpath("//*[@id='asset-1365055281271']//following::dl/dt"));
				List<WebElement> Mainimages = getCommand().driver.findElements(By.xpath("//*[@class='content-image wrapper']//following-sibling::img"));
				int count1 = Mainimages.size();
				int count = thumbnails.size();
				if(count==count1)
				{
					Assert.assertEquals(count, count1 ,"Both the images are of same size");
					
					for (int i = 1; i <=count; i++)
					{
						WebElement thumbnailIcon = getCommand().driver.findElement(By.xpath("//*[@id='asset-1365055281271']//following::dl/dt["+i+"]/div"));
						System.out.println(thumbnailIcon.getText());
						thumbnailIcon.click();
						
						String thumbnail = getCommand().driver.findElement(By.xpath("//*[@id='asset-1365055281271']//following::dl/dt["+i+"]")).getAttribute("class");
						String mainimage = getCommand().driver.findElement(By.xpath("//*[@class='content-image wrapper']//following-sibling::img["+i+"]")).getAttribute("class");
							if(thumbnail.contains("active")==mainimage.contains("active"))
							{
								Assert.assertTrue(true, "Thumbnail image is displayed as main Image");
							}
					}
					
					getCommand().driver.findElement(By.xpath("//*[@id='asset-1365055281271']//following-sibling::a[@class='btn']")).click();
					
					if(getCommand().driver.getTitle().contains("products"))
					{
						Assert.assertTrue(true, "Successfully in Products Category Page");
					}	
				}	
		}
		catch(Exception ex)
		{
			ex.getMessage();
		}
		return this;
	}
	
	public Hippo_PortablePage  VerifyHippoPortable_HeroImage() throws InterruptedException
    {
		try
        {
              String HeroImage1 = "(//div[@class='hero-image'])[1]/img";
              String HeroImage2 = "(//div[@class='hero-image'])[2]/img";
              String HeroImage3 = "(//div[@class='hero-image'])[3]/img";
              String HeroImage4 = "(//div[@class='hero-image'])[4]/img";
              String HeroImage5 = "(//div[@class='hero-image'])[5]/img";
              String HeroImage6 = "(//div[@class='hero-image'])[6]/img";
        
              
              log("Verifying hero image Autorotate functionality",LogType.STEP);
              ArrayList<String> Heroimagelst = new ArrayList<String>();
              String Heroimagetext1= getCommand().driver.findElement(By.xpath(HeroImage1)).getText();
              Heroimagelst.add(Heroimagetext1);
              getCommand().waitFor(5);
              String Heroimagetext2= getCommand().driver.findElement(By.xpath(HeroImage2)).getText();
              Heroimagelst.add(Heroimagetext2);
              getCommand().waitFor(5);
              String Heroimagetext3= getCommand().driver.findElement(By.xpath(HeroImage3)).getText();
              Heroimagelst.add(Heroimagetext3);
              
              getCommand().waitFor(5);
              String Heroimagetext4= getCommand().driver.findElement(By.xpath(HeroImage4)).getText();
              Heroimagelst.add(Heroimagetext4);
              
              getCommand().waitFor(5);
              String Heroimagetext5= getCommand().driver.findElement(By.xpath(HeroImage5)).getText();
              Heroimagelst.add(Heroimagetext5);
              getCommand().waitFor(5);
              String Heroimagetext6= getCommand().driver.findElement(By.xpath(HeroImage6)).getText();
              Heroimagelst.add(Heroimagetext6);
              getCommand().waitFor(5);
              
              if((Heroimagelst.get(0)!=Heroimagelst.get(1)) && (Heroimagelst.get(1)!=Heroimagelst.get(2))  && (Heroimagelst.get(2)!=Heroimagelst.get(3))  && (Heroimagelst.get(3)!=Heroimagelst.get(4) && (Heroimagelst.get(5)!=Heroimagelst.get(0))))
              {	
            	  log("Autorotate functionality is working",LogType.STEP);
              }
              else
              {
            	  log("Autorotate functionality is not working",LogType.ERROR_MESSAGE);
            	  Assert.fail("Autorotate functionality is not working");
              }

              getCommand().driver.findElement(By.xpath(HeroImage1)).isDisplayed();
              log("Clicking on Navigation dot2 to get second hero image",LogType.STEP);
              getCommand().driver.findElement(By.xpath("//ul[@class='hero-nav']/li[2]")).click();
              getCommand().waitFor(10);
              getCommand().driver.findElement(By.xpath(HeroImage2)).isDisplayed();
              if(!(HeroImage2==HeroImage1))
              {
            	  log("Hero Image changed After click on Navigation dots",LogType.STEP);
              }	
              else
              {
            	  log("Hero Image remains unchanged After click on Navigation dots",LogType.ERROR_MESSAGE);
            	  Assert.fail("Hero Image remains unchanged After click on Navigation dots");
              }

              log("Clicking on Navigation dot3 to get third hero image",LogType.STEP);
              getCommand().driver.findElement(By.xpath("//ul[@class='hero-nav']/li[3]")).click();
              getCommand().waitFor(10);
              getCommand().driver.findElement(By.xpath(HeroImage3)).isDisplayed();
              if(!(HeroImage3==HeroImage2))
              {
            	  log("Hero Image changed After click on Navigation dots",LogType.STEP);
              }
              else
              {
            	  log("Hero Image remains unchanged After click on Navigation dots",LogType.ERROR_MESSAGE);
            	  Assert.fail("Hero Image remains unchanged After click on Navigation dots");
              }

              log("Clicking on Navigation dot4 to get fourth hero image",LogType.STEP);
              getCommand().driver.findElement(By.xpath("//ul[@class='hero-nav']/li[4]")).click();
              getCommand().waitFor(10);
              getCommand().driver.findElement(By.xpath(HeroImage4)).isDisplayed();
              if(!(HeroImage4==HeroImage3))
              {
            	  log("Hero Image changed After click on Navigation dots",LogType.STEP);
              }
              else
              {
            	  log("Hero Image remains unchanged After click on Navigation dots",LogType.ERROR_MESSAGE);
            	  Assert.fail("Hero Image remains unchanged After click on Navigation dots");
              }

              log("Clicking on Navigation dot5 to get fifth hero image",LogType.STEP);
              getCommand().driver.findElement(By.xpath("//ul[@class='hero-nav']/li[5]")).click();
              getCommand().waitFor(10);
              getCommand().driver.findElement(By.xpath(HeroImage5)).isDisplayed();
              if(!(HeroImage5==HeroImage4))
              {
            	  log("Hero Image changed After click on Navigation dots",LogType.STEP);
              }
              else
              {
            	  log("Hero Image remains unchanged After click on Navigation dots",LogType.ERROR_MESSAGE);
            	  Assert.fail("Hero Image remains unchanged After click on Navigation dots");
              }
              
              log("Clicking on Navigation dot6 to get fifth hero image",LogType.STEP);
              getCommand().driver.findElement(By.xpath("//ul[@class='hero-nav']/li[6]")).click();
              getCommand().waitFor(10);
              getCommand().driver.findElement(By.xpath(HeroImage6)).isDisplayed();
              if(!(HeroImage6==HeroImage5))
              {
            	  log("Hero Image changed After click on Navigation dots",LogType.STEP);
              }
              else
              {
            	  log("Hero Image remains unchanged After click on Navigation dots",LogType.ERROR_MESSAGE);
            	  Assert.fail("Hero Image remains unchanged After click on Navigation dots");
              }

              log("Clicking on Navigation dot1 to get first hero image",LogType.STEP);
              getCommand().driver.findElement(By.xpath("//ul[@class='hero-nav']/li[1]")).click();
              getCommand().waitFor(10);
              getCommand().driver.findElement(By.xpath(HeroImage1)).isDisplayed();
              if(!(HeroImage1==HeroImage6))
              {
            	  log("Hero Image changed After click on Navigation dots",LogType.STEP);
              }
              else
              {
            	  log("Hero Image remains unchanged After click on Navigation dots",LogType.ERROR_MESSAGE);
            	  Assert.fail("Hero Image remains unchanged After click on Navigation dots");
              }
        }
        catch(Exception e)
        {
              Assert.fail(e.getMessage());
        }
        return this;
    }
	
	public Hippo_PortablePage Portables_WorldWide() throws InterruptedException
    {
	
			 try
			 {
				 pageLoad();
				 List<String> WorldWideBannerRegionsList = new ArrayList<String>();                  
				 int  WorldWideBanner_Columnscount = 0;
				 
				 log("Checking worldwide banner for Industrial Site",LogType.STEP);
				 log("Clicking on KOHLER Industrial Banner",LogType.STEP);
		                  
				 getCommand().isTargetPresent(KohlerIndustrialWorldWide);
				 getCommand().click(KohlerIndustrialWorldWide);
				 
				 log("Verifying WorldWide Banner is composed of 7 columns",LogType.STEP);

				 List<WebElement> WorldWideBanner = getCommand().driver.findElements(By.xpath("//*[@id='gb-8a9t0w5t2-c--content-section--mobile']/div"));
				 
				 for(WebElement WorldWidebanner : WorldWideBanner)
				 {
					 if(WorldWidebanner.isDisplayed())
					 {
						 WorldWideBanner_Columnscount++;
					 }
				 }

				 if(WorldWideBanner_Columnscount == 7)
					 
				 {
					 for(int i=1;i<=WorldWideBanner_Columnscount;i++) {
						 
						 System.out.println("main heading..."+i);
						 log("WorldWide Banner is displayed with 7 columns",LogType.STEP);
						 log("Checking WorldWide Banner is displayed with 7 different regions",LogType.STEP);
						
						 
						 List<WebElement> WorldWideBannerRegions = getCommand().driver.findElements(By.xpath("//*[@id='gb-8a9t0w5t2-c--content-section--mobile']/div"));
						 log("Getting regions text and verifying all are of different regions",LogType.STEP);
						 
						 for (WebElement WorldWideBannerRegion : WorldWideBannerRegions)
						 {
							 WorldWideBannerRegionsList.add(WorldWideBannerRegion.getText());
						 }
			                 
						// Assert.assertTrue(CompareDataFromSameList(WorldWideBannerRegionsList),"WorldWide Banner is not displayed with 7 different regions");
						 log("WorldWide Banner is displayed with 7 different regions",LogType.STEP);                 
						 getCommand().driver.findElement(By.xpath("//*[@id='gb-8a9t0w5t2-c--content-section--mobile']/div["+i+"]")).click();
						 
						Target countries = new Target("countries","(//*[@id='gb-8a9t0w5t2-c--content-section--mobile']/div["+i+"]/div/ul/li/a)",Target.XPATH);
						int count=getCommand().getTargetCount(countries);
						
						 for(int j=1;j<=count;j++) {
							 System.out.println("countries clicked..."+j);
							 Target country_Link =new Target("country_Link","(//*[@id='gb-8a9t0w5t2-c--content-section--mobile']/div["+i+"]/div/ul/li/a)["+j+"]",Target.XPATH);				 
							 String Pageurl = getCommand().getPageUrl();			
							 log("Clicking on various links and verifying they are Navigated as expected",LogType.STEP);
								 String Linktext =getCommand().getText(country_Link);			 
								 log("Clicking and opening the link "+Linktext+ " in new tab",LogType.STEP); 				 
								 getCommand().sendKeys(country_Link, Keys.chord(Keys.CONTROL,Keys.RETURN));
								 getCommand().waitFor(5);
								 ArrayList<String> listofTabs = new ArrayList<String> (getCommand().driver.getWindowHandles());
								 log("Switching to new tab",LogType.STEP);
								 
								 getCommand().driver.switchTo().window(listofTabs.get(1));
								 log("Getting new tab page title",LogType.STEP);
								 String CurrentpageUrl = getCommand().getPageUrl();
								 
								 if(Pageurl.equals(CurrentpageUrl))
								 {	
									 log("Clicking on link "+Linktext+" is not redirecting to the corresponding page",LogType.ERROR_MESSAGE);
									 Assert.fail("Clicking on link "+Linktext+" is not redirecting to the corresponding page");
								 }
								 else
								 {                                             
									 log("Clicking on link "+Linktext+" is redirecting to the corresponding page",LogType.STEP);
								 }
								 getCommand().driver.close();
								 getCommand().driver.switchTo().window(listofTabs.get(0));
								 
								 
					 } 
						// getCommand().driver.findElement(By.xpath("//*[@id='gb-8a9t0w5t2-c--content-section--mobile']/div["+i+"]")).click();
				
					 }
				 }
				 
				 
		}   
		catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
			 return this;
	
    }

	//Helpers
	
	//Compare Data from Lists
	public boolean CompareDataFromSameList(List<String> list)
	{
		for (int i = 0; i < list.size()-1; i++) 
		{
			for (int k = i+1; k < list.size(); k++) 
			{			
				if(list.get(i).equals(list.get(k)))
				{
					Assert.fail("Mismatch in data present in the list");
				}				      
			}	      
		}		
		return this != null;
	}
	
	// Compare min power range and product in product category 
		public static boolean verifyStringRange(String value, List<String> values)
		{
			boolean status = false;
			
			// Get the min power range from the slider and split to get the exact value
			String[] rangeVal = value.split(" ");
			String MinRangeVal =rangeVal[0];
			System.out.println(MinRangeVal);
			
			// loop the list to get the product power range and iterate through each to get the value
			for (int i = 0; i < values.size(); i++)
	        {	
	        	String[] val = values.get(i).split("\n");
	        	String val1 =val[1].toString();
	        	System.out.println(val1);
	        	
	        	String[] finalVal = val1.split(" ");
	        	String finalValueIs = finalVal[0];
	        	
	        	System.out.println(finalValueIs);
	        	
	        	// compare slider range with product power range and return the value
	        	int resultsIs = finalValueIs.compareTo(MinRangeVal);
	        	if(resultsIs == 0 || resultsIs >0)
	        		status = true;
			}			
			return status;
		}

		// verify that product are displayed in alphabetical order when sorted by NAME
		public static boolean VerifySortByNameIs(List<String> ExpectedVal ,List<String> ActualVal)
		{
			boolean status = false;
			if(ExpectedVal.size() ==  ActualVal.size())
			{
				if(ExpectedVal.equals(ActualVal))
					status = true;
			}		
			return status;
		}
		
	//Compare Tabs
	public Hippo_PortablePage CompareTabs(List<String> expectedList, List<String> actualList)
	{
		if (expectedList.size() == actualList.size())
		{
			for (int i=0; i < actualList.size(); i++)
			{
				if (!expectedList.get(i).equals(actualList.get(i)))
				{
					Assert.fail("Mismatch in product Tabs");
				}
			}
		}	
		return this;
	}
	
	public void FluentWait(Target ele)
	{
		
		// Waiting 30 seconds for an element to be present on the page, checking
		// for its presence once every 5 seconds.
		Wait<WebDriver> wait = new FluentWait<WebDriver>(getCommand().driver)
		    .withTimeout(80, TimeUnit.SECONDS)
		    .pollingEvery(5, TimeUnit.SECONDS)
		    .ignoring(NoSuchElementException.class);

		getCommand().isTargetVisible(ele);
	}
	
	//PageLoad
		public Hippo_PortablePage pageLoad()
		{
			try
			{
				JavascriptExecutor js = (JavascriptExecutor) getCommand().driver;
				String test = js.executeScript("return document.readyState").toString();
				
				while (!test.equalsIgnoreCase("complete"))
				{
					getCommand().waitFor(1);
					test = js.executeScript("return document.readyState").toString();
				}
			}
			catch (Exception ex)
			{
				Assert.fail(ex.getMessage());
			}
			return this;
		}
}