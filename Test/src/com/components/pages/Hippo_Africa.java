package com.components.pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.openqa.selenium.Keys;

import com.components.repository.SiteRepository;
import com.iwaf.framework.components.Target;
import com.iwaf.framework.components.IReporter.LogType;

public class Hippo_Africa extends SitePage{
	
	//Defining locators in Hippo Africa Page	
	public static final Target HeroPrevArrow  = new Target("HeroPrevArrow","//*[@id='myCarousel']/a/button[@class='slick-prev']",Target.XPATH);
	public static final Target HeroNextArrow  = new Target("HeroNextArrow","//*[@id='myCarousel']/a/button[@class='slick-next']",Target.XPATH);
	public static final Target AfricaMap  = new Target("AfricaMap","//*[@class='map']/img",Target.XPATH);
	public static final Target AfricaMap_Country  = new Target("AfricaMap_Country","//*[@id='Kenya']/ol/li/p[2]",Target.XPATH);
	public static final Target AfricaMap_Country_Attribute  = new Target("AfricaMap_Country_Attribute","//*[@id='Kenya']",Target.XPATH);
	public static final Target OurBrands  = new Target("OurBrands","//*[@id='main-nav__buttons']/li[5]/ul/li[2]",Target.XPATH);
	public static final Target WhereToBuy = new Target("WhereToBuy","//*[@id=\"main-nav__buttons\"]/li[5]/ul/li[1]/a",Target.XPATH);
	public static final Target Hero = new Target("Hero","//*[@id='myCarousel']/div",Target.XPATH);
	public static final Target PromoModules =  new Target("PromoModules","//*[@class='row main-content']/div/div/div/div[2]/div",Target.XPATH);
	public static final Target Footer = new Target("Footer","//*[@id='footer']/div[1]/div",Target.XPATH);
	public static final Target Literature = new Target("Literature","//*[@id=\"main-nav__buttons\"]/li[2]/a",Target.XPATH);
	public static final Target Literature_Header = new Target("Literature_Header","/html/body/div[1]/div[2]/div/div/div/div/div/header/h2",Target.XPATH);
	public static final Target Highlights = new Target("Highlights","//*[@id=\"main-nav__buttons\"]/li[3]/a",Target.XPATH);
	public static final Target Highlights_Header = new Target("Highlights_Header","//div[@class='hero-text']/div/h2",Target.XPATH);
	
	//------------------------------------------Mobile xpaths--------------------------------------------------------------
	
	public static final Target Hamburger_Icon  = new Target("Hamburger_Icon","//*[@id='mobile-nav-top__button--menu']",Target.XPATH);
	public static final Target Hero_Next_Arrow  = new Target("Hero_NextBtn","//*[@id='myCarousel']/a[2]/button",Target.XPATH);
	public static final Target Hero_Img  = new Target("Hero_Img","//*[@id='myCarousel']/div/div/div/div/a/img",Target.XPATH);
	public static final Target Hero_Prev_arrow  = new Target("Hero_Prev_arrow","//*[@id='myCarousel']/a[1]/button",Target.XPATH);
	public static final Target OurBrands_Tray  = new Target("OurBrands_Tray","//*[@id=\"main-nav__buttons\"]/li[5]/ul/li[3]/ul",Target.XPATH);
	public static final Target WhereToBuy_Header  = new Target("WhereToBuy_Header","/html/body/div[1]/div[2]/div[1]/div/div/div/article/div/div/div/header/h2",Target.XPATH);
	public static final Target ContactUs  = new Target("ContactUs","//*[@id=\"main-nav__buttons\"]/li[4]/a",Target.XPATH);
	public static final Target KohlerCo  = new Target("KohlerCo","//*[@id=\"footer\"]/div[1]/div/div[2]/button",Target.XPATH);
	
	Actions Action = new Actions(getCommand().driver);

	JavascriptExecutor JS = (JavascriptExecutor)getCommand().driver;
    
    //For getting Browser Name
  	Capabilities caps = ((RemoteWebDriver) getCommand().driver).getCapabilities();
	
    public Hippo_Africa(SiteRepository repository)
	{
		super(repository);
	}

    public Hippo_Africa _GoToHippoAfricaPage() 
	{	  
		log("Navigate to Hippo Africa Page",LogType.STEP);
		return this;	 
	}
    //Verify general layout of Hippo Africa Home Page
    public Hippo_Africa VerifyHomePageLayout()
  	{
    	try
  		{	
  			//String HeroText = getCommand().getAttribute(Hero, "class");
  			getCommand().waitFor(5);
			if(getCommand().isTargetPresent(Hero))
			{
			       log("Hero is displayed",LogType.STEP);
			}
			else
			{
			       log("Hero is not displayed",LogType.ERROR_MESSAGE);
			       Assert.fail("Hero is not displayed");			     
			} 						
	
			//String PromoModulesText = getCommand().getAttribute(PromoModules, "class");
	
			if(getCommand().isTargetPresent(PromoModules))
			{
			       log("PromoModules is displayed",LogType.STEP);
			}
	
			else
			{
			       log("PromoModules is not displayed",LogType.ERROR_MESSAGE);
			       Assert.fail("PromoModules is not displayed");
			}
	
			//String FooterText = getCommand().getAttribute(Footer, "class");
	
			if(getCommand().isTargetPresent(Footer))
			{
			       log("Footer is displayed",LogType.STEP);
			}
	
			else
			{
			       log("Footer is not displayed",LogType.ERROR_MESSAGE);
			       Assert.fail("Footer is not shown");
			}
			
			getCommand().waitForTargetPresent(Hamburger_Icon);
			getCommand().click(Hamburger_Icon);
  			String OurBrandsText = getCommand().getText(OurBrands);
  			
  			OurBrandsText = OurBrandsText.substring(0,10);
  			
  			if(getCommand().isTargetPresent(OurBrands) && OurBrandsText.equals("Our Brands"))
  		    {
  	           	log(OurBrandsText+" is displayed",LogType.STEP);
  		    }
  		    else
  		    {
  		        log(OurBrandsText+" is not displayed",LogType.ERROR_MESSAGE);
  		        Assert.fail(OurBrandsText+"is not displayed");
  		    }			
  			
  			String WhereToBuyText = getCommand().getText(WhereToBuy);
  			
  			
  			if(getCommand().isTargetPresent(WhereToBuy) && WhereToBuyText.equals("Where to Buy"))
  		    {
  	           	log(WhereToBuyText+" is displayed",LogType.STEP);
  		    }
  			else
  		    {
  		        log(WhereToBuyText+" is not displayed",LogType.ERROR_MESSAGE);
  		        Assert.fail(WhereToBuyText+"is not displayed");
  		    }
  			  				
  			
			List<WebElement> GlobalNav = getCommand().driver.findElements(By.xpath("//*[@id='main-nav__buttons']/li"));
			
			
			
			for(int i=1;i<GlobalNav.size();i++)
			{
				String NavText=getCommand().driver.findElement(By.xpath("//*[@id='main-nav__buttons']/li["+i+"]")).getText();
				
					if(!NavText.isEmpty())
					{
						if(NavText.contains("ABOUT US") || NavText.contains("LITERATURE") || NavText.contains("HIGHLIGHTS") || NavText.contains("CONTACT US")) 
						{
							log(NavText+" is displayed in Global Navigation", LogType.STEP);
							log("Correct Text: "+NavText+" is displayed in Global Navigation",LogType.STEP);
						}
						else
						{
							log("Correct Text: "+NavText+" is not displayed in Global Navigation",LogType.ERROR_MESSAGE);
							Assert.fail("Correct Text: "+NavText+" is not displayed in Global Navigation");
						}
					}
					
						
			}	
	}
  	
  catch(Exception ex)
  {
  	Assert.fail(ex.getMessage());
  }
  return this;
}  
    
	//Verify other Kohler brands and Where to Buy display
	public Hippo_Africa VerifyOtherBrands() throws InterruptedException
	{
		try
		{
			getCommand().waitForTargetVisible(Hamburger_Icon).click(Hamburger_Icon);
			getCommand().waitForTargetVisible(OurBrands).click(OurBrands);
			getCommand().waitFor(3);
			log("Verifying Our Brands",LogType.STEP);
			List<WebElement> ourBrands=getCommand().driver.findElements(By.xpath("//*[@id='main-nav__buttons']/li[5]/ul/li[3]/ul/li/a"));
			if(getCommand().isTargetVisible(OurBrands_Tray)) {
				log("Our Brand tray opened",LogType.STEP);
				for(int i=1;i<ourBrands.size();i++) {
				
				Target OtherBrands =new Target("OurBrands","(//*[@id='main-nav__buttons']/li[5]/ul/li[3]/ul/li/a)["+i+"]",Target.XPATH);
				System.out.println(getCommand().isTargetVisible(OtherBrands));
				int Win_size = getCommand().driver.getWindowHandles().size();
				log("Verify the size of open windows currently: "+ Win_size,LogType.STEP);
				log("click on each link and verify in new tab",LogType.STEP);
				getCommand().sendKeys(OtherBrands, Keys.chord(Keys.CONTROL,Keys.RETURN));
				//getCommand().click(OtherBrands);
				getCommand().waitFor(10);
				ArrayList<String> listofTabs = new ArrayList<String> (getCommand().driver.getWindowHandles());
				getCommand().waitFor(1);
				getCommand().driver.switchTo().window(listofTabs.get(1));
				Win_size = getCommand().driver.getWindowHandles().size();
			    Assert.assertEquals(2, Win_size, "On clicking on link, the page is not opned in new window");
			    
				String pageTitle = getCommand().driver.getTitle();
				log("verify the Page title"+ pageTitle,LogType.STEP);
				//System.out.println(pageTitle);
				getCommand().driver.close();
				log("Close the tab and switch back to parent window",LogType.STEP);
				getCommand().driver.switchTo().window(listofTabs.get(0));
			}
				
				
			}
			
			
				
			
			
			
			
			

			
					
					/*if(UtilityBarText.equals("WheretoBuy"))
					{						
						log(UtilityBarText+" is present in utility bar",LogType.STEP);
						log("Clicking on  "+UtilityBarText,LogType.STEP);
						Utilitybar.click();
						getCommand().waitFor(3);
						log("Checking Africa MAP is present in Where to buy page",LogType.STEP);
						if(getCommand().isTargetPresent(AfricaMap))
						{
							String MapText = getCommand().getAttribute(AfricaMap, "alt");
							MapText = MapText.substring(0, MapText.length() - 4);
							log("Where to Buy page displays with an "+MapText,LogType.STEP);
							log("Getting all the countries displayed in the MAP and checking they are highlighted",LogType.STEP);
							List<WebElement> AfricaCountries = getCommand().driver.findElements(By.xpath("//*[@class='stores-map']/div[2]/div/div"));	
							for(WebElement AfricaCountrie:AfricaCountries)
							{
								String color = AfricaCountrie.getCssValue("color"); 
								String backcolor = AfricaCountrie.getCssValue("background-color");
								String id = AfricaCountrie.getAttribute("id");
								id = id.substring(4);
								if(color!=backcolor)
								{
									log("Country "+id.toUpperCase()+" is diplayed and it is hihglighted in the map",LogType.STEP);
								}
								
								else
								{
									log("Country "+id.toUpperCase()+" is diplayed and it is not hihglighted in the map",LogType.ERROR_MESSAGE);
									Assert.fail("Country "+id.toUpperCase()+" is diplayed and it is not hihglighted in the map");
								}
								
							}
							
							log("Click on any country and verify corresponding information displays on a left section",LogType.STEP);
							
							WebElement Country = AfricaCountries.get(0);
							Country.click();
							String id = AfricaCountries.get(0).getAttribute("id");
							id = id.substring(4);
							String DisplayedCountry = getCommand().getText(AfricaMap_Country);
							String Content = getCommand().getAttribute(AfricaMap_Country_Attribute, "class");				
							if(Content.contains("active") && DisplayedCountry.toUpperCase().equals(id.toUpperCase()))
							{
								log("After clicking on country "+id+" corresponding information displays on a left section",LogType.STEP);								
							}
							
							else
							{
								log("After clicking on country "+id+" corresponding information is not displays on a left section",LogType.ERROR_MESSAGE);
								Assert.fail("After clicking on country "+id+" corresponding information is not displays on a left section");
							}						
						}
						*/
					}
		
			
	
		
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	//Verify where to buy link
	public Hippo_Africa VerifyWhereToBuy() throws InterruptedException{
		try {
			getCommand().waitForTargetVisible(Hamburger_Icon).click(Hamburger_Icon);
			getCommand().waitForTargetVisible(WhereToBuy).click(WhereToBuy);
			getCommand().waitFor(3);
			if(getCommand().isTargetVisible(WhereToBuy_Header)) {
				log("Clicking of where to buy link is naviagted to correct page",LogType.STEP);
				getCommand().driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[1]/div/div/div/article/div/div/div/section/div[1]/ul/li/button")).click();
				String country=getCommand().driver.findElement(By.xpath("//*[@id=\"dropdown9-tab\"]")).getText();
				System.out.println("The dropdown values is "+country);
				getCommand().driver.findElement(By.xpath("//*[@id=\"dropdown9-tab\"]")).click();
				String data=getCommand().getText(AfricaMap_Country);
				System.out.println("The address country is "+data);
				if(data.equalsIgnoreCase(country)) {
					log("The information of "+data+" is displayed",LogType.STEP);
				}
				else {
					log("The displayed information of "+data+" is wrong",LogType.STEP);
				}
		}
		}
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	
	//Verify Footer layout
	public Hippo_Africa VerifyFooter()
	{
		log("Verifying footer is composed of 5 columns Links",LogType.STEP);
        // get the list of links available in footer
		getCommand().scrollTo(ContactUs);
        List<WebElement> footer = getCommand().driver.findElements(By.xpath("//*[@id='footer']/div[1]/div/div[@class='col-1-of-5-md col-full-sm']"));
        int  footerlinks_count = footer.size();
        if(footerlinks_count == 5)
        {
        	log("Footer is composed of 5 columns Links",LogType.STEP);
        	List<WebElement> footerHeaders = getCommand().driver.findElements(By.xpath("//*[@id='footer']/div[1]/div/div[@class='col-1-of-5-md col-full-sm']/button"));
        	for(WebElement footerHeader:footerHeaders) 
        	{
        		String HeaderText = footerHeader.getText();
        		log("'"+HeaderText+"' column is displayed under Footer section",LogType.STEP);
        	}
        }
        
        else
        {
        	log("Footer is not composed of 5 columns Links",LogType.ERROR_MESSAGE);
        	Assert.fail("Footer is not composed of 5 columns Links");
        }     
        try 
        {
        	log("Checking the navigation of each link under Footer section",LogType.STEP);
        	for (int j = 1; j <= footerlinks_count; j++) 
        		
            {
        		  String PageUrl= getCommand().getPageUrl();
        		  getCommand().driver.findElement(By.xpath("(//*[@id='footer']/div[1]/div/div[@class='col-1-of-5-md col-full-sm']/button)["+j+"]")).click();
                  List<WebElement> footerheaderlinks = getCommand().driver.findElements(By.xpath("//*[@id='footer']/div[1]/div/div["+j+"]/div/a"));
                
                  int footerheaderlinksCount = footerheaderlinks.size();
                  for(int k=0; k<footerheaderlinksCount;k++)
                  {
                	  List<WebElement> footerheaderLinks = getCommand().driver.findElements(By.xpath("//*[@id='footer']/div[1]/div/div["+j+"]/div/a"));
                	  String Linktext = footerheaderLinks.get(k).getText();
                	  if(!Linktext.equalsIgnoreCase("YouTube")) {
                	  
	                	  log("Clicking on the link "+Linktext,LogType.STEP);
	                	  getCommand().waitFor(1);
	                      JS.executeScript("arguments[0].scrollIntoView(true);", footerheaderLinks.get(k));
	                	  String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN); 
	                	  footerheaderLinks.get(k).sendKeys(selectLinkOpeninNewTab);
	                      getCommand().waitFor(7);
	                      ArrayList<String> listofTabs = new ArrayList<String> (getCommand().driver.getWindowHandles());
	                      if(listofTabs.size()>1)
	      	              {
	                    	  log("Switching to new tab",LogType.STEP);
	                          getCommand().driver.switchTo().window(listofTabs.get(1));
	                          getCommand().waitFor(3);
	                          log("Getting new tab page title",LogType.STEP);
	          	    		  String Currentpageurl = getCommand().getPageUrl();
	          	    		  
	          	    		  if(PageUrl.equals(Currentpageurl))
	          	    		  {	    			
	          	    			log("Clicking on link "+Linktext+" is not redirecting to the corresponding page",LogType.ERROR_MESSAGE);
	          	    			Assert.fail("Clicking on link "+Linktext+" is not redirecting to the corresponding page");
	          	    		  }
	          	    		  else
	          	    		  {    			
	          	    			log("Clicking on link "+Linktext+" is redirecting to the corresponding page",LogType.STEP);
	          	    		  }
	          	    		
	          	    		  getCommand().driver.close();
	          	    		  getCommand().driver.switchTo().window(listofTabs.get(0));
	
	      	              }
                	  }
                      
                    
                  }  	                  
            }
        	
        }catch(Exception ex)
        {
               ex.getMessage();
        }

		return this;
	}
    
	//Verify that Contact Us page can be accessed via Global nav
	public Hippo_Africa VerifyAccessContactUsPage()
	{
		try {
			
			getCommand().waitForTargetVisible(Hamburger_Icon).click(Hamburger_Icon);
			
			getCommand().click(ContactUs);
			getCommand().waitFor(5);
			String Title1=getCommand().driver.getTitle();
			getCommand().driver.navigate().back();
			getCommand().scrollTo(KohlerCo);
			getCommand().driver.findElement(By.xpath("//*[@id=\"footer\"]/div[1]/div/div[3]/button")).click();
			getCommand().driver.findElement(By.xpath("//*[@id=\"footer__tab--3\"]/a[1]")).click();
			getCommand().waitFor(5);
			String Title2=getCommand().driver.getTitle();
			Assert.assertEquals(Title1, Title2,"Mismatch in Page titles for Contact us page");
			log("Contact Us page can be accessed via Global Nav and Footer",LogType.STEP);
	     }		
		catch(Exception ex)
		{
			Assert.fail("Contact Us page cannot be accessed via GLobal Nav and footer");
	    }
        return this;
	}      
    
	//Verify form fields are aligned to the right, and a left section with extra information.
   /* public Hippo_Africa VerifyContactUsAlignment()
     {
     	try {	
    			Point Informtion = getCommand().driver.findElement(By.xpath("//*[@class='col-md-3 col-sm-3 col-xs-12']")).getLocation();
    		    int xcord_Informtion = Informtion.getX();
    			 
    			 Point Form = getCommand().driver.findElement(By.xpath("//*[@id='contact-us']")).getLocation();
    			 int xcord_Form = Form.getX();
    			 
    			 if(xcord_Informtion<xcord_Form)
    			 {
    				 log("Information column is present", LogType.STEP);
    			 }
    			 
    			 else
    			 {
    				 log("Form fields is not on the left", LogType.ERROR_MESSAGE);
    				 Assert.fail("Form fields column is not on the left");
    			 } 				
    	     }		
    		catch(Exception ex)
    		{
    			 Assert.fail("Form fields column is not on the left");
    	    }
      return this;
    	}*/

    //Verify Global nav links
	public Hippo_Africa VerifyGlobalNavigation()
	{
		try
		{
			List<WebElement> GlobalNavigations = getCommand().driver.findElements(By.xpath("//*[@class='nav-btns']/ul/li/a"));
			
			for(WebElement GlobalNavigation : GlobalNavigations)
			{
				String GlobalNavText = GlobalNavigation.getText();
				
				switch(GlobalNavText)
				{    
				   case "ABOUT US":  
					   log(GlobalNavText+" is present at Global Navigation",LogType.STEP);
				   break; 
				   case "LITERATURE":
					   log(GlobalNavText+" is present at Global Navigation",LogType.STEP);
				   break;
				   case "HIGHLIGHTS":
					   log(GlobalNavText+" is present at Global Navigation",LogType.STEP);
				   break;
				   case "CONTACT US":
					   log(GlobalNavText+" is present at Global Navigation",LogType.STEP);
				   break; 
				    
				   default:
					   log("Logo is present at Global Navigation",LogType.STEP);
						String pageTitle = getCommand().driver.getTitle();
						String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN); 
						GlobalNavigation.sendKeys(selectLinkOpeninNewTab);
				        getCommand().waitFor(10);
				        ArrayList<String> listofTabs = new ArrayList<String> (getCommand().driver.getWindowHandles());
				        log("Switching to new tab",LogType.STEP);
				        getCommand().driver.switchTo().window(listofTabs.get(1));
		                String CurrentpageTitle = getCommand().driver.getTitle();
		                
		                if(CurrentpageTitle.equals(pageTitle))
		                {
		                	log("Clicking on Logo is redirecting to same current page",LogType.STEP);
		                }                
		                else
		                {
		                	log("Clicking on Logo is not redirecting to same current page",LogType.ERROR_MESSAGE);
		                	Assert.fail("Clicking on Logo is not redirecting to same current page");
		                }
		                getCommand().driver.close();
		          		getCommand().driver.switchTo().window(listofTabs.get(0));
				}
			}
		}
		
		catch(Exception ex) 
		{
			Assert.fail(ex.getMessage());
		}
		
		return this;
	}
	
	//Verify Literature page
	public Hippo_Africa LiteraturePage()
	{	
		try
		{
			log("Clicking on Literature Page",LogType.STEP);
			getCommand().waitForTargetVisible(Hamburger_Icon).click(Hamburger_Icon);
			getCommand().isTargetPresent(Literature);
			getCommand().click(Literature);
			
			getCommand().waitForTargetVisible(Literature_Header, 120);

	        List<WebElement> Modules = getCommand().driver.findElements(By.xpath("//*[@class='feature-section']/div/div/figure"));
	        int ModulesCount =  Modules.size();
	        log(ModulesCount+ " modules present in a literature page",LogType.STEP);
	        log("Checking each modules in a literature page has a PDF link",LogType.STEP);
	        int PDFCounter = 0;
	        for(int i=1; i<=ModulesCount;i++)
	        {
	        	String Text = getCommand().driver.findElement(By.xpath("//*[@class='feature-section']/div/div/figure["+i+"]/article/img")).getAttribute("title");
	        	
	        	WebElement PDF = getCommand().driver.findElement(By.xpath("//*[@class='feature-section']/div/div/figure["+i+"]/article/figcaption/footer/a"));
	        	
	        	String Href = PDF.getAttribute("href");
	        	
	        	log("Checking module "+Text+" in a literature page has a PDF link",LogType.STEP);
	        	
	        	if(PDF.isDisplayed() || Href.endsWith(".pdf"))
	        	{
	        		log("For module "+Text+" PDF link is present",LogType.STEP);
	        		PDFCounter++;      		
	        	}
	        	
	        	else
	        	{
	        		log("For module "+Text+" no PDF link is present",LogType.ERROR_MESSAGE);
	        		Assert.fail("For module "+Text+" no PDF link is present");
	        	}
	        	
	        }
	        
	        if(PDFCounter == ModulesCount)
	        {
	        	log("Each module has a PDF link",LogType.STEP);     	
	        }
	        
	        else
	        {
	        	log("Some module doesn't has PDF link",LogType.ERROR_MESSAGE);  
	        	Assert.fail("Some module doesn't has PDF link");
	        }
		}
		catch(Exception ex) 
		{
			Assert.fail(ex.getMessage());
		}
		return this;
	}	
	
	//Verify Highlights page
	public Hippo_Africa HighlightsPage()
	{	
		try
		{
			log("Clicking on Highlights Page",LogType.STEP);
			
			getCommand().waitForTargetVisible(Hamburger_Icon).click(Hamburger_Icon);
			getCommand().isTargetPresent(Highlights);
			getCommand().click(Highlights);			
			getCommand().waitForTargetVisible(Highlights_Header, 120);
			
	        log("Getting Total no. of modules present in a page",LogType.STEP);
	        List<WebElement> ModulesList = getCommand().driver.findElements(By.xpath("//*[@class = 'feature-box-title']/a"));
	        int ModulesCount =  ModulesList.size();
	        log(ModulesCount+ " modules present in a Highlights page",LogType.STEP);
	        log("Checking each modules in a Highlights page is a link to an article page",LogType.STEP);
	        int ArticleCounter = 0;
	        for(int i=0; i<ModulesCount;i++)
	        {
	        	List<WebElement> Modules = getCommand().driver.findElements(By.xpath("//*[@class = 'feature-box-title']/a"));
	        	String Href = Modules.get(i).getAttribute("href");
	        	String Pageurl= getCommand().getPageUrl();
	        	getCommand().waitFor(2);
	        	String selectModuleOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN); 
	        	Modules.get(i).sendKeys(selectModuleOpeninNewTab);
	            getCommand().waitFor(5);
	            ArrayList<String> listofTabs = new ArrayList<String> (getCommand().driver.getWindowHandles());
	            if(listofTabs.size()>1)
	            {
	            	log("Switching to new tab",LogType.STEP);
		            getCommand().driver.switchTo().window(listofTabs.get(1));
		            
		            String Currentpageurl = getCommand().getPageUrl();
		            if(Pageurl.equals(Currentpageurl))
			    	{	    			
			    		log("Clicking on link "+Href+" is not redirecting to the corresponding page",LogType.ERROR_MESSAGE);
			    		Assert.fail("Clicking on link "+Href+" is not redirecting to the corresponding page");
			    	}
			    	else
			    	{    			
			    		log("Clicking on link "+Href+" is redirecting to the corresponding page",LogType.STEP);
			    		ArticleCounter++;
			    	}
		            
		        	getCommand().driver.close();

	                getCommand().driver.switchTo().window(listofTabs.get(0));
	            }
	            
	            else
	            {
	            	String Currentpageurl = getCommand().getPageUrl();
		            if(Pageurl.equals(Currentpageurl))
			    	{	    			
			    		log("Clicking on link "+Href+" is not redirecting to the corresponding page",LogType.ERROR_MESSAGE);
			    		Assert.fail("Clicking on link "+Href+" is not redirecting to the corresponding page");
			    	}
			    	else
			    	{    			
			    		log("Clicking on link "+Href+" is redirecting to the corresponding page",LogType.STEP);
			    		ArticleCounter++;
			    	}
		            
		            getCommand().goBack();
	            }
	            
	        }
	        
	        if(ArticleCounter == ModulesCount)
	        {
	        	log("Each module is a link to an article page",LogType.STEP);     	
	        }
	        
	        else
	        {
	        	log("Each module is not a link to an article page",LogType.ERROR_MESSAGE);    
	        	Assert.fail("Not article links");
	        }
		}
		
		catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
	}
       
    //Verify that clicking on Hero image redirects to correspondent page
    public Hippo_Africa VerifyHeroImageNavigation()
	{
    	try
		{	List<WebElement> NavDots = getCommand().driver.findElements(By.xpath("//*[@id='myCarousel']/ol/li"));
			
			int i=1;
			
			log("Clicking on each nav dots and clicking on image displayed for each nav dot",LogType.STEP);
			
			for(int k=0;k<NavDots.size();k++)
			{
				List<WebElement> Navdots = getCommand().driver.findElements(By.xpath("//*[@id='myCarousel']/ol/li"));
				
				String PageUrl = getCommand().getPageUrl();
				
				log("Clicking on "+i+" Nav dot",LogType.STEP);
				
				Navdots.get(k).click();
				
				getCommand().waitFor(2);
				
                WebElement Image= getCommand().driver.findElement(By.xpath("//*[@id='myCarousel']/div/div/div/div["+i+"]"));
				
				if(Image.isDisplayed())
				{
					String ImageText = Image.getText();	
					
					log("Clicking on image "+ ImageText,LogType.STEP);
					
					Image.click();
					
					getCommand().waitFor(4);
					
					String CurrentPageUrl = getCommand().getPageUrl();
					
					Assert.assertNotEquals(PageUrl, CurrentPageUrl,"Clicking on image "+ ImageText+ " is not redirecting to its site");
					
					log("Clicking on image "+ ImageText+ " is redirecting to its site: "+getCommand().getPageTitle(),LogType.STEP);
					
					getCommand().goBack();
					
					getCommand().waitFor(3);
								
					i++;				
				}
				
				else
				{
					log("Failed to click on "+i+" Image",LogType.ERROR_MESSAGE);
					Assert.fail("Failed to click on "+i+" Image");
				}
			}  
			}	
		
		catch(Exception ex)
		{
			ex.getMessage();
			Assert.fail(ex.getMessage());
		}
	
		return this;
	}
    
    //Verify Hero carousel using NavDots,Arrows
    public Hippo_Africa HippoAfrica_HeroImageCarousel()
    {
    	try
		{
    		List<String> Image_Heading=new ArrayList<>();
			int count=getCommand().getTargetCount(Hero_Img);
			
			log("Checking Scroll feature using next arrow",LogType.STEP);
			
			for(int k=2;k<=count;k++)
			{
				log("Clicking on next arrow and checking image changes",LogType.STEP);
				
				getCommand().click(Hero_Next_Arrow);
				
				getCommand().waitFor(2);
				
				WebElement Image= getCommand().driver.findElement(By.xpath("//*[@id='myCarousel']/div/div/div/div["+k+"]/a/img"));
				if(Image.isDisplayed()) {
					String imageText=getCommand().driver.findElement(By.xpath("//*[@id='myCarousel']/div/div/div/div["+k+"]/div/h1")).getText();
					if(imageText.equals("")) 
					{
						String NavDotXpath_Emptytext = "//*[@id='myCarousel']/div/div/div/div["+k+"]/div/h1[2]";
						String ImageText_Empty = getCommand().getDriver().findElement(By.xpath(NavDotXpath_Emptytext)).getText();
						Image_Heading.add(ImageText_Empty);
					}			
				
				}

				else
				{
					log("Image is not displayed after clicking on next arrow",LogType.ERROR_MESSAGE);
					Assert.fail("Image is not displayed after clicking on next arrow");
				}
				log("Checking Scroll feature using previous arrow",LogType.STEP);

				for(int m=2;m<=count;m++)
				{
					log("Clicking on previous arrow and checking image changes",LogType.STEP);
					
					getCommand().click(Hero_Next_Arrow);
					
					getCommand().waitFor(2);
					
					WebElement Image_Prev= getCommand().driver.findElement(By.xpath("//*[@id='myCarousel']/div/div/div/div["+m+"]/a/img"));
					if(Image_Prev.isDisplayed()) {
						String imageText=getCommand().driver.findElement(By.xpath("//*[@id='myCarousel']/div/div/div/div["+m+"]/div/h1")).getText();
						if(imageText.equals("")) 
						{
							String NavDotXpath_Emptytext = "//*[@id='myCarousel']/div/div/div/div["+k+"]/div/h1[2]";
							String ImageText_Empty = getCommand().getDriver().findElement(By.xpath(NavDotXpath_Emptytext)).getText();
							Image_Heading.add(ImageText_Empty);
						}			
					
					}

					else
					{
						log("Image is not displayed after clicking on previous arrow",LogType.ERROR_MESSAGE);
						Assert.fail("Image is not displayed after clicking on next arrow");
					}
				
			}
			
            //log("Comparing title's in list with each other and checking they are unique",LogType.STEP);
			
			//Assert.assertTrue(CompareDataFromSameList(Imagetext_NextArrow),"Clicking on next arrow, image not changes and scroll is not working");
			
			log("Clicking on next arrow, image changes and scroll is working",LogType.STEP);
			
			getCommand().mouseHover(HeroNextArrow).click(HeroNextArrow);
			
		
			
				}
		}

		

		
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
    }
   
    //Method to verify data from same list to check Hero images navigation
	public boolean CompareDataFromSameList(List<String> list)
	{
		boolean status=true;
		label:
		for (int i = 0; i < list.size()-1; i++) 
		{
			for (int k = i+1; k < list.size(); k++) 
			{			
				if(list.get(i).equals(list.get(k)))
				{
					status=false;
					break label;
				}				      
			}	      
		}		
		return status;
	}
}  

  