package com.components.pages;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.openqa.selenium.Keys;
import org.openqa.selenium.Point;

import com.components.repository.SiteRepository;
import com.components.yaml.SearchData;
import com.iwaf.framework.components.IReporter.LogType;
import com.iwaf.framework.components.Target;

public class Hippo_Brazil extends SitePage
{
	
	/* Defining the locators on the Page */ 
	
	public static final Target Hero  = new Target("Hero","//*[@id='koh-page-outer']/div/div/div/div/div/div[1]/section",Target.XPATH);
	public static final Target PromoModulesGrid  = new Target("PromoModulesGrid","//*[@id='koh-page-outer']/div/div/div/div/div/div[2]/section",Target.XPATH);
	public static final Target Footer  = new Target("Footer","//*[@id='koh-page-outer']/div/footer/div[2]",Target.XPATH);
	
	public static final Target HeroNextArrow  = new Target("HeroNextArrow","//*[@id='koh-page-outer']/div/div/div/div/div/div[1]/section/div/div[1]/button[2]/span[1]",Target.XPATH);
	public static final Target HeroPrevArrow  = new Target("HeroPrevArrow","//*[@id='koh-page-outer']/div/div/div/div/div/div[1]/section/div/div[1]/button[1]/span[1]",Target.XPATH);
		
	public static final Target Banheiro  = new Target("Banheiro","//*[@id='koh-primary-nav-menu']/ul/li[1]/span",Target.XPATH);
	public static final Target Chuveiroa  = new Target("Chuveiroa","//*[@id='koh-primary-nav-menu']/ul/li[1]/div/div/ul[3]/li[2]/a",Target.XPATH);
	public static final Target ChuveiroaHeader  = new Target("ChuveiroaHeader","//*[@id='koh-page-outer']/div/div/section[2]/header/h2",Target.XPATH);
	
	
	public static final Target CompareOverlay  = new Target("CompareOverlay","//*[@id='comparePanel']/div",Target.XPATH);
	public static final Target CompareOverlayHeader  = new Target("CompareOverlayHeader","//*[@id='comparePanel']/div/div[1]/span",Target.XPATH);
	public static final Target CompareOverlayArrow  = new Target("CompareOverlayArrow","//*[@id='comparePanel']/div/div[1]/button[1]",Target.XPATH);
	public static final Target CompareOverlayclearresult  = new Target("CompareOverlayclearresult","//*[@id='comparePanel']/div/div[1]/button[2]",Target.XPATH);
	public static final Target Compare_btn  = new Target("Compare_btn","//*[@id='comparePanel']/div/div[1]/a",Target.XPATH);
	public static final Target CompareModal_Header = new Target("CompareModal_Header","//*[@class='koh-compare-header']/h1",Target.XPATH);
	public static final Target CompareModal_Print= new Target("CompareModal_Print","//*[@class='koh-compare-top']/button[@class='koh-compare-print']",Target.XPATH);
	public static final Target CompareModal_FeatureColumn= new Target("CompareModal_FeatureColumn","//*[@class='koh-compare-features']/ul",Target.XPATH);
	public static final Target CompareModal_Close= new Target("CompareModal_Close","//*[@class='koh-compare-panel-modal modal c-koh-compare-panel-modal remodal remodal-is-initialized remodal-is-opened']/button[@class='remodal-close']",Target.XPATH);

	public static final Target CozinhaNav  = new Target("CozinhaNav","//*[@id='koh-primary-nav-menu']/ul/li[2]",Target.XPATH);
	public static final Target CozinhaNav_SubOption1  = new Target("CozinhaNav_SubOption1","//*[@id='koh-primary-nav-menu']/ul/li[2]/div/div/ul/li[2]/a",Target.XPATH);
	public static final Target CozinhaNav_HeroImage  = new Target("CozinhaNav_HeroImage","//*[@id='koh-page-outer']/div/div/section[1]/div/div[2]/div/div/div/div/img",Target.XPATH);
	public static final Target CozinhaNav_Sort  = new Target("CozinhaNav_Sort","//*[@id='koh-page-outer']/div/div/section[2]/div[1]/button",Target.XPATH);
	public static final Target CozinhaNav_FilterCategory = new Target("CozinhaNav_FilterCategory","//*[@id='koh-page-outer']/div/div/section[2]/div[2]/div[1]",Target.XPATH);
	public static final Target CozinhaNav_preselectedfilter = new Target("CozinhaNav_preselectedfilter","//*[@id='koh-page-outer']/div/div/section[2]/div[2]/div[2]/div[1]/ul/li/button/span[2]",Target.XPATH);
	public static final Target CozinhaNav_ProductGrid = new Target("CozinhaNav_ProductGrid","//*[@id='koh-page-outer']/div/div/section[2]/div[3]",Target.XPATH);

	public static final Target Search  = new Target("Search","//*[@id='koh-page-outer']/div/header/div/div[1]/div[2]/div[2]/form/span[2]/input[1]",Target.XPATH);
	//public static final Target SearchButton  = new Target("SearchButton","//*[@id='koh-page-outer']/div/header/div/div[1]/div[2]/div[2]/form/span[2]/span/button",Target.XPATH);
	public static final Target ProductId_PDP  = new Target("ProductId_PDP","//*[@id='koh-page-outer']/div/div/section/div[1]/div[3]/div[2]/span",Target.XPATH);
	public static final Target CareAndCleaning  = new Target("CareAndCleaning","//*[@id=\"koh-page-outer\"]/div/footer/div[2]/ul/li[3]/ul/li[2]/a",Target.XPATH);
	
	public static final Target ResultsHeader  = new Target("ResultsHeader","//*[@id='koh-page-outer']/div/div/div[1]/div/h1",Target.XPATH);
	public static final Target Results  = new Target("Results","//*[@id='koh-page-outer']/div/div/div/div/p",Target.XPATH);
		
	public static final Target FilterCorAcabamento  = new Target("FilterCorAcabamento","//*[@id=\"koh-page-outer\"]/div/div/section[2]/div[2]/div[2]/div[2]/div[1]/span",Target.XPATH);
	public static final Target FilterTipoDe  = new Target("FilterLitrosporMinuto","//*[@id='koh-page-outer']/div/div/section[2]/div[2]/div[2]/div[2]/div[2]/span",Target.XPATH);
	public static final Target FilterCount = new Target("FilterCount","//*[@id='koh-page-outer']/div/div/section[2]/div[2]/div[2]/div[2]/div[2]/div/ul/li/button/span[3]",Target.XPATH);
	public static final Target Backgroundcolor_Filter = new Target("Backgroundcolor_Filter","//*[@id='koh-page-outer']/div/div/section[2]/div[2]/div[2]/div[2]/div[2]/div/ul/li/button",Target.XPATH);
	
	public static final Target Colorswatch_Hover = new Target("Colorswatch_Hover","//*[@id='koh-page-outer']/div/div/section[2]/div[2]/div[2]/div[2]/div[1]/div/ul/li/a/img",Target.XPATH);
//---------------------------------------------Mobile new xpaths---------------------------------------------------------------------------------
	
	public static final Target Hamburger_Icon = new Target("Hamburger_Icon","//*[@id='koh-page-outer']/div/header/div/div[1]/div[2]/div[1]/nav/div[1]/button[1]",Target.XPATH);
	public static final Target KohlerWorldWide  = new Target("KohlerWorldWide","//*[@id='koh-primary-nav-menu']/div[2]/div[1]/ul[2]/li/a",Target.XPATH);
	public static final Target Search_Icon  = new Target("Search_Icon","//*[@id='koh-page-outer']/div/header/div/div[1]/div[2]/div[1]/nav/div[1]/button[2]",Target.XPATH);
	public static final Target BathroomBtn=new Target("BathroomBtn","//*[@id='koh-primary-nav-menu']/ul/li[1]/span",Target.XPATH);
	public static final Target KitchenBtn=new Target("BathroomBtn","//*[@id=\"koh-primary-nav-menu\"]/ul/li[2]/span",Target.XPATH);
	public static final Target IDEAS=new Target("BathroomBtn","//*[@id=\"koh-primary-nav-menu\"]/ul/li[3]/a",Target.XPATH);
	public static final Target KohlerCo_Footer=new Target("KohlerCo_Footer","//*[@id=\"koh-page-outer\"]/div/footer/div[2]/ul/li[2]/span",Target.XPATH);
	public static final Target Kitchen_SubMenu=new Target("Kitchen_SubMenu","(//*[@class='koh-nav-subnav open']//following::a[1])[1]",Target.XPATH);
    public static final Target SearchInput = new Target("SearchInput","//*[@id=\"koh-nav-searchbox\"]",Target.XPATH);
    public static final Target SearchButton = new Target("SearchButton","(//*[@id='koh-nav-searchbutton'])[1]",Target.XPATH);
	public static final Target Product_Category  = new Target("Product_Category","//*[@id=\"koh-page-outer\"]/div/div/div[1]/div/div/button",Target.XPATH);
	public static final Target SortBy  = new Target("SortBy","//*[@id=\"koh-page-outer\"]/div/div/section/div[1]/button",Target.XPATH);
	public static final Target PromoGrid  = new Target("PromoGrid","//*[@id=\"koh-page-outer\"]/div/div/section/div[3]/div[1]/div/div[1]/a",Target.XPATH);
	public static final Target Filter  = new Target("Filter","//*[@id=\"koh-page-outer\"]/div/div/section/div[2]/div[1]",Target.XPATH);

	
	
	
	
	Actions Action = new Actions(getCommand().driver);
	
	//For getting Browser Name
	Capabilities caps = ((RemoteWebDriver) getCommand().driver).getCapabilities();
	
	JavascriptExecutor js = (JavascriptExecutor) getCommand().getDriver(); 
	
	
	public Hippo_Brazil(SiteRepository repository)
	{
		super(repository);
	}

	/* Functions on the Page are defined below */
	
	public Hippo_Brazil atHippo_Brazil()
	{
		log("Launched Hippo Brazil Site",LogType.STEP);
		//getCommand().captureScreenshot("C:\\Users\\Arvind01\\Desktop\\Add To Cart\\HomePage.png");
		return this;		
	}

	//Method to verify Hippo Brazil home page layout
	public Hippo_Brazil HippoBrazil_HomePageLayout()
	{
		try
		{
			
			String HeroText = getCommand().getAttribute(Hero, "class");
			
			if(getCommand().isTargetVisible(Hero) && HeroText.contains("hero"))
			{
				log("Hero is displayed",LogType.STEP);
			}
			
			else
			{
				log("Hero is not displayed",LogType.ERROR_MESSAGE);
				Assert.fail("Hero is not displayed");
			}
	        
	        String PromoModulesText = getCommand().getAttribute(PromoModulesGrid,"class");
			
			if(getCommand().isTargetVisible(PromoModulesGrid) && PromoModulesText.contains("promo-grid"))
			{
				log("PromoModules is displayed",LogType.STEP);
			}
			
			else
			{
				log("PromoModules is not displayed",LogType.ERROR_MESSAGE);
				Assert.fail("PromoModules is not displayed");
			}
			
			String FooterText = getCommand().getAttribute(Footer,"class");
			
			if(getCommand().isTargetVisible(Footer) && FooterText.contains("footer"))
			{
				log("Footer is displayed",LogType.STEP);
			}
			
			else
			{
				log("Footer is not displayed",LogType.ERROR_MESSAGE);
				Assert.fail("Footer is not displayed");
			}
			 if(getCommand().isTargetVisible(Search_Icon))
				{
					log("Search is displayed",LogType.STEP);
				}
				
				else
				{
					log("Search is not displayed",LogType.ERROR_MESSAGE);
					Assert.fail("Search is not displayed");
				}
			 
			 getCommand().waitForTargetVisible(Hamburger_Icon).click(Hamburger_Icon);
			String WorldWideText = getCommand().getText(KohlerWorldWide);
			
			if(getCommand().isTargetVisible(KohlerWorldWide) && WorldWideText.equals("KOHLER Worldwide"))
			{
				log(WorldWideText+" is displayed",LogType.STEP);
			}
			
			else
			{
				log(WorldWideText+" is not displayed",LogType.ERROR_MESSAGE);
				Assert.fail(WorldWideText+" is not displayed");
			}
			
			

			List<String> GlobalNav = new ArrayList<String>();	
			
			String browserName = caps.getBrowserName();
			
			GlobalNav.add("//*[@id='koh-primary-nav-menu']/ul/li[1]/span");
			GlobalNav.add("//*[@id='koh-primary-nav-menu']/ul/li[2]/span");
			GlobalNav.add("//*[@id='koh-primary-nav-menu']/ul/li[3]/a");
			GlobalNav.add("//*[@id='koh-page-outer']/div/header/div/div[1]/div[2]/div[1]/nav/div[1]/a/img");

			for(String Xpath:GlobalNav)
			{			
				WebElement Element = getCommand().driver.findElement(By.xpath(Xpath));
				String Text = Element.getText();
				if(Element.isDisplayed())
				{
					if(!Text.isEmpty())
					{
						if(browserName.equals("MicrosoftEdge"))
						{
							if(Text.equals("Banheiro") || Text.equals("Cozinha") || Text.equals("IDEIAS")) {
								log(Text+" is displayed in Global Navigation",LogType.STEP);
								log("Correct Text: "+Text+" is displayed in Global Navigation",LogType.STEP);
							}

							else
							{
								log("Correct Text: "+Text+" is not displayed in Global Navigation",LogType.ERROR_MESSAGE);
								Assert.fail("Correct Text: "+Text+" is not displayed in Global Navigation");
							}
						}
						
						else
						{
							if(Text.equals("BANHEIRO") || Text.equals("COZINHA") || Text.equals("IDEIAS")) {
								log(Text+" is displayed in Global Navigation",LogType.STEP);
								log("Correct Text: "+Text+" is displayed in Global Navigation",LogType.STEP);
							}

							else
							{
								log("Correct Text: "+Text+" is not displayed in Global Navigation",LogType.ERROR_MESSAGE);
								Assert.fail("Correct Text: "+Text+" is not displayed in Global Navigation");
							}
						}
					}
					
					else
					{
						log("Logo is displayed in Global Navigation",LogType.STEP);
					}
				}
				
				else
				{
					if(!Text.isEmpty())
						log(Text+" is not displayed in Global Navigation",LogType.ERROR_MESSAGE);
					    
					else
						log("Logo is not displayed in Global Navigation",LogType.ERROR_MESSAGE);
					
					Assert.fail("Logo is not displayed in Global Navigation");
				}
			}
		}
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		
		return this;
	}	
	
	
	
	
	//Method to verify Global Navigation folding/Unfolding in Home page
	public Hippo_Brazil HippoBrazil_GlobalNavunfolding() throws InterruptedException
	{
		try {
			 getCommand().waitForTargetVisible(Hamburger_Icon).click(Hamburger_Icon);
			 getCommand().waitFor(2);
			 log("clicking on BathRoom link from Hamburger menu", LogType.STEP);
			 getCommand().click(BathroomBtn);
			 WebElement BathroomMenu=getCommand().driver.findElement(By.xpath("//*[@id=\"koh-primary-nav-menu\"]/ul/li[1]/div/div"));
			 if(BathroomMenu.isDisplayed()) {
				 log("clicking on BathRoom link and unfolded", LogType.STEP);
				 getCommand().click(BathroomBtn);
			 }
			 else {
				 log("clicking on BathRoom link not unfolded", LogType.STEP);
			 }
			 getCommand().waitFor(3);
			 
			 log("clicking on Kitchen link from Hamburger menu", LogType.STEP);
			 getCommand().click(KitchenBtn);
			 
			 WebElement KitchenMenu=getCommand().driver.findElement(By.xpath("//*[@id=\"koh-primary-nav-menu\"]/ul/li[2]/div/div"));
			 if(KitchenMenu.isDisplayed()) {
				 log("clicking on Kitchen link and unfolded", LogType.STEP);
				 getCommand().click(KitchenBtn);
			 }
			 else {
				 log("clicking on Kitchen link not unfolded", LogType.STEP);
			 }
			 getCommand().waitFor(3);	 
			log("Both the links are unfolded in the same manner",LogType.STEP);
		
			getCommand().click(IDEAS);
			getCommand().waitFor(3);
			String mainWindow = getCommand().getDriver().getWindowHandle();
		
			Set<String> s1 = getCommand().getDriver().getWindowHandles();
		
			s1.remove(mainWindow);
		
			Iterator<String> i1 = s1.iterator();
	
		while(i1.hasNext()) {
			
			String childWindow = i1.next();
			
			getCommand().getDriver().switchTo().window(childWindow);
		
				String currentUrl = getCommand().getPageUrl();
				
				log("currentUrl: "+currentUrl,LogType.STEP);
				
				String actual = "ideas.kohler.com";
				
				if(currentUrl.contains(actual)) {
					
					log(" Ideas opens in a new tab and url is verified", LogType.STEP);
					
				}else {
					
					log(" Ideas not opened in a new tab and url is not verified", LogType.STEP);
					
					Assert.fail("Ideas is not opened in a new tab");
			}
			
		}
	
	 }
		
	 catch(Exception ex) {
			
			log("SearchResultsText is not displayed",LogType.STEP);
			
	        Assert.fail(ex.getMessage());
	        
		}
	
	return this;	
	}

	//Method to verify Footer section in Home Page
	public Hippo_Brazil HippoBrazil_Footer()
	{	
		log("Verifying footer is composed of 3 columns Links",LogType.STEP);
		getCommand().scrollTo(KohlerCo_Footer);

        List<WebElement> footer = getCommand().driver.findElements(By.xpath("//*[@id='koh-page-outer']/div/footer/div[2]/ul/li/span"));

        int  footerlinks_count = footer.size();

        if(footerlinks_count == 3)
        {
        	log("Footer is composed of 3 columns Links",LogType.STEP);
        	
        	List<WebElement> footerHeaders = getCommand().driver.findElements(By.xpath("//*[@id='koh-page-outer']/div/footer/div[2]/ul/li/span/span"));
        	
        

            for(WebElement footerHeader:footerHeaders)
            {
            	String HeaderText = footerHeader.getText();
            	
                		log("Expected Column text: "+HeaderText+", is displayed under Footer section",LogType.STEP);
                	
            	
            	
            }
        }
		
        try

        {
        	
        	log("Checking the navigation of each link under Footer section",LogType.STEP);
        	for (int j = 1; j <= footerlinks_count; j++)
        	{
        		getCommand().driver.findElement(By.xpath("//*[@id='koh-page-outer']/div/footer/div[2]/ul/li["+j+"]/span")).click();
        		String pageTitle = getCommand().driver.getTitle();
        		List<WebElement> footerheaderlinks = getCommand().driver.findElements(By.xpath("//*[@id='koh-page-outer']/div/footer/div[2]/ul/li["+j+"]/ul/li/a"));
        		int size=footerheaderlinks.size();
        		for(int k=1; k<size;k++)
                {
        			Target subLinks=new Target("subLinks","//*[@id='koh-page-outer']/div/footer/div[2]/ul/li["+j+"]/ul/li["+k+"]/a",Target.XPATH);
               	 	String Linktext=getCommand().getText(subLinks);
                    log("Clicking on the link "+Linktext,LogType.STEP);
                    
                   
                    String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN);

                    getCommand().sendKeys(subLinks,selectLinkOpeninNewTab);
                    
                    getCommand().waitFor(8);

                    ArrayList<String> listofTabs = new ArrayList<String> (getCommand().driver.getWindowHandles());

                    log("Switching to new tab",LogType.STEP);

                    getCommand().driver.switchTo().window(listofTabs.get(1));
                    
                    log("Getting new tab page title",LogType.STEP);
                    
                    String CurrentpageTitle = getCommand().driver.getTitle();
                    
                    if(pageTitle.equals(CurrentpageTitle))

                    {
                        log("Clicking on link "+Linktext+" is not redirecting to the corresponding page",LogType.ERROR_MESSAGE);
                        Assert.fail("Clicking on link "+Linktext+" is not redirecting to the corresponding page");
                    }

                    else
                    {                                             
                         log("Clicking on link "+Linktext+" is redirecting to the corresponding page",LogType.STEP);
                    }

                    getCommand().driver.close();

                    getCommand().driver.switchTo().window(listofTabs.get(0));
                }
        	}
        }catch(Exception ex)

        {    	
            ex.getMessage();
        }
        
        return this;
	}
	
	//Method to verify Footer - CareandCleaning page
	public Hippo_Brazil HippoBrazil_FooterCareandCleaningpage() throws InterruptedException
	{
		try
		{
			getCommand().scrollTo(KohlerCo_Footer);
			getCommand().driver.findElement(By.xpath("//*[@id=\"koh-page-outer\"]/div/footer/div[2]/ul/li[3]/span")).click();
			getCommand().waitForTargetPresent(CareAndCleaning, 3);
			log("Naviagting to footer and click on 'cuidados e limpeza'",LogType.STEP);
			String pageTitle = getCommand().driver.getTitle();
			String Linktext = getCommand().getText(CareAndCleaning);
	        getCommand().click(CareAndCleaning);
	        
	        getCommand().waitFor(4);
	        
	        log("Getting currrent page title and Url",LogType.STEP);
			
	        String CurrentpageTitle = getCommand().driver.getTitle();
	        
	        String Currentpageurl = getCommand().driver.getCurrentUrl();
	        
	        log("Checking care & cleaning page displays",LogType.STEP);
	        
	        if(pageTitle.equals(CurrentpageTitle) && !Currentpageurl.contains("care-and-cleaning"))
	        {
	            log("Clicking on link "+Linktext+" is not redirecting to the corresponding page",LogType.ERROR_MESSAGE);
	            Assert.fail("Clicking on link "+Linktext+" is not redirecting to the corresponding page");
	        }

	        else
	        {
	            log("Clicking on link "+Linktext+" is redirecting to care & cleaning page",LogType.STEP);
	            
	        }
	        
	        log("Checking article page contains any information",LogType.STEP);
			
			List<WebElement> Paragraph = getCommand().driver.findElements(By.xpath("//*[@id='koh-page-outer']/div/div/div/div/div/div/div/section/div/div/p"));
			
			if(Paragraph.size()!=0)
			{
				log(Paragraph.size()+" lines of information present in care & cleaning article page",LogType.STEP);
			}
			
			else
			{
				log("No information present in care & cleaning article page",LogType.ERROR_MESSAGE);
				Assert.fail("No information present in care & cleaning article page");
			}
		}
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}

	//Method to verify product display page
	public Hippo_Brazil HippoBrazil_PDPDisplayPage(String Data)
	{
		try
		{
			log("Sending product details in to search box control",LogType.STEP);
			SearchData search = SearchData.fetch(Data);
			
			String Product = search.ProductSku;
			
			getCommand().isTargetVisible(Search_Icon);
	    	getCommand().click(Search_Icon);
	    	getCommand().sendKeys(SearchInput, Product);
	    	
			log("Clicking on search button",LogType.STEP);
			
			getCommand().click(SearchButton); 
	    	getCommand().waitFor(5);
			
			log("Getting current page url and checking product details",LogType.STEP);
			
			String Url = getCommand().driver.getCurrentUrl();
			
			String ProductID = getCommand().getText(ProductId_PDP);
			
			ProductID = ProductID.substring(2);
			
			if(Url.contains("product-detail") && ProductID.equals(Product)) {
				log("Correct product page is displayed",LogType.STEP);
			}
			else
			{
				log("Wrong product got displayed. Expected: "+Product+ ", Actual: "+ProductID,LogType.ERROR_MESSAGE);
				Assert.fail("Wrong product got displayed. Expected: "+Product+ ", Actual: "+ProductID);
			}
		}
		
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}

		return this;
	}
	
	//Method to verify product display page when searched with wrong details
	public Hippo_Brazil HippoBrazil_PDP_NoResults(String Data)
	{
		try
		{
			log("Sending wrong product details in to search box control",LogType.STEP);
			SearchData search = SearchData.fetch(Data);
			
			String Product = search.ProductSku;
			
			getCommand().isTargetVisible(Search_Icon);
	    	getCommand().click(Search_Icon);
	    	getCommand().sendKeys(SearchInput, Product);
	    	
			log("Clicking on search button",LogType.STEP);
			
			getCommand().click(SearchButton); 
	    	getCommand().waitFor(5);
			
			getCommand().waitForTargetPresent(Results, 5);
			
			log("Getting count for the search results",LogType.STEP);
			
			String ProductsCount = getCommand().getText(Results);
			
			log("Checking Zero results fetched for the wrong search",LogType.STEP);
			
			if(ProductsCount.contains("0")) {
				log("No results page displays for wrong search",LogType.STEP);
			}
			else
			{
				log("No results page not displays for wrong search",LogType.ERROR_MESSAGE);
				Assert.fail("No results page not displays for wrong search");
			}
		}
		
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}

		return this;
	}
	
	//Method to verify SKU updated by changing different color swatches
	public Hippo_Brazil HippoBrazil_SkuUpdate(String Data) throws InterruptedException
	{
		try
		{
			log("Sending product details in to search box control",LogType.STEP);
			
			SearchData search = SearchData.fetch(Data);
			
			String Product = search.ProductSku;
			
			getCommand().isTargetVisible(Search_Icon);
	    	getCommand().click(Search_Icon);
	    	getCommand().sendKeys(SearchInput, Product);
	    	
			log("Clicking on search button",LogType.STEP);
			
			getCommand().click(SearchButton); 
	    	getCommand().waitFor(5);
			
	        
	        List<String> ProductSkus = new ArrayList<String>();
	        
	        log("Getting all the color chips available for the searched product and Clicking on each color chip",LogType.STEP);
			
			List<WebElement> Colors = getCommand().driver.findElements(By.xpath("//*[@id='koh-page-outer']/div/div/section/div[1]/div[3]/div[2]/div/ul/li/span"));
			
			int i=1;
			
			for(WebElement Color : Colors) 
			{
				log("Clicking on "+i+" color chip and getting the sku# for the selected color chip",LogType.STEP);
				Color.click();
				getCommand().waitFor(3);
				String Sku = Color.getAttribute("class");
				if(Sku.contains("koh-selected-variant")) 
				{
					log("Getting the SKU# for selected color chip and adding to list",LogType.STEP);
					String ProductSku = getCommand().getText(ProductId_PDP);
					
					ProductSku = ProductSku.substring(2);
					
					ProductSkus.add(ProductSku);
					i++;
				}
				
				else 
				{
					log("Clicking on "+i+" color chip is not get selected",LogType.ERROR_MESSAGE);
					Assert.fail("Clicking on "+i+" color chip is not get selected");
				}
			}
			
			log("Checking Sku# is different for each selected color chip",LogType.STEP);
			
			getCommand().waitFor(3);
			
			Assert.assertTrue(CompareDataFromSameList(ProductSkus),"Sku# is not getting updated for different color chip");
			
			log("Sku# is different for each selected color chip. Clicking on color chip SKU# updates",LogType.STEP);
		}
		
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}

		return this;
	}

	//Method to verify World wide section in home page
	public Hippo_Brazil VerifyHippoBrazil_WorldWide() throws InterruptedException
	{
		try
	     {
	    	getCommand().waitForTargetVisible(Hamburger_Icon).click(Hamburger_Icon);
			getCommand().waitForTargetVisible(KohlerWorldWide).click(KohlerWorldWide);
			 
			WebElement element=getCommand().driver.findElement(By.xpath("//*[@id='koh-primary-nav-menu']/div[2]/div[2]/div[1]/ul/li[1]/span"));
	   	      JavascriptExecutor jse=(JavascriptExecutor)getCommand().driver;
	   	       jse.executeScript("arguments[0].scrollIntoView(true);", element);
	   	       Thread.sleep(2000);
			
	   	       List<WebElement> ColumnsHeader=getCommand().driver.findElements(By.xpath("//*[@id='koh-primary-nav-menu']/div[2]/div[2]/div[1]/ul/li/span"));
	        int NoOfColumnsCount=ColumnsHeader.size();
	     
	      if(NoOfColumnsCount==6)       
	         {
	               log("Kohler World Wide banner is composed of 6 columns",LogType.STEP);          
	         }
	      else
	        {
	          log("Kohler World Wide banner is not composed of 6 columns",LogType.ERROR_MESSAGE);
	          Assert.fail("Kohler World Wide banner is not composed of 6 columns");         
	        }
	    
	      for(int i=0;i<NoOfColumnsCount;i++)

	      {

	        String ColumnsHeaderText = ColumnsHeader.get(i).getText();
	        if(ColumnsHeaderText.equalsIgnoreCase("North America")|| ColumnsHeaderText.equalsIgnoreCase("South America")|| ColumnsHeaderText.equalsIgnoreCase("Africa")|| ColumnsHeaderText.equalsIgnoreCase("Europe")||
	        		ColumnsHeaderText.equalsIgnoreCase("Asia")||ColumnsHeaderText.equalsIgnoreCase("Oceania"))
	        {
	              log("Expected Column text: "+ColumnsHeaderText+", is displayed in World Wide banner",LogType.STEP);
	        }

	       else
	        {
	              log("Expected Column text is not displayed for column " +ColumnsHeaderText+", is displayed in World Wide banner",LogType.ERROR_MESSAGE);
	              Assert.fail("Expected Column text is not displayed for column " +ColumnsHeaderText+", is displayed in World Wide banner");
	        }
	      }   
	     
	      
	      log("Checking the navigation of each link",LogType.STEP);
	      //List<WebElement> BannerHeaders=getCommand().driver.findElements(By.xpath("//*[@id='koh-page-outer']/div/header/div/div[1]/div[1]/div[2]/div[1]/ul/li/span"));

	      for (int j = 1; j <= NoOfColumnsCount; j++)

	      {
	          String Url = getCommand().driver.getCurrentUrl();
	          getCommand().driver.findElement(By.xpath("(//*[@id='koh-primary-nav-menu']/div[2]/div[2]/div[1]/ul/li/span)["+j+"]")).click();
	          List<WebElement> bannerheaderlinks = getCommand().driver.findElements(By.xpath("//*[@id='koh-primary-nav-menu']/div[2]/div[2]/div[1]/ul/li["+j+"]/ul/li/a"));
	          int country_Count=bannerheaderlinks.size();
	          log("Total number of countries available in "+j+" column is "+country_Count ,LogType.STEP);
	          for(int k=1;k<=country_Count;k++)
	          {
	              Target countryLinks=new Target("countryLinks","(//*[@id='koh-primary-nav-menu']/div[2]/div[2]/div[1]/ul/li["+j+"]/ul/li/a)["+k+"]",Target.XPATH);

	             String Linktext =getCommand().getText(countryLinks);
	             log("Clicking on the link "+Linktext,LogType.STEP);
	            
	            String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN);
	            getCommand().sendKeys(countryLinks,selectLinkOpeninNewTab);
	             getCommand().waitFor(5);

	            ArrayList<String> listofTabs = new ArrayList<String> (getCommand().driver.getWindowHandles());

	            log("Switching to new tab",LogType.STEP);
	            getCommand().driver.switchTo().window(listofTabs.get(1));
	            
	            String CurrentpageUrl = getCommand().driver.getCurrentUrl();
	           if(Url.equals(CurrentpageUrl))
	           
	            {
	                  log("Clicking on link "+Linktext+" is not redirecting to the corresponding page",LogType.ERROR_MESSAGE);
	                  Assert.fail("Clicking on link "+Linktext+" is not redirecting to the corresponding page");
	            }

	           else
	              {                                             
	                   log("Clicking on link "+Linktext+" is redirecting to the corresponding page",LogType.STEP);
	              }

	            getCommand().driver.close();
	            getCommand().driver.switchTo().window(listofTabs.get(0));

	          }
	         getCommand().driver.findElement(By.xpath("(//*[@id='koh-primary-nav-menu']/div[2]/div[2]/div[1]/ul/li/span)["+j+"]")).click();
	         }
	      
	      log("Verifying navigation of each other Kohler brands",LogType.STEP);
	  
	      List<WebElement> Brands = getCommand().driver.findElements(By.xpath("//*[@id=\"koh-primary-nav-menu\"]/div[2]/div[2]/div[2]/ul/li/a"));	
			
	      int NoofOtherBrands=Brands.size();
	      
	      if(NoofOtherBrands==6)       
	       {
	             log("6 other kohler brands are present",LogType.STEP);          
	       }
	     else
	      {
	        log("Other Kohler brands are not present",LogType.ERROR_MESSAGE);
	        Assert.fail("Other Kohler brands are not present");         
	      }
	      for(WebElement Brand:Brands)
			{
	    	    String Url = getCommand().driver.getCurrentUrl();
	    	  
				String href = Brand.getAttribute("href");
				log("Clicking on the brand with link: "+href,LogType.STEP);
				String SelectLinkOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN);
				Brand.sendKeys(SelectLinkOpeninNewTab);
				Thread.sleep(9000);
				ArrayList<String> ListOfTabs = new ArrayList<String>(getCommand().driver.getWindowHandles());
				getCommand().driver.switchTo().window(ListOfTabs.get(1));
				String CurrentPageTitle = getCommand().driver.getTitle();
				log("Navigated to brand '"+CurrentPageTitle+"'",LogType.STEP);
	  
				String CurrentpageUrl = getCommand().driver.getCurrentUrl();
		           if(Url.equals(CurrentpageUrl))
		           
		            {
		                  log("Clicking on link "+CurrentPageTitle+" is not redirecting to the corresponding page",LogType.ERROR_MESSAGE);
		                  Assert.fail("Clicking on link "+CurrentPageTitle+" is not redirecting to the corresponding page");
		            }

		           else
		              {                                             
		                   log("Clicking on link "+CurrentPageTitle+" is redirecting to the corresponding page",LogType.STEP);
		              }
				
			
					getCommand().driver.close();
		    		getCommand().driver.switchTo().window(ListOfTabs.get(0));				
				}
			}
	       
	      catch(Exception ex)
	      {
	          Assert.fail(ex.getMessage());
	      }
	     
			 return this;
	}
	
	//Method to verify Compare feature
	public Hippo_Brazil VerifyCompareFeature() throws InterruptedException
	{
		try
		{
			log("Verifying Compare Feature",LogType.STEP);
			
			log("Clicking on Banheiro",LogType.STEP);
			getCommand().isTargetPresent(Banheiro);
			
			getCommand().click(Banheiro);
			
			log("Clicking on Chuveiroa",LogType.STEP);
			
	        getCommand().isTargetPresent(Chuveiroa);
			
			getCommand().click(Chuveiroa);
			
			getCommand().waitFor(3);
			
			int productscount = 1;
			
			List<WebElement> Products = getCommand().driver.findElements(By.xpath("//*[@id='koh-page-outer']/div/div/section[2]/div[3]/div"));
			
			log("Selecting three products for comapre",LogType.STEP);
			
			String browserName = caps.getBrowserName();
			
			if(browserName.equals("firefox"))
			{					
				js.executeScript("arguments[0].scrollIntoView(true);", Products.get(0));
			}
		    
			for(WebElement Product : Products)
			{				
				if(productscount<=3)
				{
					Actions Action = new Actions(getCommand().driver);
					
					Action.moveToElement(Product).build().perform();
					
					getCommand().waitFor(1);
					
					String Xpath = "//*[@id='koh-page-outer']/div/div/section[2]/div[3]/div["+productscount+"]/div/div[2]/a[@class = 'koh-compare-add koh-compare-button-tile']";
					
					WebElement Compare = getCommand().driver.findElement(By.xpath(Xpath));
					
					if(Compare.isDisplayed())
					{
						Compare.click();
						getCommand().waitFor(1);
						if(browserName.equals("firefox"))
						{					
							getCommand().click(CompareOverlayArrow);
						}
					}
					productscount++;
				}			
			}
			
			getCommand().waitFor(2);
			
			log("Selected three products for compare",LogType.STEP);
			
			log("Checking Compare overlay displayed with three products",LogType.STEP);
			
			if(browserName.equals("firefox"))
			{					
				getCommand().click(CompareOverlayArrow);
			}
			
			List<WebElement> CompareProductsCount = getCommand().driver.findElements(By.xpath("//*[@id='comparePanel']/div/div[2]/div"));
			
			getCommand().waitForTargetVisible(CompareOverlayHeader);
			
			String Compareheadertext =  getCommand().getText(CompareOverlayHeader);
			
			if(browserName.equals("MicrosoftEdge"))
			{
				if(CompareProductsCount.size()==3 && Compareheadertext.equals("Comparar Produtos"))
				{
					log("Compare panel displayed with three products", LogType.STEP);
				}
				
				else
				{
					log("Compare panel is not displayed with three products", LogType.ERROR_MESSAGE);
					Assert.fail("Compare panel is not displayed with three products");
				}
			}
			else
			{
				if(CompareProductsCount.size()==3 && Compareheadertext.equals("COMPARAR PRODUTOS"))
				{
					log("Compare panel displayed with three products", LogType.STEP);
				}
				
				else
				{
					log("Compare panel is not displayed with three products", LogType.ERROR_MESSAGE);
					Assert.fail("Compare panel is not displayed with three products");
				}
			}

			getCommand().waitFor(2);
			
			log("Checking for ProductName/Sku",LogType.STEP);
			
			for(int j=1; j<=CompareProductsCount.size(); j++)
			{
				for(int k=1; k<=CompareProductsCount.size()-1;k++)
				{
					String Xpath = "//*[@id='comparePanel']/div/div[2]/div["+j+"]/a/span["+k+"]";
					
					WebElement Productdetail = getCommand().driver.findElement(By.xpath(Xpath));
					
					if(Productdetail.isDisplayed())
					{
						log("Product Name/Sku is displayed as: "+Productdetail.getText(), LogType.STEP);
					}
					
					else
					{
						log("Product Name/Sku is not displayed", LogType.ERROR_MESSAGE);
						Assert.fail("Product Name/Sku is not displayed");
					}
				}
			}
			
			log("Checking for Cross in all three products",LogType.STEP);
			
			int i = 1;
			List<WebElement> Cross = getCommand().driver.findElements(By.xpath("//*[@id='comparePanel']/div/div[2]/div/button"));
			for(WebElement cross : Cross)
			{
				if(cross.isDisplayed())
				{
					log("Cross is present for "+i+" product",LogType.STEP);
				}
				
				else
				{
					log("Cross is not present for "+i+" product",LogType.ERROR_MESSAGE);
					Assert.fail("Cross is not present for "+i+" product");
				}
				i++;
			}
			
			log("Checking arrow for compare overlay window",LogType.STEP);
			
			if(getCommand().getAttribute(CompareOverlayArrow,"class").contains("open"))
			{
				log("Clicking on arrow in compare overlay window",LogType.STEP);
				getCommand().click(CompareOverlayArrow);
				getCommand().waitFor(5);
				
				if(!getCommand().getAttribute(CompareOverlayArrow,"class").contains("open")) 
				{
					log("contrac/expand button working as expected",LogType.STEP);
				}

				else
				{
					log("contrac/expand button is not working as expected",LogType.ERROR_MESSAGE);
					Assert.fail("contrac/expand button is not working as expected");
				}
					
				
				getCommand().click(CompareOverlayArrow);
				getCommand().waitFor(5);
			}
			
			//Code for Compare window
			
			CompareModal();
			
			getCommand().waitFor(2);
			
			log("Clicking on clear results button",LogType.STEP);
			getCommand().click(CompareOverlayclearresult);
			
			getCommand().waitFor(3);
			
			if(!getCommand().isTargetVisible(CompareOverlay))
			{
				log("Clicking on clear results button removed the products and compare overlay  window is closed",LogType.STEP);	
			}
			
			else
			{
				log("Clear results is not working",LogType.ERROR_MESSAGE);
				Assert.fail("Clear results is not working");		
			}
			
		}
		catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		
		return this;

	}
	
	//Method to verify Compare window
	public Hippo_Brazil CompareModal() throws InterruptedException
	{
		try
		{
			log("Clicking om Compare button in Compare overlay display",LogType.STEP);
			
			getCommand().isTargetPresent(Compare_btn);
			
			getCommand().click(Compare_btn);
			
			getCommand().waitFor(3);
			
			log("Checking Compare modal displayed",LogType.STEP);
			
			String Text = getCommand().getText(CompareModal_Header);
			
			List<WebElement> CompareModalProducts = getCommand().driver.findElements(By.xpath(".//*[@class='koh-compare-products']/ul/li"));
			
            String browserName = caps.getBrowserName();
			
			if(browserName.equals("MicrosoftEdge"))
			{
				if(Text.equals("Comparar produtos") && CompareModalProducts.size()==3)
				{
					log("Compare modal displayed after cliking on compare button",LogType.STEP);
				}
				
				else
				{
					log("Compare modal is not displayed after cliking on compare button", LogType.ERROR_MESSAGE);
					Assert.fail("Compare modal is not displayed after cliking on compare button");
				}
			}
			
			else
			{
				if(Text.toUpperCase().equals("COMPARAR PRODUTOS") && CompareModalProducts.size()==3)
				{					
					log("Compare modal displayed after cliking on compare button",LogType.STEP);
				}
				
				else
				{
					log("Compare modal is not displayed after cliking on compare button", LogType.ERROR_MESSAGE);
					Assert.fail("Compare modal is not displayed after cliking on compare button");
				}
			}
			
			log("Checking Images and product features are displayed for all three products",LogType.STEP);
			
			for(int i=1; i<=CompareModalProducts.size();i++)
			{
				String Xpath = ".//*[@class='koh-compare-products']/ul/li["+i+"]/div/a/img";
				
				WebElement Productdetail = getCommand().driver.findElement(By.xpath(Xpath));
				
				if(Productdetail.isDisplayed())
				{
					log("Image is displayed for "+i+" product", LogType.STEP);
				}
				
				else
				{
					log("Image is not displayed for "+i+" product", LogType.ERROR_MESSAGE);
					Assert.fail("Image is not displayed for "+i+" product");
				}
				
				String FeatureXpath = ".//*[@class='koh-compare-products']/ul/li["+i+"]/ul";
				
                WebElement Productfeaturedetail = getCommand().driver.findElement(By.xpath(FeatureXpath));
				
				if(Productfeaturedetail.isDisplayed())
				{
					log("product feature's is displayed for "+i+" product", LogType.STEP);
				}
				
				else
				{
					log("product feature's are not displayed for "+i+" product", LogType.ERROR_MESSAGE);
					Assert.fail("product feature's are not displayed for "+i+" product");
				}
				
				
			}
			
			log("Checking Product Name/Sku are displayed for all three products",LogType.STEP);
			
			for(int j=1; j<=CompareModalProducts.size(); j++)
			{
				for(int k=1; k<=CompareModalProducts.size()-1;k++)
				{
					String Xpath = ".//*[@class='koh-compare-products']/ul/li["+j+"]/div/a/span["+k+"]";
					
					WebElement Productdetail = getCommand().driver.findElement(By.xpath(Xpath));
					
					if(Productdetail.isDisplayed())
					{
						log("Product Name/Sku is displayed as: "+Productdetail.getText()+" for "+j+" product", LogType.STEP);
					}
					
					else
					{
						log("Product Name/Sku is not displayed for "+j+" product", LogType.ERROR_MESSAGE);
						Assert.fail("Product Name/Sku is not displayed for "+j+" product");
					}
				}
			}
			log("Checking Print option is displayed in compare modal",LogType.STEP);
			
			if(getCommand().isTargetVisible(CompareModal_Print))
			{
				log("Print option is displayed in Comapre Modal", LogType.STEP);
			}
			
			else
			{
				log("Print option is not displayed in Comapre Modal", LogType.ERROR_MESSAGE);
				Assert.fail("Print option is not displayed in Comapre Modal");
			}
			
			log("Checking Product Feature column headers is displayed in Compare modal",LogType.STEP);
			
			if(getCommand().isTargetVisible(CompareModal_FeatureColumn))
			{
				log("Product Feature column headers is displayed in Comapre Modal", LogType.STEP);
				
				Point ProductFeature = getCommand().driver.findElement(By.xpath("//*[@class='koh-compare-features']/ul")).getLocation();
			    int xcord_ProductFeature = ProductFeature.getX();
				 
				 Point Products = getCommand().driver.findElement(By.xpath("//*[@class='koh-compare-products']/ul")).getLocation();
				 int xcord_Products = Products.getX();
				 
				 if(xcord_ProductFeature<xcord_Products)
				 {
					 log("product features column is on the left", LogType.STEP);
				 }
				 
				 else
				 {
					 log("product features column is not on the left", LogType.ERROR_MESSAGE);
					 Assert.fail("product features column is not on the left");
				 }
				 
			}
			
			else
			{
				log("Product Feature column headers is not displayed in Comapre Modal", LogType.ERROR_MESSAGE);
				Assert.fail("Product Feature column headers is not displayed in Comapre Modal");
			}
			
            getCommand().isTargetPresent(CompareModal_Close);
			
			getCommand().click(CompareModal_Close);
		}
		
		catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		
		return this;
	}
	
	//Method to verify Search results page
	public Hippo_Brazil HippoBrazil_SearchResultsPage(String Data)
	{
		 //String browserName = caps.getBrowserName();
		try
		{
            log("Sending details in to search box control",LogType.STEP);
			
			SearchData search = SearchData.fetch(Data);
			
			String Product = search.ProductSku;
			
			getCommand().isTargetVisible(Search_Icon);
	    	getCommand().click(Search_Icon);
	    	getCommand().sendKeys(SearchInput, Product);
	    	
			
			log("Clicking on search button",LogType.STEP);
			
			getCommand().click(SearchButton); 
	    	getCommand().waitFor(5);
			
            log("Getting count for the search results",LogType.STEP);
			
			String ProductsCount = getCommand().getText(Results);
			
			ProductsCount = ProductsCount.substring(0, 3);
			
            String ProductHeader = getCommand().getText(ResultsHeader);
			
			ProductHeader = ProductHeader.substring(24, 32);
			
			if(!ProductsCount.equals("0") && ProductHeader.equals(Product)) 
			{
				log("Search results screen displayed with "+ProductsCount+ " results for search:"+ Product,LogType.STEP);
				
				log("Checking two tabs (produto, spec) are present in page",LogType.STEP);
				getCommand().driver.findElement(By.xpath("//*[@id='koh-page-outer']/div/div/div[1]/div/div/button")).click();
					
				List<WebElement> values = getCommand().driver.findElements(By.xpath("//*[@id='koh-page-outer']/div/div/div[1]/div/div/ul/li/a"));
				
				for(WebElement val : values)
				{
					String Text = val.getText();
					if(Text.length()>8)
					{
						Text =Text.substring(0,7);
					}
					
					else
					{
						Text =Text.substring(0,4);
					}
					
					if(val.isDisplayed())
					{
						
							if(Text.equalsIgnoreCase("PRODUTO") || Text.equalsIgnoreCase("SPEC"))
							{
								log("Tab text "+ Text+ " is present as expected",LogType.STEP);
							}
							
							else
							{
								log("Tab text "+Text+ " is not present as expected",LogType.ERROR_MESSAGE);
								Assert.fail("Tab text "+Text+ " is not present as expected");
							}
					
					}
					
					else
					{
						log("Tab "+ Text+ "is not present in page",LogType.ERROR_MESSAGE);
						Assert.fail("Tab "+ Text+ "is not present in page");
					}
				}
				
				log("Checking Product category,filters, Sort by drop down and product grid ",LogType.STEP);
				getCommand().isTargetVisible(Product_Category);
				log("Product category is available in the search results page ",LogType.STEP);
				getCommand().isTargetVisible(PromoGrid);
				log("Promo grids are available in the search results page ",LogType.STEP);
				getCommand().isTargetVisible(SortBy);
				log("Sort by dropdown is available in the search results page ",LogType.STEP);
				getCommand().isTargetVisible(Filter);
				log("Product filter is available in the search results page ",LogType.STEP);
			}
			
			else
			{
				log("No results got fetched for search: "+Product,LogType.ERROR_MESSAGE);
				Assert.fail("No results got fetched for search: "+Product);
			}
		}
		
		catch(Exception ex)
		{
			ex.getMessage();
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	//Method to verify Quick view window of products
	public Hippo_Brazil HippoBrazil_QuickView() throws InterruptedException
	{		
		try
		{
			log("Navigating to product list page",LogType.STEP);
			
			getCommand().waitForTargetVisible(Hamburger_Icon).click(Hamburger_Icon);
            log("Navigating to product list page",LogType.STEP);
            getCommand().isTargetPresent(KitchenBtn);
            getCommand().click(KitchenBtn);
			
            getCommand().waitFor(3);
            getCommand().click(Kitchen_SubMenu);
			
			getCommand().waitFor(3);
			
	        List<WebElement> Products = getCommand().driver.findElements(By.xpath("//*[@id='koh-page-outer']/div/div/section[2]/div[3]/div"));
		    
			int Count =1;
			int j =3;
			
			log("Clicking on plus icon of each product",LogType.STEP);
			
			for(WebElement Product : Products)
			{		
				js.executeScript("arguments[0].scrollIntoView(true);", Product);
				
				Action.moveToElement(Product).build().perform();
				
				String Xpath = "//*[@id='koh-page-outer']/div/div/section[2]/div[3]/div["+Count+"]/div/div[2]/div/button/span";		
				
				WebElement PlusIcon = getCommand().driver.findElement(By.xpath(Xpath));
				
				if(PlusIcon.isDisplayed())
				{
					log("Clicking on "+Count+" product plus icon",LogType.STEP);
					
					js.executeScript("arguments[0].click();", PlusIcon);
					
					getCommand().waitFor(5);
					
					String ImageXpath = "/html/body/div["+j+"]/div/div/a/img";
					String ProductDetailsXpath = "/html/body/div["+j+"]/div/div[2]";
					
					
					WebElement ProductImage = getCommand().driver.findElement(By.xpath(ImageXpath));
					WebElement ProductDetails = getCommand().driver.findElement(By.xpath(ProductDetailsXpath));

					 Point point_Image = ProductImage.getLocation();
					 int xcord_Image = point_Image.getX();
					 
					 Point point_ProductDetails = ProductDetails.getLocation();
					 int xcord_ProductDetails = point_ProductDetails.getX();
					 
					 log("Checking image is on left and product details on right",LogType.STEP);
					 
					 if(xcord_Image<xcord_ProductDetails)
					 {
						 log("Product image is on left side and  Product description, SKU#, color swatches and compare CTA on right side",LogType.STEP);
						 
						 log("Checking product details are present in quivk view",LogType.STEP);
						 
						 WebElement Description = getCommand().driver.findElement(By.xpath("/html/body/div["+j+"]/div/div[2]/span/a"));
						 WebElement Sku = getCommand().driver.findElement(By.xpath("/html/body/div["+j+"]/div/div[2]/span[2]"));
						 WebElement Comparar = getCommand().driver.findElement(By.xpath("/html/body/div["+j+"]/div/div[2]/div/div"));
						 WebElement ColorSwatches = getCommand().driver.findElement(By.xpath("/html/body/div["+j+"]/div/div[2]/div/ul"));
						 
						 if(Description.isDisplayed() && Sku.isDisplayed() && Comparar.isDisplayed() && ColorSwatches.isDisplayed()) 
						 {
							 log("Product description, SKU#, color swatches and compare CTA are present in quick view",LogType.STEP);
						 }
						 
						 else
						 {
							 log("Either Product description/SKU#/color swatches/compare CTA are not present in quick view for "+Count+" product",LogType.ERROR_MESSAGE);
							 Assert.fail("Either Product description/SKU#/color swatches/compare CTA are not present in quick view for "+Count+" product");
						 }
						 
						 getCommand().driver.findElement(By.xpath("/html/body/div["+j+"]/div/button")).click();
						 
						 Thread.sleep(5000);
					 }
					 
					 else
					 {
						 log("Product image is not on left side and  Product description, SKU#, color swatches and compare CTA are not on right side",LogType.ERROR_MESSAGE);
						 Assert.fail("Product image is not on left side and  Product description, SKU#, color swatches and compare CTA are not on right side");
					 }			 
				}
				
				else
				{
					log("Plus icon is not displayed after hoverig on product",LogType.ERROR_MESSAGE);
					 Assert.fail("Plus icon is not displayed after hoverig on product");
				}
				j++;
				Count++;

			}
		}	
		catch(Exception ex)
		{
			ex.getMessage();
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	//Method to verify Hero image navigation using Nav dots & Arrows
	public Hippo_Brazil HippoBrazil_HeroImageCrousel()
	{
		try
		{
			List<String> Imagetext_NavDots = new ArrayList<String>();
			List<String> Imagetext_NextArrow = new ArrayList<String>();
			List<String> Imagetext_PrevArrow = new ArrayList<String>();

			List<WebElement> NavDots = getCommand().driver.findElements(By.xpath("//*[@id='koh-page-outer']/div/div/div/div/div/div[1]/section/div/div[1]/ul/li"));
			
			int i=2;
			int j=1;
			int l=2;
			int n=5;
			
			log("Clicking on each nav dots and checking image changes for each nav dot click",LogType.STEP);
			
			for(WebElement NavDot : NavDots)
			{
				
				log("Clicking on "+j+" Nav dot",LogType.STEP);
				
				NavDot.click();
				
				getCommand().waitFor(2);
							
				WebElement Image= getCommand().driver.findElement(By.xpath("//*[@id='koh-page-outer']/div/div/div/div/div/div[1]/section/div/div[2]/div/div/div["+i+"]/div[2]/span/span[@class='koh-banner-title']"));
				
				if(Image.isDisplayed())
				{
					log("Getting image title and adding to list",LogType.STEP);
					String ImageText = Image.getText();
					Imagetext_NavDots.add(ImageText);				
					i++;				
				}
				
				else
				{
					log("Image is not displayed after clicking on "+j+" Nav dot",LogType.ERROR_MESSAGE);
					Assert.fail("Image is not displayed after clicking on "+j+" Nav dot");
				}
				j++;
		    }
			
			log("Comparing title's in list with each other and checking they are unique",LogType.STEP);
			
			Assert.assertTrue(CompareDataFromSameList(Imagetext_NavDots),"Clicking on each navigation dots, image not changes");
			
			log("Clicking on each navigation dots, image changes",LogType.STEP);
			
			log("Checking Scroll feature using next arrow",LogType.STEP);
			
			for(int k=1;k<=NavDots.size();k++)
			{
				log("Clicking on next arrow and checking image changes",LogType.STEP);
				
				getCommand().click(HeroNextArrow);
				
				Thread.sleep(2000);
				
				WebElement Image= getCommand().driver.findElement(By.xpath("//*[@id='koh-page-outer']/div/div/div/div/div/div[1]/section/div/div[2]/div/div/div["+l+"]/div[2]/span/span[@class='koh-banner-title']"));
				
				if(Image.isDisplayed())
				{
					log("Getting image title and adding to list",LogType.STEP);
					String ImageText = Image.getText();
					Imagetext_NextArrow.add(ImageText);				
					l++;				
				}
				
				else
				{
					log("Image is not displayed after clicking on next arrow",LogType.ERROR_MESSAGE);
					Assert.fail("Image is not displayed after clicking on next arrow");
				}
			}
			
            log("Comparing title's in list with each other and checking they are unique",LogType.STEP);
			
			Assert.assertTrue(CompareDataFromSameList(Imagetext_NextArrow),"Clicking on next arrow, image not changes and scroll is not working");
			
			log("Clicking on next arrow, image changes and scroll is working",LogType.STEP);
			
			getCommand().mouseHover(HeroNextArrow).click(HeroNextArrow);
			
			log("Checking Scroll feature using previous arrow",LogType.STEP);

			for(int m=1;m<=NavDots.size();m++)
			{
				log("Clicking on prev arrow and checking image changes",LogType.STEP);
				
				getCommand().click(HeroPrevArrow);
				
				Thread.sleep(2000);
				
				WebElement Image= getCommand().driver.findElement(By.xpath("//*[@id='koh-page-outer']/div/div/div/div/div/div[1]/section/div/div[2]/div/div/div["+n+"]/div[2]/span/span[@class='koh-banner-title']"));
				
				if(Image.isDisplayed())
				{
					log("Getting image title and adding to list",LogType.STEP);
					String ImageText = Image.getText();
					Imagetext_PrevArrow.add(ImageText);				
					n--;				
				}
				
				else
				{
					log("Image is not displayed after clicking on prev arrow",LogType.ERROR_MESSAGE);
					Assert.fail("Image is not displayed after clicking on prev arrow");
				}
			}
			
            log("Comparing title's in list with each other and checking they are unique",LogType.STEP);
			
			Assert.assertTrue(CompareDataFromSameList(Imagetext_PrevArrow),"Clicking on prev arrow, image not changes and scroll is not working");
			
			log("Clicking on prev arrow, image changes and scroll is working",LogType.STEP);
        
		}
		
		catch(Exception ex)
		{
			ex.getMessage();
			Assert.fail(ex.getMessage());
		}

		return this;
	}
	
	//Method to verify navigation after clicking on each image from hero carousel
	public Hippo_Brazil HippoBrazil_HeroImageNavigation()
	{
		try
		{	
            List<WebElement> NavDots = getCommand().driver.findElements(By.xpath("//*[@id='koh-page-outer']/div/div/div/div/div/div[1]/section/div/div[1]/ul/li"));
			
			int i=2;
			int j=1;
			
			log("Clicking on each nav dots and clicking on image displayed for each nav dot",LogType.STEP);
			
			for(int k=0;k<NavDots.size();k++)
			{
				List<WebElement> Navdots = getCommand().driver.findElements(By.xpath("//*[@id='koh-page-outer']/div/div/div/div/div/div[1]/section/div/div[1]/ul/li"));
				
				String PageUrl = getCommand().getPageUrl();
				
				log("Clicking on "+j+" Nav dot",LogType.STEP);
				
				//((JavascriptExecutor) getCommand().driver).executeScript("arguments[0].scrollIntoView(true);", Navdots.get(k));
				
				getCommand().waitFor(3);
				
				Navdots.get(k).click();
				
				getCommand().waitFor(2);
				
                WebElement Image= getCommand().driver.findElement(By.xpath("//*[@id='koh-page-outer']/div/div/div/div/div/div[1]/section/div/div[2]/div/div/div["+i+"]/div[1]/a"));
				
				if(Image.isDisplayed())
				{
					String ImageText = Image.getText();	
					
					log("Clicking on image "+ ImageText,LogType.STEP);
					
					Image.click();
					
					getCommand().waitFor(4);
					
					String CurrentPageUrl = getCommand().getPageUrl();
					
					Assert.assertNotEquals(PageUrl, CurrentPageUrl,"Clicking on image "+ ImageText+ " is not redirecting to its site");
					
					log("Clicking on image "+ ImageText+ " is redirecting to its site: "+getCommand().getPageTitle(),LogType.STEP);
					
					getCommand().goBack();
					
					getCommand().waitFor(4);
								
					i++;				
				}
				
				else
				{
					log("Failed to click on "+j+" Image",LogType.ERROR_MESSAGE);
					Assert.fail("Failed to click on "+j+" Image");
				}
				j++;
		    }
		}
		
		catch(Exception ex)
		{
			ex.getMessage();
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	//Method to verify Filters
	public Hippo_Brazil HippoBrazil_Filter()
	{		
		try
		{
			log("Navigating to product list page",LogType.STEP);
			
			getCommand().waitForTargetVisible(Hamburger_Icon).click(Hamburger_Icon);
            log("Navigating to product list page",LogType.STEP);
            getCommand().isTargetPresent(KitchenBtn);
            getCommand().click(KitchenBtn);
			
            getCommand().waitFor(3);
            getCommand().click(Kitchen_SubMenu);
			
			getCommand().waitFor(3);
			
			log("Checking filters work properly",LogType.STEP);	
			getCommand().click(Filter);
			
			log("Clicking on Tippo De Instalaco filter",LogType.STEP);
			
			getCommand().waitFor(2);
			
			getCommand().isTargetPresent(FilterTipoDe);
			
			getCommand().click(FilterTipoDe);
			
			log("Getting total filter options available in Tippo De Instalacao filter",LogType.STEP);
			
			List<WebElement> FilterList = getCommand().driver.findElements(By.xpath("//*[@id='koh-page-outer']/div/div/section[2]/div[2]/div[2]/div[2]/div[2]/div/ul/li/a"));
			
			log(FilterList.size()+" filter options available in Tippo De Instalacao filter",LogType.STEP);
			
			log("Clicking on first filter",LogType.STEP);
			
			String Filtertext = FilterList.get(0).getAttribute("data-facet-value");
			
			FilterList.get(0).click();
			
			getCommand().waitFor(4);
			getCommand().click(Filter);
			
			List<WebElement> FilterListafterselecting = getCommand().driver.findElements(By.xpath("//*[@id='koh-page-outer']/div/div/section[2]/div[2]/div[2]/div[2]/div[2]/div/ul/li/button"));
			
			String Backgroundcolor = FilterListafterselecting.get(0).getCssValue("background-color");
			
			String Filtercount =getCommand().getText(FilterCount);
			
			Filtercount = Filtercount.substring(1,3);
			
		
				if(FilterListafterselecting.size()==1 && FilterListafterselecting.get(0).getAttribute("data-facet-value").equals(Filtertext) && Backgroundcolor.contains("rgba(229, 229, 229, 1)")) 
				{				
					log(Filtertext+ " is selected and highlighted on a gray box",LogType.STEP);				
				}
				
				else
				{
					log(Filtertext+ " is not selected",LogType.STEP);
					Assert.fail(Filtertext+ " is not selected");
				}
		

	        log("Getting total no. of products present for the seleced filter",LogType.STEP);
			
			
			
			List<WebElement> Products = getCommand().driver.findElements(By.xpath("//*[@id='koh-page-outer']/div/div/section[2]/div[3]/div"));  
			
			if(Products.size() == Integer.parseInt(Filtercount) )
			{
				log("Product grid updates as per selected filter",LogType.STEP);
			}
			
			else
			{
				log("Product grid not updates as per selected filter",LogType.ERROR_MESSAGE);
				Assert.fail("Product grid not updates as per selected filter");
				
			}
			

			log("Checking color swatches displayed in 'Cor/Acabamento' filter",LogType.STEP);
			//getCommand().click(Filter);
			
			getCommand().isTargetPresent(FilterCorAcabamento);
			
			getCommand().click(FilterCorAcabamento);
			String Filterstatus = getCommand().getAttribute(FilterCorAcabamento,"class");
			if(Filterstatus.contains("open")) 
			{
				log("'Cor/Acabamento' filter is open",LogType.STEP);
				
				List<WebElement> ColorSwatches = getCommand().driver.findElements(By.xpath("//*[@id='koh-page-outer']/div/div/section[2]/div[2]/div[2]/div[2]/div[1]/div/ul/li"));
				
				log("Checking color swatches are present in 'Cor/Acabamento' filter",LogType.STEP);
				
				if(ColorSwatches.size()>0)
				{
					log("color swatches are present in 'Cor/Acabamento' filter",LogType.STEP);
				}
				else {
					log("color swatches are not present in 'Cor/Acabamento' filter",LogType.STEP);
				}
			
			
			
		}
		}
		
		catch(Exception ex)
		{
			ex.getMessage();
			Assert.fail(ex.getMessage());
		}
		return this;
	}	
	
	//Method to verify Category page template
	public Hippo_Brazil HippoBrazil_CategoryPageTemlate() throws InterruptedException
	{
		try 
		{
			
			getCommand().waitForTargetVisible(Hamburger_Icon).click(Hamburger_Icon);
			log("Navigating to Cozinha > Cubas de cozinha",LogType.STEP);
			
	        getCommand().click(KitchenBtn);
	        getCommand().waitFor(2);
	        getCommand().click(Kitchen_SubMenu);
	        
	        getCommand().waitForTargetPresent(CozinhaNav_HeroImage, 3);
			
	        log("Checking Hero Image is displayed",LogType.STEP);
			
			if(getCommand().isTargetVisible(CozinhaNav_HeroImage))
			{
				log("Hero Image is dispalyed",LogType.STEP);
			}
			
			else
			{
				log("Hero Image is not dispalyed",LogType.ERROR_MESSAGE);
				Assert.fail("Hero Image is not dispalyed");
			}
	        
			log("Checking Sort drop down is displayed",LogType.STEP);
	        getCommand().waitForTargetPresent(CozinhaNav_Sort, 3);
	        
	       // String browserName = caps.getBrowserName();
			
			if(getCommand().isTargetVisible(CozinhaNav_Sort))
			{
				log("Sort drop down is Visible",LogType.STEP);
				
				
					getCommand().click(CozinhaNav_Sort);
			
				
				List<WebElement> SortOptions = getCommand().driver.findElements(By.xpath("//*[@id='koh-page-outer']/div/div/section[2]/div[1]/ul/li"));
				for(WebElement SortOption : SortOptions)
				{
						log("Sort drop down is displayed with option: "+SortOption.getText(),LogType.STEP);
						getCommand().click(CozinhaNav_Sort);
						
				}
			}
			
			else
			{
				log("Sort option is not visible",LogType.ERROR_MESSAGE);
				Assert.fail("Sort option is not visible");
			}
			getCommand().waitFor(3);
			log("Checking Filter Category is dispalyed",LogType.STEP);
			
			if(getCommand().isTargetVisible(CozinhaNav_FilterCategory))
			{
				log("Filter Category is dispalyed",LogType.STEP);
				getCommand().click(CozinhaNav_FilterCategory);
				
				log("Checking Cubas de cozinha category is preselected",LogType.STEP);
				
				String SelectedCategoryText = getCommand().getText(CozinhaNav_preselectedfilter);
				if(getCommand().isTargetPresent(CozinhaNav_preselectedfilter) && SelectedCategoryText.equals("Cubas de cozinha"))
				{
					log("Cubas de cozinha category is preselected",LogType.STEP);
				}
				else
				{
					log("Cubas de cozinha category is not preselected",LogType.ERROR_MESSAGE);
					Assert.fail("Cubas de cozinha category is not preselected");
				}
				
				log("Checking Filters (all closed by default)",LogType.STEP);
				
				List<WebElement> FilterContent = getCommand().driver.findElements(By.xpath("//*[@id='koh-page-outer']/div/div/section[2]/div[2]/div[2]/div[2]/div/span"));
				for(WebElement Filtercontent : FilterContent) {
					String Status = Filtercontent.getAttribute("class");
					
					if(!Status.contains("open")) {
						log("Filter "+ Filtercontent.getText()+ " is closed by default",LogType.STEP);
					}
					
					else {
						log("Filter "+ Filtercontent.getText()+ " is open by default",LogType.ERROR_MESSAGE);
						Assert.fail("Filter "+ Filtercontent.getText()+ " is open by default");
					}
				}
			}
			else
			{
				log("Filter Category is not displayed",LogType.ERROR_MESSAGE);
			}
			
			log("Checking Product grid is displayed",LogType.STEP);
			
			if(getCommand().isTargetVisible(CozinhaNav_ProductGrid))
			{
				List<WebElement> products = getCommand().driver.findElements(By.xpath("//*[@id='koh-page-outer']/div/div/section[2]/div[3]/div"));
				log("Product grid is dispalyed with "+ products.size()+" products",LogType.STEP);
			}
			else
			{
				log("Product grid is not displayed",LogType.ERROR_MESSAGE);
				Assert.fail("Product grid is not displayed");
			}
		}
		
		catch(Exception ex)
		{
			ex.getMessage();
			Assert.fail(ex.getMessage());
		}	    
		
		return this;
	}
	
	//Method to verify product display page template
	public Hippo_Brazil VerifyPDPTemplatePage(String Data)
    {
		try
		{
            SearchData search = SearchData.fetch(Data);
			
			String Product = search.ProductSku;
		    log("Enetring the product id into search box",LogType.STEP);

			getCommand().isTargetVisible(Search_Icon);
	    	getCommand().click(Search_Icon);
	    	getCommand().sendKeys(SearchInput, Product);
	    	log("Clicking on search button",LogType.STEP);
	    	getCommand().click(SearchButton); 
	    	getCommand().waitFor(5);
	           
	           getCommand().waitForTargetVisible(ProductId_PDP);

	           String Url = getCommand().driver.getCurrentUrl();

	           String ProductID = getCommand().getText(ProductId_PDP);

	           ProductID = ProductID.substring(2);

	           if(Url.contains("product-detail") && ProductID.equals(Product)) {

	                  log("Correct product page is displayed",LogType.STEP);
	           }

	           else
	           {
	                log("Wrong product got displayed. Expected: "+Product+ ", Actual: "+ProductID,LogType.ERROR_MESSAGE);
	                Assert.fail("Wrong product got displayed. Expected: "+Product+ ", Actual: "+ProductID);
	           }
	           
	           WebElement ThumbNail=getCommand().driver.findElement(By.xpath("//*[@id='koh-page-outer']/div/div/section/div[1]/div[1]/div[3]/div/div/div/div/img"));
	           
	           if(ThumbNail.isDisplayed())
	           {
	               log("ThumbNails are displayed",LogType.STEP);
	           }
	           else
	           {
	               log("ThumbNails are not displayed",LogType.ERROR_MESSAGE);
	               Assert.fail("ThumbNails are not displayed");
	           }
	           
	           WebElement Product1=getCommand().driver.findElement(By.xpath("//*[@id='koh-page-outer']/div/div/section/div[1]/div[3]/div[1]/div[2]"));
	           if(Product1.getText()!=null)
	           {
	               log("Product name is displayed",LogType.STEP);
	           }
	           else
	           {
	               log("Product name is not displayed",LogType.ERROR_MESSAGE);
	               Assert.fail("Product name is not displayed on the main page");
	           }
	           
	           WebElement SKU=getCommand().driver.findElement(By.xpath("//*[@id='koh-page-outer']/div/div/section/div[1]/div[3]/div[2]/span"));
	           if(SKU.getText()!=null)
	           {
	               log("SKU# is displayed",LogType.STEP);
	           }
	           else
	           {
	               log("SKU# is not displayed",LogType.ERROR_MESSAGE);
	               Assert.fail("SKU# is not displayed");
	           }   

	           WebElement ColorSwatch=getCommand().driver.findElement(By.xpath("//*[@id='koh-page-outer']/div/div/section/div[1]/div[3]/div[2]/div/ul/li/span/button/span"));
	           
	           if(ColorSwatch.isDisplayed())
	           {
	               log("Color Swatches are displayed",LogType.STEP);
	           }
	           else
	           {
	               log("Color Swatches are not displayed",LogType.ERROR_MESSAGE);
	               Assert.fail("Color Swatches are not displayed");
	           }   

	           WebElement Service=getCommand().driver.findElement(By.xpath("//*[@id='koh-page-outer']/div/div/section/div[3]/div[1]/div[1]/span"));
	           
	           if(Service.isDisplayed())
	           {
	               log("Service & Support is displayed",LogType.STEP);
	           }
	           else
	           {
	               log("Service & Support is not displayed",LogType.ERROR_MESSAGE);
	               Assert.fail("Service & Support is not displayed");
	           }  

	           List<WebElement> Information=getCommand().driver.findElements(By.xpath("//*[@id='koh-page-outer']/div/div/section/div[3]/div[1]/div[2]/span"));
	           
	           if(Information.size()!=0)
	           {
	               log("Resources-Technical Information is displayed",LogType.STEP);
	           }
	           else
	           {
	               log("Resources-Technical Information is not displayed",LogType.ERROR_MESSAGE);
	               Assert.fail("Resources-Technical Information is not displayed");
	           }
		}

        catch(Exception ex)
   		{
   			ex.getMessage();
   			Assert.fail(ex.getMessage());
   		}	    
   		
   		return this;
    }
	
	//Method to verify sorting
	public Hippo_Brazil HippoBrazil_Sort()
	{
		try
		{
			ArrayList<String> Defaultvalues= new ArrayList<String>();
			ArrayList<String> valuesAscOrder = new ArrayList<String>();
			ArrayList<String> valuesDscOrder = new ArrayList<String>();
			ArrayList<String> Relavancevalues = new ArrayList<String>();
			
			getCommand().waitForTargetVisible(Hamburger_Icon).click(Hamburger_Icon);
            log("Navigating to product list page",LogType.STEP);
            getCommand().isTargetPresent(KitchenBtn);
            getCommand().click(KitchenBtn);
			
            getCommand().waitFor(3);
            getCommand().click(Kitchen_SubMenu);
			
			getCommand().waitFor(3);
			
			List<WebElement> ProductsBeforeSort = getCommand().driver.findElements(By.xpath("//*[@id='koh-page-outer']/div/div/section[2]/div[3]/div/div/div/a/span[@class='koh-product-description']"));
			
			for(int l=0;l<ProductsBeforeSort.size();l++)
			{
				String Text = ProductsBeforeSort.get(l).getText();
				
				Defaultvalues.add(Text);  
			}
			
			log("Clicking on sort drop down",LogType.STEP);
			//String browserName = caps.getBrowserName();
			
			getCommand().waitFor(3);
			
				getCommand().click(CozinhaNav_Sort);
			
			
			log("Checking sort option is displayed in page",LogType.STEP);
			
	        if(getCommand().isTargetVisible(CozinhaNav_Sort))
			{
				log("Sort drop down is Visible",LogType.STEP);

				List<WebElement> SortOptions = getCommand().driver.findElements(By.xpath("//*[@id='koh-page-outer']/div/div/section[2]/div[1]/ul/li"));
				
				for(int l=0 ; l<SortOptions.size();l++)
				{
					List<WebElement> Sortoptions = getCommand().driver.findElements(By.xpath("//*[@id='koh-page-outer']/div/div/section[2]/div[1]/ul/li"));

					String valueIs = Sortoptions.get(l).getText();
					
					if(valueIs.equals("Nome Z-A")) 
					{	
						js.executeScript("arguments[0].click();", Sortoptions.get(l));
						
						//Sortoptions.get(l).click();
						
						log("Selecting sort option "+valueIs,LogType.STEP);
						
						getCommand().waitFor(4);
						
						log("Getting all product name and adding to list after sorted with "+valueIs,LogType.STEP);
						
						List<WebElement> Product = getCommand().driver.findElements(By.xpath("//*[@id='koh-page-outer']/div/div/section[2]/div[3]/div/div/div/a/span[@class='koh-product-description']"));
						
						for(int k=0;k<Product.size();k++)
						{
							String Text = Product.get(k).getText();
							
							valuesDscOrder.add(Text);  
						}
					}
					
					if(valueIs.equals("Nome A-Z")) 
					{	
						log("Selecting sort option "+valueIs,LogType.STEP);
						
						js.executeScript("arguments[0].click();", Sortoptions.get(l));
						
						//Sortoptions.get(l).click();

						getCommand().waitFor(4);
						
						log("Getting all product name and adding to list after sorted with "+valueIs,LogType.STEP);
						
						List<WebElement> Product = getCommand().driver.findElements(By.xpath("//*[@id='koh-page-outer']/div/div/section[2]/div[3]/div/div/div/a/span[@class='koh-product-description']"));
						
						for(int k=0;k<Product.size();k++)
						{
							String Text = Product.get(k).getText();
							
							valuesAscOrder.add(Text);  
						}
						
						getCommand().click(CozinhaNav_Sort);
					}
					
					if(valueIs.equals("Relevāncia"))
					{
                        log("Selecting sort option "+valueIs,LogType.STEP);
						
						Sortoptions.get(l).click();

						getCommand().waitFor(4);
						
						log("Getting all product name and adding to list after sorted with "+valueIs,LogType.STEP);
						
						List<WebElement> Product = getCommand().driver.findElements(By.xpath("//*[@id='koh-page-outer']/div/div/section[2]/div[3]/div/div/div/a/span[@class='koh-product-description']"));
						
						for(int k=0;k<Product.size();k++)
						{
							String Text = Product.get(k).getText();
							
							Relavancevalues.add(Text);  
						}
						
						getCommand().click(CozinhaNav_Sort);
					}
				}
				
				ArrayList<String> actualValueIs = new ArrayList<String>();
				
                actualValueIs.addAll(valuesAscOrder);
                
                Collections.sort(actualValueIs);
                
                log("Checking Sort values stored in list's",LogType.STEP);
                
                Assert.assertTrue(VerifySortByNameIs(valuesAscOrder,actualValueIs),"Sort is not working");
                
                Assert.assertTrue(VerifySortByNameIs(Defaultvalues,Relavancevalues),"Sort is not working");

                log("Sort is working",LogType.STEP);
			}
		}
		
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		
		return this;
	}
	
	// Method to verify that product are displayed in alphabetical order when sorted by Name/Reverse order & Relevance	
    public static boolean VerifySortByNameIs(List<String> ExpectedVal ,List<String> ActualVal)
    {
           boolean status = false;
           
           if(ExpectedVal.size() ==  ActualVal.size())
           {
                 if(ExpectedVal.equals(ActualVal))
                        status = true;
           }
                                      
                        
           return status;
    }

    //Method to verify data from same list
	public boolean CompareDataFromSameList(List<String> list)
	{
		
		boolean status = true;
		label:
	    for (int i = 0; i < list.size()-1; i++) 
		{
			for (int k = i+1; k < list.size(); k++) 
			{			
				if(list.get(i).equals(list.get(k)))
				{
					status=false;
					break label;
					
				}	
			}				
		}
		
		return status;
	}

}
