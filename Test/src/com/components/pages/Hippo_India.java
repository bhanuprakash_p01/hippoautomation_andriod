
package com.components.pages;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.components.repository.SiteRepository;
import com.components.yaml.SearchData;
import com.iwaf.framework.components.Target;
import com.iwaf.framework.components.IReporter.LogType;

public class Hippo_India extends SitePage {

	public Hippo_India(SiteRepository repository) {
		super(repository);
	}

	public Hippo_India atHippoHomePage_India() {
		log("Launched Kohler Site", LogType.STEP);
		return this;
	}

	public static final Target BathroomNav = new Target("BathroomNav", "(//a[contains(text(),'Bathroom')])[1]",Target.XPATH);
	public static final Target BathroomSubNav = new Target("BathroomSubNav", "(//a[@href='/browse/Bathroom'])[2]",Target.XPATH);
	public static final Target BathroomWallMount = new Target("BathroomWallMount","(//a[contains(text(),'Wall-Mount')])[1]", Target.XPATH);
	public static final Target SortBy = new Target("SortBy", "//button[@class='koh-sort-by']", Target.XPATH);
	public static final Target CareandCleaning = new Target("CareandCleaning","//a[contains(text(),'Care & Cleaning')]", Target.XPATH);
	public static final Target CareDropdown = new Target("Dropdown", "//button[@class='koh-article-select']",Target.XPATH);
	public static final Target Kitchen = new Target("Kitchen", "(//a[contains(text(),'KITCHEN')])[1]", Target.XPATH);
	public static final Target KitchenFaucets = new Target("KitchenFaucets", "//a[contains(text(),'Kitchen Faucets')]",Target.XPATH);
	public static final Target StoreLocator = new Target("StoreLocator","//*[@id=\"koh-primary-nav-menu\"]/div[2]/div[1]/ul[1]/li[1]/a", Target.XPATH);
	public static final Target SearchResults = new Target("SearchResults", "//p[contains(text(),'Search Results')]",Target.XPATH);
	public static final Target KohlerWorldWide = new Target("KohlerWorldWide","//*[@id=\"koh-primary-nav-menu\"]/div[2]/div[1]/ul[2]/li/a", Target.XPATH);
	public static final Target Hero = new Target("Hero", "//section[@class='c-koh-banner-carousel v-koh-hero']",Target.XPATH);
	public static final Target PromoModules = new Target("Promomodules","//section[@class='c-koh-promo-grid v-koh-scattered']", Target.XPATH);
	public static final Target ProductId = new Target("ProductId", "//span[@class='koh-product-sku']", Target.XPATH);
	public static final Target PreSelected = new Target("PreSelected", "//span[contains(text(),'Kitchen Faucets')]",Target.XPATH);
	public static final Target ProductImage = new Target("ProductImage", "//div[@class='koh-product-image']",Target.XPATH);
	public static final Target SortAtoZ = new Target("SortA-Z", "//span[contains(text(),'Name A-Z')]", Target.XPATH);
	public static final Target SortZtoA = new Target("SortZtoA", "//span[contains(text(),'Name Z-A')]", Target.XPATH);
	public static final Target EnquiryNow = new Target("EnquiryNow", "/html/body/button/span",Target.XPATH);
	public static final Target PlaceAnEnquiry = new Target("PlaceAnEnquiry", "//h1[@class='koh-contact-title']",Target.XPATH);
	public static final Target Literature = new Target("Literature","//li[@class='nav-item']/a[contains(text(),'Literature')]", Target.XPATH);
	public static final Target Ideas = new Target("Ideas", "//ul[@class='koh-nav-parents']/li[3]/a", Target.XPATH);
	public static final Target FootLiterature = new Target("FootLiterature", "//span[contains(text(),'Literature')]",Target.XPATH);
	public static final Target Pedestal = new Target("Pedestal","//li[@class='nav-item']/a[contains(text(),'Pedestal')]", Target.XPATH);
	public static final Target ClearResults = new Target("ClearResults", "//button[contains(text(),'Clear Results')]",Target.XPATH);
	public static final Target Compare = new Target("Compare", "(//a[contains(text(),'Compare')])[1]", Target.XPATH);
	public static final Target CompareHead = new Target("CompareHead", "//div[@class='koh-compare-header']/button[1]",Target.XPATH);
	public static final Target ContactUs = new Target("ContactUs", "//span[contains(text(),'Contact Us')]",Target.XPATH);
	public static final Target YesButton = new Target("YesButton", "//label[@for='yes']", Target.XPATH);
	public static final Target NoButton = new Target("NoButton", "//label[@for='no']", Target.XPATH);
	public static final Target Category = new Target("Category", "//h3[@class='koh-filter-group-title']", Target.XPATH);
	public static final Target QuickView = new Target("QuickView","(//button[@class='koh-product-quick-view-button'])[1]", Target.XPATH);
	public static final Target Image = new Target("Image", "//*[@id=\"koh-page-outer\"]/div/div/section/div[2]/div[2]/div/div[1]/a/div/img", Target.XPATH);
	public static final Target ProductDes = new Target("ProductDes", "(//span[@class='koh-product-description'])[1]",Target.XPATH);
	public static final Target SKU = new Target("SKU", "(//span[@class='koh-product-sku'])[1]", Target.XPATH);
	public static final Target Price = new Target("Price", "(//span[@class='koh-product-price'])[1]", Target.XPATH);
	public static final Target CareHeader = new Target("CareHeader", "//h2[contains(text(),'Care & Cleaning')]",Target.XPATH);

	//--------------------------------------Mobile xpaths----------------------------------------------------------------------
	
	public static final Target Hamburger_Icon = new Target("Hamburger_Icon", "//*[@id='koh-page-outer']/div/header/div/div[1]/div[2]/div[1]/nav/div[1]/button[1]",Target.XPATH);
	public static final Target SearchIcon = new Target("SearchIcon", "//*[@id=\"koh-page-outer\"]/div/header/div/div[1]/div[2]/div[1]/nav/div[1]/button[2]", Target.XPATH);
	public static final Target SearchInput = new Target("SearchInput", "//*[@id='koh-nav-searchbox']", Target.XPATH);
	public static final Target SearchBtn = new Target("SearchBtn", "(//*[@id='koh-nav-searchbutton'])[1]",Target.XPATH);
	public static final Target CategoryBtn = new Target("CategoryBtn", "//*[contains(text(),'Categories and Filters')]", Target.XPATH);
	public static final Target Help_footer = new Target("Help_footer", "(//*[@id='koh-page-outer']/div/footer/div[2]/ul/li/span/span)[3]", Target.XPATH);

	
	
	
	//*[@id="koh-page-outer"]/div/div/section/div/div[1]/span
		
	
	
	
	
	
	Capabilities caps = ((RemoteWebDriver) getCommand().driver).getCapabilities();
	JavascriptExecutor js = (JavascriptExecutor) getCommand().driver;

	// Verify HomePageLayout
	public Hippo_India VerifyHomePageLayout() {

		try {
			String HeroText = getCommand().getAttributeValue(Hero, "class");

			if (getCommand().isTargetPresent(Hero) && HeroText.contains("hero"))

			{

				log("Hero is displayed in HomePage", LogType.STEP);

			}

			else

			{

				log("Hero is not displayed in HomePage", LogType.ERROR_MESSAGE);
				Assert.fail("Hero is not displayed in HomePage");

			}

			String PromoModulesText = getCommand().getAttributeValue(PromoModules, "class");

			if (getCommand().isTargetPresent(PromoModules) && PromoModulesText.contains("promo-grid"))

			{

				log("PromoModules are displayed in HomePage", LogType.STEP);

			}

			else

			{

				log("PromoModules are not displayed in HomePage", LogType.ERROR_MESSAGE);
				Assert.fail("PromoModules are not displayed in HomePage");

			}

			if (getCommand().isTargetPresent(SearchIcon))

			{

				log("Search is displayed", LogType.STEP);

			}

			else

			{

				log("Search is not displayed in HomePage", LogType.ERROR_MESSAGE);
				Assert.fail("Search is not displayed in HomePage");

			}

			List<WebElement> FooterHeader = getCommand().driver
					.findElements(By.xpath("(//span[@class='koh-nav-section-title'])"));
			int NoOfFooterHeaders = FooterHeader.size();

			if (NoOfFooterHeaders == 4)

			{
				log("Footer is displayed in HomePage", LogType.STEP);

			} else {
				log("Footer is not displayed in HomePage", LogType.ERROR_MESSAGE);
				Assert.fail("Footer is not displayed in HomePage");

			}
			
			
			
			getCommand().waitForTargetVisible(Hamburger_Icon).click(Hamburger_Icon);
			log("Verify Home Page Layout", LogType.STEP);
			getCommand().waitForTargetPresent(StoreLocator);

			if (getCommand().isTargetPresent(StoreLocator)) {

				Assert.assertEquals(getCommand().getText(StoreLocator), "Store Locator");
				log("StoreLocator is displayed in HomePage", LogType.STEP);
			}

			else {
				log("StoreLocator is not displayed in HomePage", LogType.ERROR_MESSAGE);
				Assert.fail("StoreLocator is not displayed in HomePage");

			}

			if (getCommand().isTargetPresent(KohlerWorldWide)) {

				Assert.assertEquals(getCommand().getText(KohlerWorldWide), "KOHLER Worldwide");
				log("KohlerWorldWide is displayed in HomePage", LogType.STEP);
			}

			else {
				log("KohlerWorldWide is not displayed in HomePage", LogType.ERROR_MESSAGE);
				Assert.fail("KohlerWorldWide is not displayed in HomePage");

			}

			List<String> GlobalNav = new ArrayList<String>();

			GlobalNav.add("//*[@id='koh-primary-nav-menu']/ul/li[1]/a");

			GlobalNav.add("//*[@id='koh-primary-nav-menu']/ul/li[2]/a");

			GlobalNav.add("//*[@id='koh-primary-nav-menu']/ul/li[3]/a");

			GlobalNav.add("//img[contains(@src,'boldlook')]");

			for (String Xpath : GlobalNav)

			{

				WebElement Element = getCommand().driver.findElement(By.xpath(Xpath));

				String Text = Element.getText();

				if (Element.isDisplayed())

				{
					if (!Text.isEmpty())

					{
						if (Text.equalsIgnoreCase("BATHROOM") || Text.equalsIgnoreCase("KITCHEN")
								|| Text.equalsIgnoreCase("IDEAS")) {

							log("Correct Text: " + Text + " is displayed in Global Navigation", LogType.STEP);

						}

						else

						{
							log("Correct Text: " + Text + " is not displayed in Global Navigation",
									LogType.ERROR_MESSAGE);
							Assert.fail("Correct Text:" + Text + " is not displayed in Global Navigation");
						}

					}

					else

					{

						log("Blok is displayed in Global Navigation", LogType.STEP);

					}

				}

				else

				{
					if (!Text.isEmpty()) {

						log(Text + " is not displayed in Global Navigation", LogType.ERROR_MESSAGE);
						Assert.fail(Text + " is not displayed in Global Navigation");
					}

					else {

						log("Logo is not displayed in Global Navigation", LogType.ERROR_MESSAGE);
						Assert.fail("Logo is not displayed in Global Navigation");
					}

				}

			}
			
		}

		catch (Exception e) {
			Assert.fail(e.getMessage());
		}

		return this;
	}

	// Verify No Result Page
	public Hippo_India NoResultPage(String Data) {

		try {
			SearchData searchData = SearchData.fetch(Data);
			String name = searchData.keyword;
			log("Clicking of Search from Home Page", LogType.STEP);

			getCommand().isTargetVisible(SearchIcon);
			getCommand().click(SearchIcon);
		
			log("Entering keyword into the search box", LogType.STEP);

			getCommand().sendKeys(SearchInput, name);
			log("Clicking of search button", LogType.STEP);
			getCommand().isTargetVisible(SearchBtn);
			getCommand().click(SearchBtn);

		
				if (getCommand().getText(SearchResults).contains("0 Search Results"))

				{

					log("Expected No Result Page is displayed", LogType.STEP);

				}

				else {

					log("Expected No Result Page is not displayed", LogType.ERROR_MESSAGE);
					Assert.fail("Expected No Result Page is not displayed");
				}

		}

		catch (Exception e) {
			Assert.fail(e.getMessage());
		}

		return this;
	}

	// Verify PDP Display
	public Hippo_India VerifyPDPDisplay(String Data) {
		try {
			
			SearchData searchData = SearchData.fetch(Data);
			String name = searchData.keyword;
			


log("Clicking of Search from Home Page", LogType.STEP);

			getCommand().isTargetVisible(SearchIcon);
			getCommand().click(SearchIcon);
		
			log("Entering keyword into the search box", LogType.STEP);

			getCommand().sendKeys(SearchInput, name);
			log("Clicking of search button", LogType.STEP);
			getCommand().isTargetVisible(SearchBtn);
			getCommand().click(SearchBtn);
			getCommand().waitFor(5);

			String CurrentUrl = getCommand().driver.getCurrentUrl();

			log("Verify PDP display", LogType.STEP);

			if (CurrentUrl.contains(name) && getCommand().getText(ProductId).contains(name)) {

				log("Entered Product Page is displayed", LogType.STEP);

			}

			else {
				log("Entered Product Page is not displayed", LogType.ERROR_MESSAGE);
				Assert.fail("Entered Product Page is not displayed");
			}

			log("Verify PDP Template", LogType.STEP);

			/*int X = getCommand().driver.manage().window().getSize().getWidth();

			
			int x = ThumbNail.getLocation().getX();*/
			WebElement ThumbNail = getCommand().driver
					.findElement(By.xpath("(//img[@class='koh-product-iso-image'])[2]"));

			if (ThumbNail.isDisplayed()) {
				log("ThumbNails are displayed in the PDP page", LogType.STEP);
			} else {
				log("ThumbNails are not displayed in the PDP page", LogType.ERROR_MESSAGE);
				
			}

			WebElement Product = getCommand().driver
					.findElement(By.xpath("//div[@class='koh-product-short-description']"));
			

			if (Product.getText() != null) {
				log("Product name is displayed in the PDP page", LogType.STEP);
			} else {
				log("Product name is not displayed in the PDP page", LogType.ERROR_MESSAGE);
				
			}

			WebElement SKU = getCommand().driver.findElement(By.xpath("//span[@class='koh-product-sku']"));

		
			if (SKU.getText() != null) {
				log("SKU# is displayed in the PDP page", LogType.STEP);
			} else {
				log("SKU# is not displayed in the PDP page", LogType.ERROR_MESSAGE);
				
			}

			WebElement Price = getCommand().driver.findElement(By.xpath("//span[@class='value']"));

			String PriceRs = Price.getText();
			PriceRs = PriceRs.substring(3);
		

			if (PriceRs != null) {
				log("Price is displayed below to the  main image", LogType.STEP);
			} else {
				log("Price is not is not displayedbelow to the main image", LogType.ERROR_MESSAGE);
				
			}

			WebElement ColorSwatch = getCommand().driver.findElement(By.xpath("//button[@class='koh-product-color']"));

		
			if (ColorSwatch.isDisplayed() ) {
				log("Color Swatches are displayed to the right of the main image", LogType.STEP);
			} else {
				log("Color Swatches are not displayed to the right of the main image", LogType.ERROR_MESSAGE);
				
			}
			
			WebElement Enquiry = getCommand().driver.findElement(By.xpath("//*[@id='place-an-enquiry']"));
			
			if (Enquiry.isDisplayed() ) {
				log("Place an Enquiry CTA is displayed below color swatches", LogType.STEP);
			} else {
				log("Place an Enquiry CTA is not displayed below color swatches", LogType.ERROR_MESSAGE);
				
			}

			WebElement Store = getCommand().driver.findElement(By.xpath("//a[contains(text(),'Store Locator')]"));
			

			if (Store.isDisplayed()) {
				log("Store Locator CTA is displayed below color swatches", LogType.STEP);
			} else {
				log("Store Locator is not displayed below color swatches", LogType.ERROR_MESSAGE);
				
			}

			
			WebElement Share = getCommand().driver.findElement(By.xpath("(//button[@class='koh-product-button'])[1]"));

			if (Share.isDisplayed() ) {
				log("Share Icon is displayed below color swatches", LogType.STEP);
			} else {
				log("Share Icon is not displayed below color swatches", LogType.ERROR_MESSAGE);
				
			}

			WebElement Print = getCommand().driver.findElement(By.xpath("(//button[@class='koh-product-button'])[2]"));
		
			if (Print.isDisplayed()) {
				log("Print Icon is displayed below color swatches", LogType.STEP);
			} else {
				log("Print Icon is not displayed below color swatches", LogType.ERROR_MESSAGE);
				
			}

		

			List<WebElement> Features = getCommand().driver
					.findElements(By.xpath("//div[contains(text(),'Features')]"));

			if (Features.size() != 0) {
				log("Features are displayed below product description", LogType.STEP);
			} else {
				log("Features are not displayed below product description", LogType.ERROR_MESSAGE);
				
			}

			WebElement Service = getCommand().driver
					.findElement(By.xpath("//span[@class='koh-product-service-title']"));

			if (Service.isDisplayed()) {
				log("Service & Support is displayed below product description", LogType.STEP);
			} else {
				log("Service & Support is not displayed below product description", LogType.ERROR_MESSAGE);
				
			}

			List<WebElement> Information = getCommand().driver
					.findElements(By.xpath("//span[contains(text(),'Technical Information')]"));

			if (Information.size() != 0) {
				log("Technical Information is displayed below product description", LogType.STEP);
			} else {
				log("Technical Information is not displayed below product description", LogType.ERROR_MESSAGE);
				
			}

			List<WebElement> CAD = getCommand().driver
					.findElements(By.xpath("//span[contains(text(),'Cad Templates')]"));

			if (CAD.size() != 0) {
				log("CAD Templates are displayed below product descriptione", LogType.STEP);
			} else {
				log("CAD Templates are not displayed below product description", LogType.ERROR_MESSAGE);
				
			}

		}

		catch (Exception e) {
			Assert.fail(e.getMessage());
		}

		return this;

	}

	// Verify QuickView
	public Hippo_India VerifyQuickview() {
		try {
			getCommand().isTargetVisible(Kitchen);
			getCommand().click(Kitchen);
			getCommand().isTargetPresent(KitchenFaucets);
			getCommand().click(KitchenFaucets);
			getCommand().waitForTargetPresent(Hero);
			getCommand().waitForTargetPresent(Category);

			js.executeScript("window.scrollBy(0,600)");

			List<WebElement> products = getCommand().driver
					.findElements(By.xpath("//div[@class='koh-product-tile-actions']"));

			log("Verify QuickView", LogType.STEP);
			int k = 0;

			for (int i = 0; i < products.size(); i++) {

				if (k == 3) {
					k = 0;
					js.executeScript("window.scrollBy(0,400)");
				}

				WebElement element = products.get(i);
				Actions action = new Actions(getCommand().driver);

				action.moveToElement(element).build().perform();

				getCommand().waitFor(3);

				int j = i + 1;

				WebElement SKU = getCommand().driver
						.findElement(By.xpath("(//span[@class='koh-product-sku'])[" + j + "]"));

				WebElement QuickView = getCommand().driver
						.findElement(By.xpath("(//button[@class='koh-product-quick-view-button'])[" + j + "]"));
				if (QuickView.isDisplayed()) {
					log(" + symbol displays on top right corner", LogType.STEP);

					QuickView.click();

					Set<String> s1 = getCommand().driver.getWindowHandles();
					Iterator<String> it = s1.iterator();

					while (it.hasNext()) {
						String ChildWindow = it.next();

						getCommand().driver.switchTo().window(ChildWindow);

						getCommand().waitFor(5);

						WebElement Image = getCommand().driver
								.findElement(By.xpath("//*[@id='QuickView" + SKU.getText() + "']/div[1]/a/img"));
						if (Image.isDisplayed()) {
							log("Product image is on the left side in the QuickView", LogType.STEP);
						} else {
							log("Product image is not on the left side in the QuickView", LogType.ERROR_MESSAGE);
							Assert.fail("Product image is not on the left side in the QuickView");
						}

						WebElement Description = getCommand().driver
								.findElement(By.xpath("//*[@id='QuickView" + SKU.getText() + "']/div[2]/span[1]/a"));
						if (Description.getText() != null) {
							log("Product description is on the right side in the QuickView", LogType.STEP);
						}

						else {
							log("Product description is not on the right side in the QuickView", LogType.ERROR_MESSAGE);
							Assert.fail("Product description is not on the right side in the QuickView");
						}

						WebElement SKU1 = getCommand().driver
								.findElement(By.xpath("//*[@id='QuickView" + SKU.getText() + "']/div[2]/span[2]"));

						if (SKU1.getText() != null) {
							log("SKU# of the product is on the right side in the QuickView", LogType.STEP);
						} else {
							log("SKU# of the product is not on the right side in the QuickView", LogType.ERROR_MESSAGE);
							Assert.fail("SKU# of the product is not on the right side in the QuickView");
						}

						WebElement ColorSwatch = getCommand().driver.findElement(
								By.xpath("//*[@id='QuickView" + SKU.getText() + "']/div[2]/div/ul/li/span/button"));
						List<WebElement> ColorSwatches = getCommand().driver.findElements(
								By.xpath("//*[@id='QuickView" + SKU.getText() + "']/div[2]/div/ul/li/span/button"));

						if (ColorSwatch.isDisplayed() && ColorSwatches.size() != 0) {
							log("ColorSwatches of the product are displayed on the right side in the QuickView",
									LogType.STEP);
						}

						else {
							log("ColorSwatches of the product are not displayed on the right side in the QuickView",
									LogType.ERROR_MESSAGE);
							Assert.fail(
									"ColorSwatches of the product are not displayed on the right side in the QuickView");
						}

						WebElement Compare = getCommand().driver
								.findElement(By.xpath("//*[@id='QuickView" + SKU.getText() + "']/div[2]/div/div/a[1]"));

						if (Compare.isDisplayed()) {
							log("Compare CTA is displayed on the right side in the QuickView", LogType.STEP);
						}

						else {
							log("Compare CTA is not displayed on the right side in the QuickView",
									LogType.ERROR_MESSAGE);
							Assert.fail("Compare CTA is not displayed on the right side in the QuickView");
						}

						WebElement close = getCommand().driver
								.findElement(By.xpath("//*[@id='QuickView" + SKU.getText() + "']/button"));
						close.click();
						getCommand().waitFor(3);
						k++;

					}

				}

				else {
					log(" + symbol doesn't displays on top right corner", LogType.STEP);
					Assert.fail("+ symbol doesn't displays on top right corner");
				}
			}

		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}

		return this;
	}

	// Verify Care & Cleaning Page
	public Hippo_India VerifyCareandCleaningPage() throws InterruptedException {


		getCommand().waitForTargetVisible(Hamburger_Icon).click(Hamburger_Icon);
		getCommand().click(BathroomNav);
		 WebElement element=getCommand().driver.findElement(By.xpath("//*[@id='koh-primary-nav-menu']/ul/li[1]/div/div/ul[5]/li[5]/a"));
  	      JavascriptExecutor jse=(JavascriptExecutor)getCommand().driver;
  	       jse.executeScript("arguments[0].scrollIntoView(true);", element);
  	       Thread.sleep(3000);
		
  	    getCommand().click(CareandCleaning);
		getCommand().waitFor(2);
		String PageTitle = getCommand().driver.getTitle();
		String CurrentUrl = getCommand().driver.getCurrentUrl();

		log("Verify Care & Cleaning Page", LogType.STEP);

		try {

			if (PageTitle.contains("Care and Cleaning") && CurrentUrl.contains("care-and-cleaning")) {
				log("Clicking on the link is redirecting to correct page", LogType.STEP);
			} else {
				log("Clicking on the link is not redirecting to correct page", LogType.ERROR_MESSAGE);
				
			}
			getCommand().waitForTargetPresent(CareDropdown);
			getCommand().isTargetVisible(CareDropdown);

			getCommand().click(CareDropdown);

			js.executeScript("window.scrollBy(0,300)");

			List<WebElement> allLinks = getCommand().driver
					.findElements(By.xpath("//*[@class='koh-article-controls contained']/ul/li/a"));
			int count = allLinks.size();
			if (count != 0) {
				log("In Care & Cleaning Page drop-down is present", LogType.STEP);
			} else {
				log("In Care & Cleaning Page drop-down is not present", LogType.ERROR_MESSAGE);
				Assert.fail("In Care & Cleaning Page drop-down is not present");
			}
			List<WebElement> Paragraph = getCommand().driver
					.findElements(By.xpath("//div[@class='koh-simple-content-body']"));
			{

				if (Paragraph.size() != 0)

				{
					log("Small copy of information is present", LogType.STEP);

				}

				else

				{
					log("Small copy of information is not present", LogType.ERROR_MESSAGE);
					Assert.fail("Small copy of information is not present");

				}
				log("Select different options in the dropdown", LogType.STEP);
				for (int i = 1; i < count; i++) {
					getCommand().waitFor(2);
					String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL, Keys.RETURN);

					String SHeadText = getCommand().getText(CareHeader);
					WebElement Header = getCommand().driver
							.findElement(By.xpath("//h2[contains(text(),'Kitchen Sinks')]"));
					String HeaderText = Header.getText();

					allLinks.get(i).sendKeys(selectLinkOpeninNewTab);

					getCommand().waitFor(5);

					ArrayList<String> tabs2 = new ArrayList<String>(getCommand().driver.getWindowHandles());

					getCommand().driver.switchTo().window(tabs2.get(1));

					WebElement Header1 = getCommand().driver.findElement(
							By.xpath("//*[@id='koh-page-outer']/div/div/div/div/div/div[3]/div/section/div/h2"));

					if (HeaderText != Header1.getText()) {
						log(Header1.getText() + " " + "Dynamic Content is updating", LogType.STEP);
					} else {
						log(Header1.getText() + " " + "Dynamic Content is not updating", LogType.ERROR_MESSAGE);
						Assert.fail(Header1.getText() + " " + "Dynamic Content is not updating");
					}

					WebElement StaticHeader1 = getCommand().driver
							.findElement(By.xpath("//h2[contains(text(),'Care & Cleaning')]"));
					String SHeadText1 = StaticHeader1.getText();

					if (SHeadText.equals(SHeadText1)) {
						log("Static Header-Care & Cleaning is present", LogType.STEP);
					} else {
						log("Static Header-Care & Cleaning is not present", LogType.ERROR_MESSAGE);
						Assert.fail("Static Header-Care & Cleaning is not present");
					}
					getCommand().driver.close();

					getCommand().driver.switchTo().window(tabs2.get(0));
					getCommand().waitFor(3);
					getCommand().click(CareDropdown);

				}
			}
		}

		catch (Exception e) {
			Assert.fail(e.getMessage());
		}

		return this;

	}

    //Verify Category Page Template
	public void VerifyCategoryPageTemplates() {
		
		

		getCommand().waitForTargetVisible(Hamburger_Icon).click(Hamburger_Icon);
		
		getCommand().waitForTargetVisible(Kitchen).click(Kitchen);
		getCommand().waitFor(1);
		getCommand().click(KitchenFaucets);
		log("Verify Category Page Layout", LogType.STEP);

		try {
			getCommand().waitFor(3);

			if (getCommand().isTargetPresent(Hero)) {

				log("Hero Image is displayed", LogType.STEP);
			} else {
				log("Hero Image is not displayed", LogType.ERROR_MESSAGE);
				
			}
			getCommand().scrollTo(SortBy);
			getCommand().click(CategoryBtn);

			if (getCommand().getText(PreSelected).contains("Kitchen Faucets")) {
				log("Kitchen Faucets Category is pre-selected", LogType.STEP);

			} else {
				log("Kitchen Faucets Category is not pre-selected", LogType.ERROR_MESSAGE);
				

			}
			getCommand().waitFor(2);
			List<WebElement> Filters = getCommand().driver.findElements(By.xpath("//*[@id='koh-page-outer']/div/div/section[2]/div[2]/div[2]/div[2]/div/span"));

			for (int i = 1; i < Filters.size(); i++) {

				String Status = Filters.get(i).getAttribute("class");

				if (!Status.contains("open")) {
					log(Filters.get(i).getText() + " " + "Filter is closed by default", LogType.STEP);
				}

				else {
					log(Filters.get(i).getText() + " " + "Filter is not closed by default", LogType.ERROR_MESSAGE);
					Assert.fail(Filters.get(i).getText() + " " + "Filter is not closed by default");

				}
			}

			List<WebElement> SortBy = getCommand().driver.findElements(By.xpath("//button[@class='koh-sort-by']"));

			if (SortBy.size() != 0) {
				log("Sort by drop-down is present", LogType.STEP);

			} else {
				log("Sort by drop-down is not present", LogType.ERROR_MESSAGE);
				Assert.fail("Sort by drop-down is not present");

			}

			List<WebElement> ProductGrid = getCommand().driver
					.findElements(By.xpath("//div[@class='koh-search-results']"));

			if (ProductGrid.size() != 0)

			{
				log("ProductGrid is present", LogType.STEP);

			} else {
				log("ProductGrid is not present", LogType.ERROR_MESSAGE);

				Assert.fail("ProductGrid is not present");
			}

		}

		catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}

    //Verify Price Update
	public Hippo_India VerifyPriceUpdate(String Data) {

		SearchData searchData = SearchData.fetch(Data);
		String name = searchData.keyword;
		log("Clicking of Search from Home Page", LogType.STEP);

		getCommand().isTargetVisible(SearchIcon);
		getCommand().click(SearchIcon);
	
		log("Entering keyword into the search box", LogType.STEP);

		getCommand().sendKeys(SearchInput, name);
		log("Clicking of search button", LogType.STEP);
		getCommand().isTargetVisible(SearchBtn);
		getCommand().click(SearchBtn);
		getCommand().waitFor(5);
		log("Verify Price Update", LogType.STEP);

		try {

			getCommand().waitForTargetPresent(ProductImage);
			getCommand().isTargetPresent(ProductImage);
			getCommand().click(ProductImage);
			getCommand().waitFor(3);

			List<String> ProductPrices = new ArrayList<String>();

			List<WebElement> AllColors = getCommand().driver
					.findElements(By.xpath("//button[@class='koh-product-color']"));

			int k = 0, l = 0;
			for (WebElement ProductColor : AllColors) {
				ProductColor.click();
				getCommand().waitFor(3);
				WebElement Price = getCommand().driver.findElement(By.xpath("//span[@class='value']"));
				String ProductPrice = Price.getText();
				ProductPrice = ProductPrice.substring(3);

				ProductPrices.add(ProductPrice);

				for (int i = 0; i < ProductPrices.size(); i++) {
					for (int j = i + 1; j < ProductPrices.size(); j++) {

						if (ProductPrices.get(i) != ProductPrices.get(j)) {
							k++;
						}

						else {
							l++;
						}

					}
				}

			}
			if (k != 0 && l == 0) {
				log("Price is updating based on different color chips selection", LogType.STEP);

			}

			else {

				log("Price is not updating based on different color chips selection", LogType.ERROR_MESSAGE);
				Assert.fail("Price is not updating based on different color chips selection");

			}
		}

		catch (Exception e) {
			Assert.fail(e.getMessage());
		}
		return this;
	}

	// Verify Store Locator
	public Hippo_India VerifyStoreLocatorDisplay() {

		log("Verify Store Locator Display", LogType.STEP);

		log("Click on Store Locator on Hamburger menu", LogType.STEP);

		try {
			
			

			getCommand().waitForTargetVisible(Hamburger_Icon).click(Hamburger_Icon);
			getCommand().click(StoreLocator);

			getCommand().waitFor(5);

			Select Country = new Select(getCommand().driver.findElement(By.id("country")));
			List<WebElement> country = Country.getOptions();
			String SelectCountry = Country.getFirstSelectedOption().getText();

			getCommand().waitFor(5);

			Select State = new Select(getCommand().driver.findElement(By.id("state")));
			List<WebElement> state = State.getOptions();
			String SelectState = State.getFirstSelectedOption().getText();

			getCommand().waitFor(5);

			Select City = new Select(getCommand().driver.findElement(By.id("city")));
			List<WebElement> city = City.getOptions();
			String SelectCity = City.getFirstSelectedOption().getText();

			if (country.size() != 0 && state.size() != 0 && city.size() != 0) {
				log("Store Locator page displays with three dropdowns", LogType.STEP);
			} else {
				log("Store Locator page doesn't displays with three dropdowns", LogType.ERROR_MESSAGE);
				Assert.fail("Store Locator page doesn't displays with three dropdowns");
			}

			if (SelectCountry.trim().equalsIgnoreCase("India") && SelectState.trim().equalsIgnoreCase("Telangana")
					&& SelectCity.trim().equalsIgnoreCase("Hyderabad")) {
				log("dropdown menus are automatically filled using IP2 location", LogType.STEP);
			} else {
				log("dropdown menus are not automatically filled using IP2 location", LogType.ERROR_MESSAGE);
				Assert.fail("dropdown menus are not automatically filled using IP2 location");

			}
		}

		catch (Exception e) {
			Assert.fail(e.getMessage());
		}

		return this;

	}

	// Verify Footer Layout
	public Hippo_India VerifyFooterLayout()

	{
		getCommand().scrollTo(Help_footer);
		List<WebElement> FooterHeader = getCommand().driver
				.findElements(By.xpath("//*[@id='koh-page-outer']/div/footer/div[2]/ul/li/span/span"));
		int NoOfFooterHeaders = FooterHeader.size();

		log("Verify Footer Layout", LogType.STEP);

		try {
			if (NoOfFooterHeaders == 4)

			{
				log("Footer is composed of 4 columns", LogType.STEP);

			} else {
				log("Footer is composed of 4 columns", LogType.ERROR_MESSAGE);
				Assert.fail("Footer is composed of 4 columns");

			}

			for (int i = 1; i < NoOfFooterHeaders; i++)

			{

				String HeaderText = FooterHeader.get(i).getText();

				if (HeaderText.equalsIgnoreCase("Our Company") || HeaderText.equalsIgnoreCase("Kohler Co.")
						|| HeaderText.equalsIgnoreCase("Help") || HeaderText.equalsIgnoreCase("Social")) {

					log("Expected Column text: " + HeaderText + ", is displayed under Footer section", LogType.STEP);

				}

				else {

					log("Expected Column text is not displayed for column " + HeaderText + ", under Footer section",
							LogType.ERROR_MESSAGE);
					Assert.fail("Expected Column text is not displayed for column " + HeaderText + ", under Footer section");

				}

			}

			log("Checking the navigation of each link under Footer section", LogType.STEP);
			List<WebElement> FooterHeaders = getCommand().driver
					.findElements(By.xpath("//*[@id='koh-page-outer']/div/footer/div[2]/ul/li/span/span"));

			for (int j = 1; j <= FooterHeaders.size(); j++)

			{

				String pageTitle = getCommand().driver.getTitle();
				getCommand().driver.findElement(By.xpath("(//*[@id='koh-page-outer']/div/footer/div[2]/ul/li/span/span)["+j+"]")).click();

				List<WebElement> footerheaderlinks = getCommand().driver
						.findElements(By.xpath("//*[@id='koh-page-outer']/div/footer/div[2]/ul/li[" + j + "]/ul/li/a"));

				for (int k=1;k<=footerheaderlinks.size();k++)

				{
					
					Target subLinks=new Target("subLinks","//*[@id='koh-page-outer']/div/footer/div[2]/ul/li["+j+"]/ul/li["+k+"]/a",Target.XPATH);
					String Linktext=getCommand().getText(subLinks);
					 if(!Linktext.contains("YouTube")){
						log("Clicking on the link " + Linktext, LogType.STEP);
	
						String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL, Keys.RETURN);
	
						getCommand().sendKeys(subLinks,selectLinkOpeninNewTab);
	
						getCommand().waitFor(9);
	
						ArrayList<String> listofTabs = new ArrayList<String>(getCommand().driver.getWindowHandles());
	
						log("Switching to new tab", LogType.STEP);
	
						getCommand().driver.switchTo().window(listofTabs.get(1));
	
						String CurrentpageTitle = getCommand().driver.getTitle();
	
						if (pageTitle.equals(CurrentpageTitle))
	
						{
	
							log("Clicking on link " + Linktext + " is not redirecting to the corresponding page",
									LogType.ERROR_MESSAGE);
							Assert.fail("Clicking on link " + Linktext + " is not redirecting to the corresponding page");
	
						}
	
						else
	
						{
	
							log("Clicking on link " + Linktext + " is redirecting to the corresponding page", LogType.STEP);
	
						}
	
						getCommand().driver.close();
	
						getCommand().driver.switchTo().window(listofTabs.get(0));
						getCommand().waitFor(3);

				}
				}
			//	getCommand().driver.findElement(By.xpath("(//*[@id='koh-page-outer']/div/footer/div[2]/ul/li/span/span)["+j+"]")).click();


			}
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
		return this;

	}

	// Verify Uber Category Page
	public Hippo_India UberCategoryPage() {
		
		getCommand().waitForTargetVisible(Hamburger_Icon).click(Hamburger_Icon);
		getCommand().click(BathroomNav);
		getCommand().click(BathroomSubNav);

		log("Verify Bathroom Uber Category Page", LogType.STEP);

		try {

			getCommand().waitForTargetPresent(CategoryBtn);
			//WebElement Category = getCommand().driver.findElement(By.xpath("//h3[@class='koh-filter-group-title']"));

			
			List<WebElement> Categories = getCommand().driver
					.findElements(By.xpath("//*[@id=\"koh-page-outer\"]/div/div/section/div[1]/div[2]/div/ul/li"));

			if (Categories.size() != 0 ) {
				log("Category List is displayed ", LogType.STEP);
			}

			else {
				log("Category List is not displayed", LogType.ERROR_MESSAGE);
				
			}

			List<WebElement> SortBy = getCommand().driver.findElements(By.xpath("//button[@class='koh-sort-by']"));

			List<WebElement> Filters = getCommand().driver.findElements(By.xpath("//span[@class='koh-filter-name']"));
			if (SortBy.size() == 0 && Filters.size() == 0) {
				log(" No filter or sorting option", LogType.STEP);
			} else {
				log("Filter or sorting option is present", LogType.ERROR_MESSAGE);
				Assert.fail("Filter or sorting option is present");

			}

			/*WebElement Product = getCommand().driver
					.findElement(By.xpath("(//div[@class='koh-product-tile-actions'])[1]"));

			Actions action = new Actions(getCommand().driver);
			getCommand().waitFor(3);
			action.moveToElement(Product).build().perform();

			if (getCommand().isTargetPresent(QuickView)) {
				log("QuickView Icon is displayed on hover over product modules", LogType.STEP);

			}

			else {
				log("QuickView Icon is not displayed on hover over product modules", LogType.ERROR_MESSAGE);
				Assert.fail("QuickView Icon is not displayed on hover over product modules");

			}
*/
			if (getCommand().isTargetPresent(Image)) {
				log("Each Product Module has a picture", LogType.STEP);
			} else {
				log("Each Product Module doesn't have a picture", LogType.ERROR_MESSAGE);
				Assert.fail("Each Product Module doesn't have a picture");
			}

			if (getCommand().getText(ProductDes) != null) {
				log("Each Product Module has Product Description", LogType.STEP);
			}

			else {
				log("Each Product Module doesn't have Product Description", LogType.ERROR_MESSAGE);
				Assert.fail("Each Product Module doesn't have Product Description");
			}

			if (getCommand().getText(SKU) != null) {
				log("Each Product Module has Product SKU#", LogType.STEP);
			} else {
				log("Each Product Module doesn't have Product SKU#", LogType.ERROR_MESSAGE);
				Assert.fail("Each Product Module doesn't have Product SKU##");
			}

			String PriceText = getCommand().getText(Price);
			PriceText = PriceText.substring(3);

			if (PriceText != null) {
				log("Each Product Module has Product Price", LogType.STEP);
			} else {
				log("Each Product Module doesn't have Product Price", LogType.ERROR_MESSAGE);
				Assert.fail("Each Product Module doesn't have Product Price");
			}

		}

		catch (Exception e) {
			Assert.fail(e.getMessage());
		}

		return this;

	}

	// Verify Sorting
	public Hippo_India VerifySorting() {
		
		getCommand().waitForTargetVisible(Hamburger_Icon).click(Hamburger_Icon);
		getCommand().isTargetPresent(BathroomNav);
		getCommand().click(BathroomNav);
		getCommand().waitForTargetPresent(BathroomWallMount);

		getCommand().isTargetPresent(BathroomWallMount);
		getCommand().click(BathroomWallMount);
		getCommand().waitFor(3);
		//getCommand().scrollTo(SortBy);

		List<WebElement> TotalPros = getCommand().driver
				.findElements(By.xpath("//*[@id='koh-page-outer']/div/div/section[2]/div[3]/div/div/div[1]/a/span[1]"));

		List<String> ProductNames = new ArrayList<String>();

		log("Verify Sorting Options", LogType.STEP);
		System.out.println("Size of total "+TotalPros.size());
		

		for (int i = 1; i <= TotalPros.size(); i++) {

			String name = getCommand().driver
					.findElement(By.xpath("(//span[@class='koh-product-description'])[" + i + "]")).getText();
			ProductNames.add(name);
		}
		Collections.sort(ProductNames);
		
		System.out.println(ProductNames);
		

		getCommand().click(SortBy);
		getCommand().waitForTargetPresent(SortAtoZ);
		getCommand().click(SortAtoZ);

		//js.executeScript("window.scrollBy(0,500)");

		getCommand().waitFor(3);

		List<WebElement> TotalPros1 = getCommand().driver
				.findElements(By.xpath("//*[@id='koh-page-outer']/div/div/section[2]/div[3]/div/div/div[1]/a/span[1]"));
		List<String> ProductsNames1 = new ArrayList<String>();

		for (int j = 1; j <= TotalPros1.size(); j++) {

			String name1 = getCommand().driver
					.findElement(By.xpath("(//span[@class='koh-product-description'])[" + j + "]")).getText();
			ProductsNames1.add(name1);

		}
		System.out.println(ProductsNames1);
		if (ProductNames.equals(ProductsNames1)) {
			log("A-Z sorting is working fine", LogType.STEP);
		}

		else {
			log("A-Z sorting is not working fine", LogType.ERROR_MESSAGE);
			
		}

		List<WebElement> TotalProducts = getCommand().driver
				.findElements(By.xpath("//*[@id='koh-page-outer']/div/div/section[2]/div[3]/div/div/div[1]/a/span[1]"));

		List<String> ProductNames1 = new ArrayList<String>();

		try {

			for (int i = 1; i <= TotalProducts.size(); i++) {

				String name = getCommand().driver
						.findElement(By.xpath("(//span[@class='koh-product-description'])[" + i + "]")).getText();
				ProductNames1.add(name);
			}
			Collections.sort(ProductNames1, Collections.reverseOrder());
			System.out.println(ProductNames1);

			getCommand().click(SortBy);

			getCommand().waitForTargetPresent(SortZtoA);
			getCommand().click(SortZtoA);

			getCommand().waitFor(3);
			//js.executeScript("window.scrollBy(0,500)");

			List<WebElement> TotalProducts1 = getCommand().driver
					.findElements(By.xpath("//*[@id='koh-page-outer']/div/div/section[2]/div[3]/div/div/div[1]/a/span[1]"));
			List<String> Names = new ArrayList<String>();

			for (int j = 1; j <= TotalProducts1.size(); j++) {

				String name1 = getCommand().driver
						.findElement(By.xpath("(//span[@class='koh-product-description'])[" + j + "]")).getText();
				Names.add(name1);

			}

			if (ProductNames1.equals(Names)) {
				log("Z-A sorting is working fine", LogType.STEP);
			}

			else {
				log("Z-A sorting is not working fine", LogType.ERROR_MESSAGE);
				
			}

		}

		catch (Exception e) {
			Assert.fail(e.getMessage());
		}

		return this;
	}

	// Verify Global Nav unfolding functionality
	public Hippo_India VerifyHippoIndia_GlobalNavUnfolding() throws InterruptedException {

		try {
			
			getCommand().waitForTargetVisible(Hamburger_Icon).click(Hamburger_Icon);
			 getCommand().waitFor(2);
			 log("clicking on BathRoom link from Hamburger menu", LogType.STEP);
			 getCommand().click(BathroomNav);
			 WebElement BathroomMenu=getCommand().driver.findElement(By.xpath("//*[@id='koh-primary-nav-menu']/ul/li[1]/div/div"));
			 if(BathroomMenu.isDisplayed()) {
				 log("clicking on BathRoom link and unfolded", LogType.STEP);
				 getCommand().click(BathroomNav);
			 }
			 else {
				 log("clicking on BathRoom link not unfolded", LogType.STEP);
			 }
			 getCommand().waitFor(3);
			 
			 log("clicking on Kitchen link from Hamburger menu", LogType.STEP);
			 getCommand().click(Kitchen);
			 
			 WebElement KitchenMenu=getCommand().driver.findElement(By.xpath("//*[@id=\"koh-primary-nav-menu\"]/ul/li[2]/div/div"));
			 if(KitchenMenu.isDisplayed()) {
				 log("clicking on Kitchen link and unfolded", LogType.STEP);
				 getCommand().click(Kitchen);
			 }
			 else {
				 log("clicking on Kitchen link not unfolded", LogType.STEP);
			 }
			 log("clicking on Ideas link from Hamburger menu", LogType.STEP);
			 getCommand().click(Ideas);
			 WebElement IdeasMenu=getCommand().driver.findElement(By.xpath("//*[@id=\"koh-primary-nav-menu\"]/ul/li[3]/div/div"));
			 if(IdeasMenu.isDisplayed()) {
				 log("clicking on Ideas link and unfolded", LogType.STEP);
				 getCommand().click(Ideas);
			 }
			 else {
				 log("clicking on Ideas link not unfolded", LogType.STEP);
			 }

		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
		return this;

	}

	// Verify correct display of contact us page
	public Hippo_India VerifyHippoIndia_ContactUsPage() throws InterruptedException {
		try {
			log("Clicking on Contact us link", LogType.STEP);
			getCommand().scrollTo(Help_footer).click(Help_footer);
			getCommand().isTargetPresent(ContactUs);
			getCommand().click(ContactUs);

			getCommand().waitFor(5);
			String PageTitle = getCommand().driver.getTitle();
			if (PageTitle.contains("Contact Us")) {
				log("Contact us page is displayed", LogType.STEP);
			} else {
				log("Contact us page is not displayed", LogType.STEP);
				Assert.fail("Contact us page is not displayed");
			}
			log("Selecting Radio button as Yes in contact-us page", LogType.STEP);
			getCommand().waitForTargetPresent(YesButton);
			getCommand().click(YesButton);

			getCommand().waitFor(5);

			boolean ModelNumber = getCommand().driver.findElement(By.xpath("(//input[@name='Model Number'])[1]"))
					.isDisplayed();
			boolean Color = getCommand().driver.findElement(By.xpath("//input[@name='Color/Finish']")).isDisplayed();
			boolean PlaceofPurchase = getCommand().driver.findElement(By.xpath("//input[@name='Place of Purchase']"))
					.isDisplayed();
			boolean DateofInstallation = getCommand().driver.findElement(By.xpath("(//div[@class='nice-select'])[1]"))
					.isDisplayed();
			boolean DateofInstalled = getCommand().driver.findElement(By.xpath("(//div[@class='nice-select'])[2]"))
					.isDisplayed();

			boolean MiddleName = getCommand().driver.findElement(By.xpath("(//input[@name='Middle Name'])[1]"))
					.isDisplayed();
			boolean LastName = getCommand().driver.findElement(By.xpath("(//input[@name='Last Name'])[1]"))
					.isDisplayed();
			boolean Company = getCommand().driver.findElement(By.xpath("(//input[@name='Company'])[1]")).isDisplayed();
			boolean EmailAddress = getCommand().driver.findElement(By.xpath("//input[@id='email']")).isDisplayed();
			boolean CfmEmailAddress = getCommand().driver
					.findElement(By
							.xpath("(//label[contains(text(),'Confirm Email Address')]//following-sibling::input)[1]"))
					.isDisplayed();
			boolean Country = getCommand().driver.findElement(By.xpath("(//input[@name='Country'])[1]")).isDisplayed();
			boolean StreetAddress = getCommand().driver.findElement(By.xpath("(//input[@name='First Name'])[1]"))
					.isDisplayed();
			boolean City = getCommand().driver.findElement(By.xpath("(//input[@name='City'])[1]")).isDisplayed();
			boolean State = getCommand().driver.findElement(By.xpath("(//input[@name='State'])[1]")).isDisplayed();
			boolean PinCode = getCommand().driver.findElement(By.xpath("(//input[@name='PIN Code'])[1]")).isDisplayed();
			boolean PhoneNumber = getCommand().driver.findElement(By.xpath("(//input[@name='Phone Number'])[1]"))
					.isDisplayed();
			boolean RequestDescription = getCommand().driver
					.findElement(By.xpath("(//textarea[@name='Request Discription'])[1]")).isDisplayed();

			if (ModelNumber && Color && PlaceofPurchase && MiddleName && LastName && Company && DateofInstallation
					&& DateofInstalled && EmailAddress && CfmEmailAddress && Country && StreetAddress && City && State
					&& PinCode && PhoneNumber && RequestDescription == true) {

				log("All field Boxes displayed correctly for the product that is already installed", LogType.STEP);
			} else {
				log("Field Boxes are not displayed correctly for the product that is already installed",
						LogType.ERROR_MESSAGE);
				Assert.fail("Field Boxes are not displayed correctly for the product that is already installed");
			}

			getCommand().driver.findElement(By.xpath("(//input[@value='Send'])[1]")).click();
			getCommand().waitFor(5);

			String PlaceofPurchaseErrormsg = getCommand().driver
					.findElement(By.xpath("//input[@name='Place of Purchase']"))
					.getAttribute("data-parsley-error-message");
			if (PlaceofPurchaseErrormsg.contentEquals("This field is required.")) {
				log("Error message displayed for PlaceofPurchase", LogType.STEP);
			} else {
				log("Error message not displayed for PlaceofPurchase", LogType.ERROR_MESSAGE);
				Assert.fail("Error message not displayed for PlaceofPurchase");
			}
			String FirstNameErrormsg = getCommand().driver.findElement(By.xpath("(//input[@name='First Name'])[1]"))
					.getAttribute("data-parsley-error-message");
			if (FirstNameErrormsg.contentEquals("This field is required.")) {
				log("Error message displayed for FirstName", LogType.STEP);
			} else {
				log("Error message not displayed for FirstName", LogType.ERROR_MESSAGE);
				Assert.fail("Error message not displayed for FirstName");
			}
			String LastNameErrormsg = getCommand().driver.findElement(By.xpath("(//input[@name='Last Name'])[1]"))
					.getAttribute("data-parsley-error-message");
			if (LastNameErrormsg.contentEquals("This field is required.")) {
				log("Error message displayed for LastName", LogType.STEP);
			} else {
				log("Error message not displayed for LastName", LogType.ERROR_MESSAGE);
				Assert.fail("Error message not displayed for LastName");
			}
			String EmailAddressErrormsg = getCommand().driver.findElement(By.xpath("//input[@id='email']"))
					.getAttribute("data-parsley-error-message");
			if (EmailAddressErrormsg.contentEquals(
					"Please enter a valid e-mail address: at least 5 characters and contains an @ and at least one dot.")) {
				log("Error message displayed for EmailAddress", LogType.STEP);
			} else {
				log("Error message not displayed for EmailAddress", LogType.ERROR_MESSAGE);
				Assert.fail("Error message not displayed for EmailAddress");
			}
			String CfmEmailAddressErrormsg = getCommand().driver
					.findElement(By
							.xpath("(//label[contains(text(),'Confirm Email Address')]//following-sibling::input)[1]"))
					.getAttribute("data-parsley-error-message");
			if (CfmEmailAddressErrormsg.contentEquals(
					"Oops, this doesn't match the e-mail address you entered above. Please re-enter your e-mail.")) {
				log("Error message displayed for CfmEmailAddress", LogType.STEP);
			} else {
				log("Error message not displayed for CfmEmailAddress", LogType.ERROR_MESSAGE);
				Assert.fail("Error message not displayed for CfmEmailAddress");
			}
			String CountryErrormsg = getCommand().driver.findElement(By.xpath("(//input[@name='Country'])[1]"))
					.getAttribute("data-parsley-error-message");
			if (CountryErrormsg.contentEquals("This field is required.")) {
				log("Error message displayed for Country", LogType.STEP);
			} else {
				log("Error message not displayed for Country", LogType.ERROR_MESSAGE);
				Assert.fail("Error message not displayed for Country");
			}
			String StreetAddressErrormsg = getCommand().driver.findElement(By.xpath("(//input[@name='First Name'])[1]"))
					.getAttribute("data-parsley-error-message");
			if (StreetAddressErrormsg.contentEquals("This field is required.")) {
				log("Error message displayed for StreetAddress", LogType.STEP);
			} else {
				log("Error message not displayed for StreetAddress", LogType.ERROR_MESSAGE);
				Assert.fail("Error message not displayed for StreetAddress");
			}
			String CityErrormsg = getCommand().driver.findElement(By.xpath("(//input[@name='City'])[1]"))
					.getAttribute("data-parsley-error-message");
			if (CityErrormsg.contentEquals("This field is required.")) {
				log("Error message displayed for City", LogType.STEP);
			} else {
				log("Error message not displayed for City", LogType.ERROR_MESSAGE);
				Assert.fail("Error message not displayed for City");
			}
			String StateErrormsg = getCommand().driver.findElement(By.xpath("(//input[@name='State'])[1]"))
					.getAttribute("data-parsley-error-message");
			if (StateErrormsg.contentEquals("???not.valid.state.text???")) {
				log("Error message displayed for State", LogType.STEP);
			} else {
				log("Error message not displayed for  State", LogType.ERROR_MESSAGE);
				Assert.fail("Error message not displayed for  State");
			}
			String PinCodeErrormsg = getCommand().driver.findElement(By.xpath("(//input[@name='PIN Code'])[1]"))
					.getAttribute("data-parsley-error-message");
			if (PinCodeErrormsg.contentEquals("This field is required.")) {
				log("Error message displayed for PinCode", LogType.STEP);
			} else {
				log("Error message not displayed for  PinCode", LogType.ERROR_MESSAGE);
				Assert.fail("Error message not displayed for  PinCode");
			}
			String PhoneNumberErrmsg = getCommand().driver.findElement(By.xpath("(//input[@name='Phone Number'])[1]"))
					.getAttribute("data-parsley-error-message");
			if (PhoneNumberErrmsg.contentEquals("This field is required.")) {

				log("Error message displayed for PhoneNumber", LogType.STEP);
			} else {
				log("Error message not displayed for  PhoneNumber", LogType.ERROR_MESSAGE);
				Assert.fail("Error message not displayed for  PhoneNumber");
			}
			getCommand().waitFor(5);

			log("Selecting Radio button as No in contact-us page", LogType.STEP);
			getCommand().driver.navigate().refresh();
			getCommand().waitFor(3);
			
			   WebElement element=getCommand().driver.findElement(By.xpath("//*[@id=\"koh-page-outer\"]/div/div/section/div/div[1]/span"));
        	      JavascriptExecutor jse=(JavascriptExecutor)getCommand().driver;
        	       jse.executeScript("arguments[0].scrollIntoView(true);", element);
        	       Thread.sleep(3000);
			
			//getCommand().scrollTo(NoButton);
			getCommand().isTargetPresent(NoButton);
			getCommand().click(NoButton);

			getCommand().waitFor(5);

			boolean ProductTypeforNo = getCommand().driver.findElement(By.xpath("(//div[@class='nice-select'])[3]"))
					.isDisplayed();
			boolean ModelNumberforNo = getCommand().driver.findElement(By.xpath("(//input[@name='Model Number'])[2]"))
					.isDisplayed();
			boolean FirstNameforNo = getCommand().driver.findElement(By.xpath("(//input[@name='First Name'])[2]"))
					.isDisplayed();
			boolean MiddleNameforNo = getCommand().driver.findElement(By.xpath("(//input[@name='Middle Name'])[2]"))
					.isDisplayed();
			boolean LastNameforNo = getCommand().driver.findElement(By.xpath("(//input[@name='Last Name'])[2]"))
					.isDisplayed();
			boolean CompanyforNo = getCommand().driver.findElement(By.xpath("(//input[@name='Company'])[2]"))
					.isDisplayed();
			boolean EmailAddressforNo = getCommand().driver.findElement(By.xpath("//input[@id='email2']"))
					.isDisplayed();
			boolean CfmEmailAddressforNo = getCommand().driver
					.findElement(By
							.xpath("(//label[contains(text(),'Confirm Email Address')]//following-sibling::input)[2]"))
					.isDisplayed();
			boolean CountryforNo = getCommand().driver.findElement(By.xpath("(//input[@name='Country'])[2]"))
					.isDisplayed();
			boolean StreetAddressforNo = getCommand().driver.findElement(By.xpath("(//input[@name='First Name'])[2]"))
					.isDisplayed();
			boolean CityforNo = getCommand().driver.findElement(By.xpath("(//input[@name='City'])[2]")).isDisplayed();
			boolean StateforNo = getCommand().driver.findElement(By.xpath("(//input[@name='State'])[2]")).isDisplayed();
			boolean PinCodeforNo = getCommand().driver.findElement(By.xpath("(//input[@name='PIN Code'])[2]"))
					.isDisplayed();
			boolean PhoneNumberforNo = getCommand().driver.findElement(By.xpath("(//input[@name='Phone Number'])[2]"))
					.isDisplayed();
			boolean RequestDescriptionforNo = getCommand().driver
					.findElement(By.xpath("(//textarea[@name='Request Discription'])[2]")).isDisplayed();
			if (ProductTypeforNo && ModelNumberforNo && FirstNameforNo && MiddleNameforNo && LastNameforNo
					&& CompanyforNo && EmailAddressforNo && CfmEmailAddressforNo && CountryforNo && StreetAddressforNo
					&& CityforNo && StateforNo && PinCodeforNo && PhoneNumberforNo && RequestDescriptionforNo == true) {

				log("All field Boxes displayed correctly for the products that was not installed", LogType.STEP);
			} else {
				log("Field Boxes are not displayed correctly for the products that was not installed",
						LogType.ERROR_MESSAGE);
				Assert.fail("Field Boxes are not displayed correctly for the products that was not installed");
			}

			getCommand().driver.findElement(By.xpath("(//input[@value='Send'])[2]")).click();
			getCommand().waitFor(10);

			if (FirstNameErrormsg.contentEquals("This field is required.")) {
				log("Error message displayed for FirstName", LogType.STEP);
			} else {
				log("Error message not displayed for FirstName", LogType.ERROR_MESSAGE);
				Assert.fail("Error message not displayed for FirstName");
			}

			if (LastNameErrormsg.contentEquals("This field is required.")) {
				log("Error message displayed for LastName", LogType.STEP);
			} else {
				log("Error message not displayed for LastName", LogType.ERROR_MESSAGE);
				Assert.fail("Error message not displayed for LastName");
			}

			if (EmailAddressErrormsg.contentEquals(
					"Please enter a valid e-mail address: at least 5 characters and contains an @ and at least one dot.")) {
				log("Error message displayed for EmailAddress", LogType.STEP);
			} else {
				log("Error message not displayed for EmailAddress", LogType.ERROR_MESSAGE);
				Assert.fail("Error message not displayed for EmailAddress");
			}

			if (CfmEmailAddressErrormsg.contentEquals(
					"Oops, this doesn't match the e-mail address you entered above. Please re-enter your e-mail.")) {
				log("Error message displayed for CfmEmailAddress", LogType.STEP);
			} else {
				log("Error message not displayed for CfmEmailAddress", LogType.ERROR_MESSAGE);
				Assert.fail("Error message not displayed for CfmEmailAddress");
			}

			if (CountryErrormsg.contentEquals("This field is required.")) {
				log("Error message displayed for Country", LogType.STEP);
			} else {
				log("Error message not displayed for Country", LogType.ERROR_MESSAGE);
				Assert.fail("Error message not displayed for Country");
			}

			if (StreetAddressErrormsg.contentEquals("This field is required.")) {
				log("Error message displayed for StreetAddress", LogType.STEP);
			} else {
				log("Error message not displayed for StreetAddress", LogType.ERROR_MESSAGE);
				Assert.fail("Error message not displayed for StreetAddress");
			}

			if (CityErrormsg.contentEquals("This field is required.")) {
				log("Error message displayed for City", LogType.STEP);
			} else {
				log("Error message not displayed for City", LogType.ERROR_MESSAGE);
				Assert.fail("Error message not displayed for City");
			}

			if (StateErrormsg.contentEquals("???not.valid.state.text???")) {
				log("Error message displayed for State", LogType.STEP);
			} else {
				log("Error message not displayed for  State", LogType.ERROR_MESSAGE);
				Assert.fail("Error message not displayed for  State");
			}

			if (PinCodeErrormsg.contentEquals("This field is required.")) {
				log("Error message displayed for PinCode", LogType.STEP);
			} else {
				log("Error message not displayed for  PinCode", LogType.ERROR_MESSAGE);
				Assert.fail("Error message not displayed for  PinCode");
			}

			if (PhoneNumberErrmsg.contentEquals("This field is required.")) {

				log("Error message displayed for PhoneNumber", LogType.STEP);
			} else {
				log("Error message not displayed for  PhoneNumber", LogType.ERROR_MESSAGE);
				Assert.fail("Error message not displayed for  PhoneNumber");
			}
			getCommand().waitFor(5);
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}

		return this;

	}

	// Verify Place an Enquiry page Functionality
	public Hippo_India VerifyHippoIndia_PlaceanEnquiryCTA() throws InterruptedException {
		try {
			log("Clicking on Enquiry CTA Link", LogType.STEP);
			getCommand().waitForTargetPresent(EnquiryNow);
			getCommand().click(EnquiryNow);
			getCommand().waitForTargetPresent(PlaceAnEnquiry);
			log("Expected URL for PlaceAnEnquiry", LogType.STEP);
			String PlaceAnEnquiryURL = "http://plumbingindia.kohler.acct.us.onehippo.com/place-an-enquiry";
			String PlaceAnEnquiryActualURL = getCommand().driver.getCurrentUrl();
			log("Actual URL for PlaceAnEnquiry", LogType.STEP);
			Assert.assertEquals(PlaceAnEnquiryActualURL, PlaceAnEnquiryURL);
			log("Place an Enquiry Expected URL matched with Actual URL", LogType.STEP);

			if (getCommand().isTargetPresent(PlaceAnEnquiry)) {
				log("Place an Enquiry Page displayed", LogType.STEP);
			} else {
				log("Place an Enquiry Page not displayed", LogType.ERROR_MESSAGE);
				Assert.fail("Place an Enquiry Page not displayed");
			}
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
		return this;
	}

	// Verify literature page Functionality
	public Hippo_India VerifyHippoIndia_LiteraturePage() throws InterruptedException {
		try {
			
			String currentWindow = getCommand().driver.getWindowHandle();
			getCommand().waitForTargetVisible(Hamburger_Icon).click(Hamburger_Icon);
			
			getCommand().isTargetPresent(BathroomNav);
			getCommand().click(BathroomNav);
			log("click an bathroom link in Global Nav menu", LogType.STEP);
			   WebElement element=getCommand().driver.findElement(By.xpath("//*[@id=\"koh-primary-nav-menu\"]/ul/li[1]/div/div/ul[5]/li[8]"));
        	      JavascriptExecutor jse=(JavascriptExecutor)getCommand().driver;
        	       jse.executeScript("arguments[0].scrollIntoView(true);", element);
        	       Thread.sleep(3000);
			log("click an literature link in bathroom submenu", LogType.STEP);
			getCommand().isTargetPresent(Literature);
			getCommand().click(Literature);
			getCommand().waitFor(5);
			String LiteratureURL = "http://plumbingindia.kohler.acct.us.onehippo.com/literature";
			log("Verify Current Literature URL matches with Expected", LogType.STEP);
			getCommand().driver.getCurrentUrl().contentEquals(LiteratureURL);
			log("Literature page accessed", LogType.STEP);
			getCommand().driver.switchTo().window(currentWindow);

			getCommand().waitFor(5);

			
			getCommand().isTargetPresent(Ideas);
			getCommand().click(Ideas);
			log("click an Ideas link in Global Nav menu", LogType.STEP);
			getCommand().waitFor(5);
			log("click an literature link in Ideas submenu", LogType.STEP);
			getCommand().driver.findElement(By.xpath("//li[@class='nav-heading']/a[contains(text(),'Literature')]"))
					.click();
			getCommand().waitFor(10);
			getCommand().driver.getCurrentUrl().contentEquals(LiteratureURL);
			log("Literature page accessed", LogType.STEP);
			getCommand().driver.switchTo().window(currentWindow);
			getCommand().waitFor(5);
			getCommand().waitForTargetVisible(Hamburger_Icon).click(Hamburger_Icon);
			log("click an literature link Footer", LogType.STEP);
			getCommand().scrollTo(Help_footer).click(Help_footer);

			getCommand().isTargetPresent(FootLiterature);
			getCommand().click(FootLiterature);
			getCommand().waitFor(5);
			if (getCommand().driver.getCurrentUrl().contentEquals(LiteratureURL)) {
				log("Literature page accessed", LogType.STEP);
			} else {
				log("Unable to Access Literature page", LogType.ERROR_MESSAGE);
				
			}

			getCommand().waitFor(5);

			/*log("Clicking on modules in Literature page", LogType.STEP);
			getCommand().driver.findElement(By.xpath("(//div[@class='koh-promo-tile'])[1]")).click();
			
			getCommand().waitFor(10);
			String LiteratureModule = getCommand().driver.getCurrentUrl();
			if (LiteratureModule.contains(".pdf")) {
				log("Clicking on Literature Module PDF is displayed", LogType.STEP);
			} else {
				log("Clicking on Literature Module PDF not displayed", LogType.ERROR_MESSAGE);
				Assert.fail("Clicking on Literature Module PDF not displayed");
			}*/
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
		return this;

	}

	// Verify hero image/carousel Functionality
	public Hippo_India VerifyHippoIndia_HeroImage() throws InterruptedException {
		try {
			String HeroImage1 = "(//a[@href='colours-and-finishes-by-kohler'])[2]/img";
			String HeroImage2 = "(//div[@class='koh-banner-background'])[3]/a/img";
			String HeroImage3 = "(//div[@class='koh-banner-background'])[4]/a/img";
			String HeroImage4 = "(//div[@class='koh-banner-background'])[5]/a/img";
			String HeroImage5 = "(//div[@class='koh-banner-background'])[6]/a/img";

			getCommand().driver.findElement(By.xpath("(//a[@href='colours-and-finishes-by-kohler'])[2]")).click();
			getCommand().waitFor(5);
			String HeroImageExpectedURL = "http://plumbingindia.kohler.acct.us.onehippo.com/colours-and-finishes-by-kohler";
			String HeroImage1ActualURL = getCommand().driver.getCurrentUrl();

			if (HeroImage1ActualURL.equalsIgnoreCase(HeroImageExpectedURL)) {
				log("Redirected to Corresponding hero image page", LogType.STEP);
			} else {
				log("Unable to Redirect to Corresponding hero image page", LogType.ERROR_MESSAGE);
				Assert.fail("Unable to Redirect to Corresponding hero image page");
			}
			getCommand().driver.navigate().back();
			getCommand().waitFor(5);

			getCommand().driver.findElement(By.xpath(HeroImage1)).isDisplayed();
			log("Clicking on Navigation dot2 to get second hero image", LogType.STEP);
			getCommand().waitFor(5);
			getCommand().driver.findElement(By.xpath("//li[@id='slick-slide01']/button")).click();
			getCommand().waitFor(5);
			getCommand().driver.findElement(By.xpath(HeroImage2)).isDisplayed();
			if (!(HeroImage2 == HeroImage1)) {
				log("Hero Image changed After click on Navigation dots", LogType.STEP);
			} else {
				log("Hero Image remains unchanged After click on Navigation dots", LogType.ERROR_MESSAGE);
				Assert.fail("Hero Image remains unchanged After click on Navigation dots");
			}

			log("Clicking on Navigation dot3 to get third hero image", LogType.STEP);
			getCommand().driver.findElement(By.xpath("//li[@id='slick-slide02']/button")).click();
			getCommand().waitFor(5);
			getCommand().driver.findElement(By.xpath(HeroImage3)).isDisplayed();
			if (!(HeroImage3 == HeroImage2)) {
				log("Hero Image changed After click on Navigation dots", LogType.STEP);
			} else {
				log("Hero Image remains unchanged After click on Navigation dots", LogType.ERROR_MESSAGE);
				Assert.fail("Hero Image remains unchanged After click on Navigation dots");
			}

			log("Clicking on Navigation dot4 to get fourth hero image", LogType.STEP);
			getCommand().driver.findElement(By.xpath("//li[@id='slick-slide03']/button")).click();
			getCommand().waitFor(5);
			getCommand().driver.findElement(By.xpath(HeroImage4)).isDisplayed();
			if (!(HeroImage4 == HeroImage3)) {
				log("Hero Image changed After click on Navigation dots", LogType.STEP);
			} else {
				log("Hero Image remains unchanged After click on Navigation dots", LogType.ERROR_MESSAGE);
				Assert.fail("Hero Image remains unchanged After click on Navigation dots");
			}

			log("Clicking on Navigation dot5 to get fifth hero image", LogType.STEP);
			getCommand().driver.findElement(By.xpath("//li[@id='slick-slide04']/button")).click();
			getCommand().waitFor(5);
			getCommand().driver.findElement(By.xpath(HeroImage5)).isDisplayed();
			if (!(HeroImage5 == HeroImage4)) {
				log("Hero Image changed After click on Navigation dots", LogType.STEP);
			} else {
				log("Hero Image remains unchanged After click on Navigation dots", LogType.ERROR_MESSAGE);
				Assert.fail("Hero Image remains unchanged After click on Navigation dots");
			}

			log("Clicking on Navigation dot1 to get first hero image", LogType.STEP);
			getCommand().driver.findElement(By.xpath("//li[@id='slick-slide00']/button")).click();
			getCommand().waitFor(5);
			getCommand().driver.findElement(By.xpath(HeroImage1)).isDisplayed();
			if (!(HeroImage1 == HeroImage5)) {
				log("Hero Image changed After click on Navigation dots", LogType.STEP);
			} else {
				log("Hero Image remains unchanged After click on Navigation dots", LogType.ERROR_MESSAGE);
				Assert.fail("Hero Image remains unchanged After click on Navigation dots");
			}

			getCommand().driver.findElement(By.xpath(HeroImage1)).isDisplayed();
			getCommand().driver
					.findElement(By.xpath("(//button[contains(@class,'slick-arrow')])[1]/span[@class='icon']")).click();
			if (!(HeroImage1 == HeroImage5)) {
				log("Hero Image changed After clicking on previous icon", LogType.STEP);
			} else {
				log("Hero Image remains unchanged After clicking on previous icon", LogType.ERROR_MESSAGE);
				Assert.fail("Hero Image remains unchanged After clicking on previous icon");
			}
			getCommand().driver.findElement(By.xpath(HeroImage5)).isDisplayed();
			getCommand().driver
					.findElement(By.xpath("(//button[contains(@class,'slick-arrow')])[2]/span[@class='icon']")).click();
			getCommand().waitFor(5);
			getCommand().driver.findElement(By.xpath(HeroImage1)).isDisplayed();
			getCommand().driver
					.findElement(By.xpath("(//button[contains(@class,'slick-arrow')])[2]/span[@class='icon']")).click();
			getCommand().waitFor(5);
			getCommand().driver.findElement(By.xpath(HeroImage2)).isDisplayed();
			if (!(HeroImage1 == HeroImage2)) {
				log("Hero Image changed After clicking on Next icon", LogType.STEP);
			} else {
				log("Hero Image remains unchanged After clicking on Next icon", LogType.ERROR_MESSAGE);
				Assert.fail("Hero Image remains unchanged After clicking on Next icon");
			}

			getCommand().driver.findElement(By.xpath(HeroImage2)).isDisplayed();
			getCommand().driver
					.findElement(By.xpath("(//button[contains(@class,'slick-arrow')])[1]/span[@class='icon']")).click();
			if (!(HeroImage1 == HeroImage2)) {
				log("Hero Image changes After clicking on previous icon", LogType.STEP);
			} else {
				log("Hero Image remains unchanged After clicking on previous icon", LogType.ERROR_MESSAGE);
				Assert.fail("Hero Image remains unchanged After clicking on previous icon");
			}
			getCommand().driver.findElement(By.xpath(HeroImage1)).isDisplayed();
			getCommand().driver
					.findElement(By.xpath("(//button[contains(@class,'slick-arrow')])[2]/span[@class='icon']")).click();
			getCommand().waitFor(5);
			getCommand().driver.findElement(By.xpath(HeroImage2)).isDisplayed();
			getCommand().driver
					.findElement(By.xpath("(//button[contains(@class,'slick-arrow')])[2]/span[@class='icon']")).click();
			getCommand().waitFor(5);
			getCommand().driver.findElement(By.xpath(HeroImage3)).isDisplayed();
			if (!(HeroImage2 == HeroImage3)) {
				log("Hero Image changed After clicking on Next icon", LogType.STEP);
			} else {
				log("Hero Image remains unchanged After clicking on Next icon", LogType.ERROR_MESSAGE);
				Assert.fail("Hero Image remains unchanged After clicking on Next icon");
			}

			getCommand().driver.findElement(By.xpath(HeroImage3)).isDisplayed();
			getCommand().driver
					.findElement(By.xpath("(//button[contains(@class,'slick-arrow')])[1]/span[@class='icon']")).click();
			if (!(HeroImage2 == HeroImage3)) {
				log("Hero Image changes After clicking on previous icon", LogType.STEP);
			} else {
				log("Hero Image remains unchanged After clicking on previous icon", LogType.ERROR_MESSAGE);
				Assert.fail("Hero Image remains unchanged After clicking on previous icon");
			}
			getCommand().driver.findElement(By.xpath(HeroImage2)).isDisplayed();
			getCommand().driver
					.findElement(By.xpath("(//button[contains(@class,'slick-arrow')])[2]/span[@class='icon']")).click();
			getCommand().waitFor(5);
			getCommand().driver.findElement(By.xpath(HeroImage3)).isDisplayed();
			getCommand().driver
					.findElement(By.xpath("(//button[contains(@class,'slick-arrow')])[2]/span[@class='icon']")).click();
			getCommand().waitFor(5);
			getCommand().driver.findElement(By.xpath(HeroImage4)).isDisplayed();
			if (!(HeroImage3 == HeroImage4)) {
				log("Hero Image changed After clicking on Next icon", LogType.STEP);
			} else {
				log("Hero Image remains unchanged After clicking on Next icon", LogType.ERROR_MESSAGE);
				Assert.fail("Hero Image remains unchanged After clicking on Next icon");
			}

			getCommand().driver.findElement(By.xpath(HeroImage4)).isDisplayed();
			getCommand().driver
					.findElement(By.xpath("(//button[contains(@class,'slick-arrow')])[1]/span[@class='icon']")).click();
			if (!(HeroImage3 == HeroImage4)) {
				log("Hero Image changed After clicking on previous icon", LogType.STEP);
			} else {
				log("Hero Image remains unchanged After clicking on previous icon", LogType.ERROR_MESSAGE);
				Assert.fail("Hero Image remains unchanged After clicking on previous icon");
			}
			getCommand().driver.findElement(By.xpath(HeroImage3)).isDisplayed();
			getCommand().driver
					.findElement(By.xpath("(//button[contains(@class,'slick-arrow')])[2]/span[@class='icon']")).click();
			getCommand().waitFor(5);
			getCommand().driver.findElement(By.xpath(HeroImage4)).isDisplayed();
			getCommand().driver
					.findElement(By.xpath("(//button[contains(@class,'slick-arrow')])[2]/span[@class='icon']")).click();
			getCommand().waitFor(5);
			getCommand().driver.findElement(By.xpath(HeroImage5)).isDisplayed();
			if (!(HeroImage4 == HeroImage5)) {
				log("Hero Image changed After clicking on Next icon", LogType.STEP);
			} else {
				log("Hero Image remains unchanged After clicking on Next icon", LogType.ERROR_MESSAGE);
				Assert.fail("Hero Image remains unchanged After clicking on Next icon");
			}

			getCommand().driver.findElement(By.xpath(HeroImage5)).isDisplayed();
			getCommand().driver
					.findElement(By.xpath("(//button[contains(@class,'slick-arrow')])[1]/span[@class='icon']")).click();
			if (!(HeroImage4 == HeroImage5)) {
				log("Hero Image changed After clicking on previous icon", LogType.STEP);
			} else {
				log("Hero Image remains unchanged After clicking on previous icon", LogType.ERROR_MESSAGE);
				Assert.fail("Hero Image remains unchanged After clicking on previous icon");
			}
			getCommand().driver.findElement(By.xpath(HeroImage4)).isDisplayed();
			getCommand().driver
					.findElement(By.xpath("(//button[contains(@class,'slick-arrow')])[2]/span[@class='icon']")).click();
			getCommand().waitFor(5);
			getCommand().driver.findElement(By.xpath(HeroImage5)).isDisplayed();
			getCommand().driver
					.findElement(By.xpath("(//button[contains(@class,'slick-arrow')])[2]/span[@class='icon']")).click();
			getCommand().waitFor(5);
			getCommand().driver.findElement(By.xpath(HeroImage1)).isDisplayed();
			if (!(HeroImage5 == HeroImage1)) {
				log("Hero Image changed After clicking on Next icon", LogType.STEP);
			} else {
				log("Hero Image remains unchanged After clicking on Next icon", LogType.ERROR_MESSAGE);
				Assert.fail("Hero Image remains unchanged After clicking on Next icon");
			}

		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}

		return this;
	}

	// Verify the Functionality of worldwide banner
	public Hippo_India VerifyHippoIndia_WorldWideBanner() throws InterruptedException {
		 try
	     {
	    	getCommand().waitForTargetVisible(Hamburger_Icon).click(Hamburger_Icon);
			getCommand().waitForTargetVisible(KohlerWorldWide).click(KohlerWorldWide);
			 
			WebElement element=getCommand().driver.findElement(By.xpath("//*[@id='koh-primary-nav-menu']/div[2]/div[2]/div[1]/ul/li[1]/span"));
	   	      JavascriptExecutor jse=(JavascriptExecutor)getCommand().driver;
	   	       jse.executeScript("arguments[0].scrollIntoView(true);", element);
	   	       Thread.sleep(2000);
			
	   	       List<WebElement> ColumnsHeader=getCommand().driver.findElements(By.xpath("//*[@id='koh-primary-nav-menu']/div[2]/div[2]/div[1]/ul/li/span"));
	        int NoOfColumnsCount=ColumnsHeader.size();
	     
	      if(NoOfColumnsCount==6)       
	         {
	               log("Kohler World Wide banner is composed of 6 columns",LogType.STEP);          
	         }
	      else
	        {
	          log("Kohler World Wide banner is not composed of 6 columns",LogType.ERROR_MESSAGE);
	          Assert.fail("Kohler World Wide banner is not composed of 6 columns");         
	        }
	    
	      for(int i=0;i<NoOfColumnsCount;i++)

	      {

	        String ColumnsHeaderText = ColumnsHeader.get(i).getText();
	        if(ColumnsHeaderText.equalsIgnoreCase("North America")|| ColumnsHeaderText.equalsIgnoreCase("South America")|| ColumnsHeaderText.equalsIgnoreCase("Africa")|| ColumnsHeaderText.equalsIgnoreCase("Europe")||
	        		ColumnsHeaderText.equalsIgnoreCase("Asia")||ColumnsHeaderText.equalsIgnoreCase("Oceania"))
	        {
	              log("Expected Column text: "+ColumnsHeaderText+", is displayed in World Wide banner",LogType.STEP);
	        }

	       else
	        {
	              log("Expected Column text is not displayed for column " +ColumnsHeaderText+", is displayed in World Wide banner",LogType.ERROR_MESSAGE);
	              Assert.fail("Expected Column text is not displayed for column " +ColumnsHeaderText+", is displayed in World Wide banner");
	        }
	      }   
	     
	      
	      log("Checking the navigation of each link",LogType.STEP);
	      //List<WebElement> BannerHeaders=getCommand().driver.findElements(By.xpath("//*[@id='koh-page-outer']/div/header/div/div[1]/div[1]/div[2]/div[1]/ul/li/span"));

	      for (int j = 1; j <= NoOfColumnsCount; j++)

	      {
	          String Url = getCommand().driver.getCurrentUrl();
	          getCommand().driver.findElement(By.xpath("(//*[@id='koh-primary-nav-menu']/div[2]/div[2]/div[1]/ul/li/span)["+j+"]")).click();
	          List<WebElement> bannerheaderlinks = getCommand().driver.findElements(By.xpath("//*[@id='koh-primary-nav-menu']/div[2]/div[2]/div[1]/ul/li["+j+"]/ul/li/a"));
	          int country_Count=bannerheaderlinks.size();
	          log("Total number of countries available in "+j+" column is "+country_Count ,LogType.STEP);
	          for(int k=1;k<=country_Count;k++)
	          {
	              Target countryLinks=new Target("countryLinks","(//*[@id='koh-primary-nav-menu']/div[2]/div[2]/div[1]/ul/li["+j+"]/ul/li/a)["+k+"]",Target.XPATH);

	             String Linktext =getCommand().getText(countryLinks);
	             log("Clicking on the link "+Linktext,LogType.STEP);
	            
	            String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN);
	            getCommand().sendKeys(countryLinks,selectLinkOpeninNewTab);
	             getCommand().waitFor(5);

	            ArrayList<String> listofTabs = new ArrayList<String> (getCommand().driver.getWindowHandles());

	            log("Switching to new tab",LogType.STEP);
	            getCommand().driver.switchTo().window(listofTabs.get(1));
	            
	            String CurrentpageUrl = getCommand().driver.getCurrentUrl();
	           if(Url.equals(CurrentpageUrl))
	           
	            {
	                  log("Clicking on link "+Linktext+" is not redirecting to the corresponding page",LogType.ERROR_MESSAGE);
	                  Assert.fail("Clicking on link "+Linktext+" is not redirecting to the corresponding page");
	            }

	           else
	              {                                             
	                   log("Clicking on link "+Linktext+" is redirecting to the corresponding page",LogType.STEP);
	              }

	            getCommand().driver.close();
	            getCommand().driver.switchTo().window(listofTabs.get(0));

	          }
	         getCommand().driver.findElement(By.xpath("(//*[@id='koh-primary-nav-menu']/div[2]/div[2]/div[1]/ul/li/span)["+j+"]")).click();
	         }
	      
	      log("Verifying navigation of each other Kohler brands",LogType.STEP);
	  
	      List<WebElement> Brands = getCommand().driver.findElements(By.xpath("//*[@id=\"koh-primary-nav-menu\"]/div[2]/div[2]/div[2]/ul/li/a"));	
			
	      int NoofOtherBrands=Brands.size();
	      
	      if(NoofOtherBrands==6)       
	       {
	             log("6 other kohler brands are present",LogType.STEP);          
	       }
	     else
	      {
	        log("Other Kohler brands are not present",LogType.ERROR_MESSAGE);
	        Assert.fail("Other Kohler brands are not present");         
	      }
	      for(WebElement Brand:Brands)
			{
	    	    String Url = getCommand().driver.getCurrentUrl();
	    	  
				String href = Brand.getAttribute("href");
				log("Clicking on the brand with link: "+href,LogType.STEP);
				String SelectLinkOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN);
				Brand.sendKeys(SelectLinkOpeninNewTab);
				Thread.sleep(9000);
				ArrayList<String> ListOfTabs = new ArrayList<String>(getCommand().driver.getWindowHandles());
				getCommand().driver.switchTo().window(ListOfTabs.get(1));
				String CurrentPageTitle = getCommand().driver.getTitle();
				log("Navigated to brand '"+CurrentPageTitle+"'",LogType.STEP);
	  
				String CurrentpageUrl = getCommand().driver.getCurrentUrl();
		           if(Url.equals(CurrentpageUrl))
		           
		            {
		                  log("Clicking on link "+CurrentPageTitle+" is not redirecting to the corresponding page",LogType.ERROR_MESSAGE);
		                  Assert.fail("Clicking on link "+CurrentPageTitle+" is not redirecting to the corresponding page");
		            }

		           else
		              {                                             
		                   log("Clicking on link "+CurrentPageTitle+" is redirecting to the corresponding page",LogType.STEP);
		              }
				
			
					getCommand().driver.close();
		    		getCommand().driver.switchTo().window(ListOfTabs.get(0));				
				}
			}
	       
	      catch(Exception ex)
	      {
	          Assert.fail(ex.getMessage());
	      }
	     
			 return this;
		 }


	// Verify compare feature works properly and Verify Compare modal
	public Hippo_India VerifyHippoIndia_Comparefeature() throws InterruptedException {
		try {

			log("Clicking on Bathroom and Navigating to Pedestal page", LogType.STEP);
			getCommand().waitForTargetPresent(BathroomNav);
			getCommand().click(BathroomNav);

			getCommand().waitForTargetPresent(Pedestal);
			getCommand().isTargetPresent(Pedestal);
			getCommand().click(Pedestal);

			js.executeScript("window.scrollBy(0,700)");
			getCommand().waitFor(5);
			String Parent = getCommand().driver.getWindowHandle();
			log("Listing all the products present in the page", LogType.STEP);
			List<WebElement> products = getCommand().driver
					.findElements(By.xpath("//div[@class='koh-search-results']/div"));
			for (int i = 0; i < 3; i++) {
				getCommand().driver.switchTo().window(Parent);
				WebElement element = products.get(i);
				Actions sact = new Actions(getCommand().driver);
				sact.moveToElement(element).build().perform();
				getCommand().waitFor(5);

				log("Click on the compare link in the product", LogType.STEP);
				List<WebElement> Compare = getCommand().driver
						.findElements(By.xpath("//div[@class='koh-product-quick-view']/following-sibling::a[1]"));
				Compare.get(i).click();
				Set<String> s1 = getCommand().driver.getWindowHandles();
				Iterator<String> it = s1.iterator();
				while (it.hasNext()) {
					String ChildWindow = it.next();
					getCommand().driver.switchTo().window(ChildWindow);
					log("switching to compare products window", LogType.STEP);
				}
				getCommand().waitFor(5);
			}

			List<WebElement> productModules = getCommand().driver
					.findElements(By.xpath("//div[@class='koh-product-tile-content']/a"));
			if (productModules.size() == 9) {
				for (int l = 1; l <= 3; l++) {
					log("Verify compare panel shows three product modules (with thumbnail, product name, SKU# and price) each with a close icon",
							LogType.STEP);
					getCommand().driver.findElement(By.xpath("//div[@class='koh-compare-items open']/div[" + l + "]"))
							.isDisplayed();
					getCommand().driver.findElement(By.xpath("(//div[@class='koh-compare-item'])[" + l + "]/a/img"))
							.isDisplayed();
					getCommand().driver.findElement(By.xpath("(//span[@class='koh-compare-name'])[" + l + "]"))
							.isDisplayed();
					getCommand().driver.findElement(By.xpath("(//span[@class='koh-compare-sku'])[" + l + "]"))
							.isDisplayed();
					getCommand().driver.findElement(By.xpath("(//span[@class='koh-compare-price'])[" + l + "]"))
							.isDisplayed();
				}
				log("Verify clear results link, compare CTA and expand arrow is displayed", LogType.STEP);

				getCommand().waitForTargetPresent(ClearResults);
				getCommand().isTargetPresent(ClearResults);
				getCommand().waitForTargetPresent(Compare);
				getCommand().isTargetPresent(Compare);
				getCommand().waitForTargetPresent(CompareHead);
				getCommand().isTargetPresent(CompareHead);

			} else {
				log("Compare panel does not displays product modules", LogType.ERROR_MESSAGE);
				Assert.fail("Compare panel does not displays product modules");
			}
			log("Click on Compare CTA link", LogType.STEP);
			getCommand().driver.findElement(By.xpath("(//a[contains(text(),'Compare')])[1]")).click();
			getCommand().waitFor(5);
			List<WebElement> productColumns = getCommand().driver
					.findElements(By.xpath("//ul[@class='koh-compare-features-list']"));
			Assert.assertEquals(4, productColumns.size(), "4 Columns are displayed in compare modal");
			Assert.assertTrue(
					getCommand().driver.findElement(By.xpath("//button[@class='koh-compare-print']")).isDisplayed(),
					"Print icon is located in the page");

			for (int k = 2; k <= 4; k++) {
				log("Verify Each product column has an image, product name, SKU# and price, and product features",
						LogType.STEP);
				boolean images = getCommand().driver
						.findElement(By.xpath("(//div[@class='koh-compare-top'])[" + k + "]/a/img")).isDisplayed();

				boolean Productname = getCommand().driver
						.findElement(By.xpath("(//div[@class='koh-compare-top'])[" + k + "]/a/span[1]")).isDisplayed();

				boolean sku = getCommand().driver
						.findElement(By.xpath("(//div[@class='koh-compare-top'])[" + k + "]/a/span[2]")).isDisplayed();

				boolean price = getCommand().driver
						.findElement(By.xpath("(//div[@class='koh-compare-top'])[" + k + "]/a/span[3]")).isDisplayed();

				boolean Productfeatures = getCommand().driver
						.findElement(By.xpath("(//ul[@class='koh-compare-features-list'])[" + k + "]")).isDisplayed();

				if (images && sku && Productname && price && Productfeatures == true) {

					log("Each product column has an image, product name, SKU# and product features", LogType.STEP);
				} else {
					log("Missing elements from product features", LogType.ERROR_MESSAGE);
					Assert.fail("Missing elements from product features");
				}

			}
			WebElement close = getCommand().driver.findElement(By.xpath("(//button[@class='remodal-close'])[10]"));
			close.click();
			List<WebElement> Compare_panel = getCommand().driver
					.findElements(By.xpath("//div[@id='comparePanel']/div"));
			getCommand().waitFor(5);
			log("Click on close results link and verifying compare panel disappears", LogType.STEP);
			getCommand().driver.findElement(By.xpath("//button[contains(text(),'Clear Results')]")).click();
			getCommand().waitFor(5);
			if (Compare_panel.size() != 0) {
				log("Compare panel disappears", LogType.STEP);
			} else {
				log("Compare panel displayed", LogType.STEP);
				Assert.fail("Compare panel displayed");
			}
			getCommand().waitFor(5);

		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
		return this;
	}

}
