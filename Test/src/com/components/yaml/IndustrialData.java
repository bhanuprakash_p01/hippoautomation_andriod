package com.components.yaml;

import com.iwaf.framework.BasePage;

public class IndustrialData 
{

	public String search_NoResult;
	public String search_Result;
	public String search_Product;
	public String pdp_product;
	public String page_Title;
	public String search_Header;
	public String dist_Header;
	public String dist_ZipCode;
	public String dist_ZipCode1;
	public String contact_Header;
	public String contact_us_firstname;
	public String contact_us_lastname;
	public String contact_us_email;
	public String contact_us_phone;
	public String contact_us_address;
	public String contact_us_address2;
	public String contact_us_city;
	public String contact_us_postalcode;
	public String contact_us_company;
	public String contact_us_comments;
	public String contact_us_newsletter_header;
	public String news_email;
	public String news_firstname;
	public String news_lastname;
	public String news_address;
	public String news_suite;
	public String news_city;
	public String news_zipcode;
	public String notalink;
	public String banner_text;
	public String contact_Page_Title;
	public String pdp_Tab1;
	public String pdp_Tab2;
	public String pdp_Tab3;
	public String health_article1;
	public String health_article2;
	public String health_article3;
	public String file_type;
	public String ele_invisible;
	
	public static IndustrialData fetch(String key)
	{
		BasePage pageObj = new BasePage();
		IndustrialData obj = pageObj.getCommand().loadYaml(key, "data-pool/Industrial_Data.yaml");
		return obj;
	}

}
