package com.components.yaml;

import com.iwaf.framework.BasePage;

public class PortableData 
{
	public String search_NoResult;
	public String search_Result;
	public String search_Product;
	public String search_Header;
	public String page_Title;
	public String dealer_zipCode;
	public String dealer_Header;
	public String contact_Header;
	public String contact_us_firstname;
	public String contact_us_lastname;
	public String contact_us_email;
	public String contact_us_phone;
	public String contact_us_address;
	public String contact_us_address2;
	public String contact_us_city;
	public String contact_us_postalcode;
	public String contact_us_comments;
	public String samelink_footer;
	public String banner_text;
	public String pdp_product;
	public String pdp_currency;
	public String pdp_Tab1;
	public String pdp_Tab2;
	public String pdp_Tab3;
	
	
	public static PortableData fetch(String key)
	{
		BasePage pageObj = new BasePage();
		PortableData obj = pageObj.getCommand().loadYaml(key, "data-pool/Portable_Data.yaml");
		return obj;
	}
}
